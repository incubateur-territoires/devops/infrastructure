#!/usr/bin/env python3

# prints a summary of volumes in scaleway and PVs in kubernetes that could probably be deleted
# requires the scw CLI and kubectl
# adapt the globals to fit your configuration

import json
import subprocess
import sqlite3

KUBECTL_PROD_COMMAND = "kubectl --context=anct-prod"
KUBECTL_DEV_COMMAND = "kubectl --context=anct-dev"
SCW_PROD_CLUSTER_NAME = "Incubateur-production"
SCW_DEV_CLUSTER_NAME = "Incubateur-development"

DB_NAME = "data.db"

def cmd(c):
    run = subprocess.run(c, shell=True, capture_output=True)
    run.check_returncode()
    return run

def persist_data(db, name, data):
    sorted_data = [
        {k: v for k, v in sorted(list(d.items()))}
        for d in data
        ]
    insert_data = [
        list(d.values()) for d in sorted_data
    ]
    columns = list(sorted_data[0].keys())
    db.execute(f"DROP TABLE IF EXISTS {name}")
    create_columns_stmt = ','.join([c + " TEXT" for c in columns])
    db.execute(f"CREATE TABLE {name} ({create_columns_stmt})")
    columns_stmt = ','.join([c for c in columns])
    param_stmt = ','.join(["?" for c in columns])
    db.executemany(f"INSERT INTO {name} ({columns_stmt}) VALUES ({param_stmt})", insert_data)
    db.commit()

def load_data():
    clusters = json.loads(cmd("scw k8s cluster list -o json").stdout)
    clusters = { c["name"]: c for c in clusters }

    volumes_prod_raw = json.loads(cmd(f"scw instance volume list -o json project-id={clusters[SCW_PROD_CLUSTER_NAME]['project_id']}").stdout)
    volumes_dev_raw = json.loads(cmd(f"scw instance volume list -o json project-id={clusters[SCW_DEV_CLUSTER_NAME]['project_id']}").stdout)
    volumes = [
        {
            "id": v["id"],
            "name": v["name"],
            "size": v["size"],
            "creation_date": v["creation_date"],
            "project": v["project"],
            "state": v["state"],
            "zone": v["zone"],
            "server_id": (v.get("server") or dict()).get("id"),
            "server_name": (v.get("server") or dict()).get("name"),
        } for v in [*volumes_prod_raw, *volumes_dev_raw]
    ]
    pvs_prod_raw = json.loads(cmd(f"{KUBECTL_PROD_COMMAND} get pv -o json").stdout)['items']
    pvs_prod_raw = [{"env":"prod", **pv} for pv in pvs_prod_raw]
    pvs_dev_raw = json.loads(cmd(f"{KUBECTL_DEV_COMMAND} get pv -o json").stdout)['items']
    pvs_dev_raw = [{"env":"dev", **pv} for pv in pvs_dev_raw]
    pvs = [
        {
            "uid": pv["metadata"]["uid"],
            "creation_date": pv["metadata"]["creationTimestamp"],
            "name": pv["metadata"]["name"],
            "size": pv["spec"]["capacity"]["storage"],
            "claim_name": pv["spec"]["claimRef"]["name"],
            "claim_namespace": pv["spec"]["claimRef"]["namespace"],
            "volume_id": pv["spec"]["csi"]["volumeHandle"].removeprefix("fr-par-1/"),
            "reclaim_policy": pv["spec"]["persistentVolumeReclaimPolicy"],
            "status": pv["status"]["phase"],
            "env": pv["env"],
        } for pv in [*pvs_prod_raw, *pvs_dev_raw]
    ]

    headers_volumes = list(volumes[0].keys())
    headers_pvs = list(pvs[0].keys())

    db = sqlite3.connect(DB_NAME)
    persist_data(db, "volumes", volumes)
    persist_data(db, "pvs", pvs)
    db.close()

load_data()

db = sqlite3.connect(DB_NAME)

query = """
select name
from volumes
where server_name is null
"""
for name, in db.execute(query):
    print(f"Scaleway volume {name} is not linked to any server")

query = """
select v.name
from volumes v
left join pvs p
on v.id = p.volume_id
where p.uid is null
and v.name like '%_pvc-%'
"""
for name, in db.execute(query):
    print(f"Scaleway volume {name} is not linked to any PV")

query = """
select p.name
from pvs p
left join volumes v
on p.volume_id = v.id
where v.id is null
"""
for name, in db.execute(query):
    print(f"PV {name} is not linked to any Scaleway volume")

query = """
select name
from pvs
where status != 'Bound'
"""
for name, in db.execute(query):
    print(f"PV {name} is not bound")
