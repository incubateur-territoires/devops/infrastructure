module "cluster_production" {
  providers = {
    scaleway.iam             = scaleway.iam
    scaleway.default_project = scaleway.default_project
  }
  source                        = "./clusters/production"
  dns_zone_incubateur           = local.dns_zone_incubateur
  scaleway_cert_manager_api_key = scaleway_iam_api_key.cert_manager

  scaleway_organization_id        = var.scaleway_organization_id
  scaleway_default_project_config = local.scaleway_default_project_config
  scaleway_project_config         = local.scaleway_cluster_production_project_config

  outline_postgresql          = var.production_tools_outline_postgresql
  outline_client_id           = var.production_tools_outline_client_id
  outline_client_secret       = var.production_tools_outline_client_secret
  outline_email_smtp_host     = var.brevo_smtp_host
  outline_email_smtp_user     = var.production_tools_outline_email_smtp_user
  outline_email_smtp_password = var.production_tools_outline_email_smtp_password

  decidim_inclusion_numerique_smtp_user     = var.production_tools_decidim_inclusion_numerique_smtp_user
  decidim_inclusion_numerique_smtp_password = var.production_tools_decidim_inclusion_numerique_smtp_password

  decidim_nfnv_smtp_user     = var.production_tools_decidim_nfnv_smtp_user
  decidim_nfnv_smtp_password = var.production_tools_decidim_nfnv_smtp_password

  sendinblue_smtp_host     = var.brevo_smtp_host
  sendinblue_smtp_user     = var.sendinblue_smtp_user
  sendinblue_smtp_password = var.sendinblue_smtp_password
  sendinblue_smtp_port     = var.sendinblue_smtp_port
}

module "cluster_development" {
  providers = {
    scaleway.iam             = scaleway.iam
    scaleway.default_project = scaleway.default_project
  }
  source                        = "./clusters/development"
  dns_zone_incubateur           = local.dns_zone_incubateur
  scaleway_cert_manager_api_key = scaleway_iam_api_key.cert_manager

  scaleway_organization_id        = var.scaleway_organization_id
  scaleway_default_project_config = local.scaleway_default_project_config
  scaleway_project_config         = local.scaleway_cluster_development_project_config

  sendinblue_smtp_host     = var.brevo_smtp_host
  sendinblue_smtp_user     = var.sendinblue_smtp_user
  sendinblue_smtp_password = var.sendinblue_smtp_password
  sendinblue_smtp_port     = var.sendinblue_smtp_port

  outline_client_id     = var.development_tools_outline_client_id
  outline_client_secret = var.development_tools_outline_client_secret
  outline_oauth_domain  = module.cluster_production.mattermost_hostname
}
