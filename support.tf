resource "scaleway_domain_record" "support_email_dkim" {
  provider = scaleway.dns
  dns_zone = local.dns_zone_incubateur
  name     = "dimail._domainkey"
  type     = "TXT"
  data     = "v=DKIM1; h=sha256; k=rsa; p=MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAwloqQEl7vqc93yhwwH01QVRhjOx2JJ/pYGS+o1lKDhdMDgWauVJTMLeFLx5aiflAV7SLY/llV/KLEoo+QXLmH+2Epm7pe9ZA0L8dvcgybKIZ1j7KnDrFVxFeuHFW0YBNixHBY/6+i5t6lWVMSMFbAMBTphBjmI1v5nHZ3x8MJk5MvVv/2XRH+YqUPCXpDN1k+GkdevimS1XI+cx0V621o1nFyytPy08ypQsyUXy5zlnfus2M2jA7VKRfUo2261WgrjfGM+banQAeAWuoohB5Le8FLhGlODn/9JnpizWtbNYX3RZyZTXeHBEn4ISSJ6GVO+V/qpznDC6YkqkgxFMwpXuwzrBIIt7BmJnqe7vhSv7W286gdCnjNnQSce/lYTEE0x7jp0Mw87ZpBhf+IHCIXk8uNFCQeSWpAhh7bMv9+Lc9Wb6vMfV421+8hdN9330NEGBhlDXGOAccCr5Rzz8R+6aCimy4MZGCZXyX3gtT6eGNDVdDpzcZ8OLBilp0dOUMP+LaOyb/0fjthVk0R2KPMtQO1MZ2X26sI9REQdsbVLZ7nKKl8+6ppMWKX2EByM24qwRXZv5LhA/H3goh0srBUcOuAB1pshrqXPWVL2wYCmvjr1Ch8hCO3I2/SShWgn7V3e9bln+qCOiZz30K69TtP/KJMmT4J28/BEXac9PsQjUCAwEAAQ=="
  ttl      = 60
}
resource "scaleway_domain_record" "support_email_dmarc" {
  provider = scaleway.dns
  dns_zone = local.dns_zone_incubateur
  name     = "_dmarc"
  type     = "TXT"
  data     = "v=DMARC1;p=quarantine;adkim=s;aspf=s"
  ttl      = 60
}
resource "scaleway_domain_record" "support_email_ip4" {
  provider = scaleway.dns
  dns_zone = local.dns_zone_incubateur
  name     = "webmail"
  type     = "CNAME"
  data     = "webmail.ox.numerique.gouv.fr."
  ttl      = 60
}
resource "scaleway_domain_record" "support_email_mx" {
  provider = scaleway.dns
  dns_zone = local.dns_zone_incubateur
  type     = "MX"
  data     = "mx.ox.numerique.gouv.fr."
  ttl      = 60
}
resource "scaleway_domain_record" "support_email_imap" {
  provider = scaleway.dns
  dns_zone = local.dns_zone_incubateur
  name     = "imap"
  type     = "CNAME"
  data     = "imap.ox.numerique.gouv.fr."
  ttl      = 60
}
resource "scaleway_domain_record" "support_email_smtp" {
  provider = scaleway.dns
  dns_zone = local.dns_zone_incubateur
  name     = "smtp"
  type     = "CNAME"
  data     = "smtp.ox.numerique.gouv.fr."
  ttl      = 60
}
