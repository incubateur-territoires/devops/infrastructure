variable "scaleway_iam_access_key" {
  type      = string
  sensitive = true
}
variable "scaleway_iam_secret_key" {
  type      = string
  sensitive = true
}

variable "scaleway_organization_id" {
  type = string
}

variable "production_base_domain" {
  type = string
}

variable "development_base_domain" {
  type = string
}

variable "production_tools_outline_postgresql" {
  type = string
}
variable "production_tools_outline_client_id" {
  type = string
}
variable "production_tools_outline_client_secret" {
  type      = string
  sensitive = true
}

variable "development_tools_outline_client_id" {
  type = string
}
variable "development_tools_outline_client_secret" {
  type      = string
  sensitive = true
}

variable "production_tools_outline_email_smtp_user" {
  type = string
}
variable "production_tools_outline_email_smtp_password" {
  type      = string
  sensitive = true
}

variable "production_tools_decidim_inclusion_numerique_smtp_user" {
  type = string
}
variable "production_tools_decidim_inclusion_numerique_smtp_password" {
  type      = string
  sensitive = true
}

variable "production_tools_decidim_nfnv_smtp_user" {
  type = string
}
variable "production_tools_decidim_nfnv_smtp_password" {
  type      = string
  sensitive = true
}

variable "brevo_smtp_host" {
  type = string
}
variable "sendinblue_smtp_user" {
  type = string
}
variable "sendinblue_smtp_password" {
  type      = string
  sensitive = true
}
variable "sendinblue_smtp_port" {
  type = string
}

variable "ovh_application_key" {
  type      = string
  sensitive = true
}
variable "ovh_application_secret" {
  type      = string
  sensitive = true
}
variable "ovh_consumer_key" {
  type      = string
  sensitive = true
}

variable "scaleway_iam_admin_group_id" {
  type = string
}
