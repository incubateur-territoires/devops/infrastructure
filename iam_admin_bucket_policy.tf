# used to configure bucket policies with the user_ids attribute
# in order for admins to still be able to access any bucket
data "scaleway_iam_group" "admin" {
  provider = scaleway.iam
  group_id = var.scaleway_iam_admin_group_id
}
