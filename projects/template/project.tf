locals {
  project_name = REPLACE_WITH_PROJECT_NAME
  # tflint-ignore: terraform_unused_declarations
  project_slug = REPLACE_WITH_PROJECT_SLUG
}
module "project" {
  source               = "../generic"
  common               = var.common
  project_name         = local.project_name
  with_tf_project      = false
  with_record          = false
  with_wildcard_record = false
  with_scw_project     = false
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.default = scaleway.default
  }
}
