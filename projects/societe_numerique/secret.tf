resource "scaleway_secret" "app_deploy_api_key" {
  provider = scaleway.project
  name     = "app_deploy_api_key"
}
resource "scaleway_secret_version" "app_deploy_api_key" {
  provider  = scaleway.project
  secret_id = scaleway_secret.app_deploy_api_key.id
  data = jsonencode({
    project_id = module.project.scaleway_project_config.project_id
    access_key = module.societe_numerique_deploy_api_key.access_key
    secret_key = module.societe_numerique_deploy_api_key.secret_key
  })
}

resource "scaleway_secret" "app_runtime_api_key" {
  provider = scaleway.project
  name     = "app_runtime_api_key"
}
resource "scaleway_secret_version" "app_runtime_api_key" {
  provider  = scaleway.project
  secret_id = scaleway_secret.app_runtime_api_key.id
  data = jsonencode({
    project_id = module.project.scaleway_project_config.project_id
    access_key = module.societe_numerique_runtime_api_key.access_key
    secret_key = module.societe_numerique_runtime_api_key.secret_key
  })
}
