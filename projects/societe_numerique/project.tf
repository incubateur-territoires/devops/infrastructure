module "project" {
  source               = "../generic"
  common               = var.common
  project_name         = "Societe Numerique"
  startup_group        = "societenumerique"
  with_record          = false
  with_wildcard_record = false
  with_scw_project     = true
  with_tf_project      = false
  scw_custom_groups = {
  }
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.default = scaleway.default
  }
}
