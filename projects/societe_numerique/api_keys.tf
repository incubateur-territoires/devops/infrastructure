module "societe_numerique_runtime_api_key" {
  source     = "../../modules/scw_api_key"
  project_id = module.project.scaleway_project_config.project_id
  permissions = [
    "ObjectStorageObjectsRead",
    "ObjectStorageObjectsWrite",
    "ObjectStorageObjectsDelete",
  ]
  name        = "Societe numerique runtime API key"
  description = "Permissions required for application runtime"
  providers = {
    scaleway.iam = scaleway.iam
  }
}

module "societe_numerique_deploy_api_key" {
  source     = "../../modules/scw_api_key"
  project_id = module.project.scaleway_project_config.project_id
  permissions = [
    "ObjectStorageFullAccess",
    "ContainerRegistryFullAccess",
    "ContainersFullAccess",
    "ServerlessSQLDatabaseFullAccess",
  ]
  name        = "Societe numerique deploy API key"
  description = "Permissions required for application deployment"
  providers = {
    scaleway.iam = scaleway.iam
  }
}
