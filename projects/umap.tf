resource "scaleway_domain_record" "umap" {
  provider = scaleway.dns
  dns_zone = var.dns_zone_incubateur
  name     = "umap"
  type     = "CNAME"
  data     = var.production_cluster_cname
}

resource "scaleway_domain_record" "umap_zammad" {
  provider = scaleway.dns
  dns_zone = var.dns_zone_incubateur
  name     = "doc.umap"
  type     = "ALIAS"
  data     = "uma-zammad.anct.cloud-ed.fr."
}
