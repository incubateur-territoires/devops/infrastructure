resource "scaleway_domain_zone" "project" {
  provider   = scaleway.dns
  project_id = module.project.scaleway_project_config.project_id
  domain     = var.common.dns_zone_incubateur
  subdomain  = "agence.dev"
}
locals {
  dns_zone = "${scaleway_domain_zone.project.subdomain}.${scaleway_domain_zone.project.domain}"
}

resource "scaleway_domain_record" "cname" {
  provider = scaleway.project
  dns_zone = local.dns_zone
  type     = "ALIAS"
  data     = "site-web-anct-dev.osc-fr1.scalingo.io."
}

resource "scaleway_domain_record" "dev_edge_bucket" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "swa.dev"
  type     = "CNAME"
  data     = "d2c04e3d-5ce0-4194-80b8-aeb20549d393.svc.edge.scw.cloud."
  ttl      = 60
}
