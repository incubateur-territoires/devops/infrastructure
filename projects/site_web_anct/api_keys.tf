module "tem_api_key" {
  source      = "../../modules/scw_api_key"
  name        = "${local.project_slug} TEM key"
  project_id  = module.project.scaleway_project_config.project_id
  description = "Used by the ${local.project_name} application to send mail using scaleway's TEM"
  permissions = ["TransactionalEmailEmailFullAccess"]
  providers = {
    scaleway.iam = scaleway.iam
  }
}
