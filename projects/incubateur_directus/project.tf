module "project" {
  source       = "../generic"
  common       = var.common
  project_name = "Incubateur Directus"

  with_record          = false
  with_wildcard_record = false
  with_scw_project     = true
  with_tf_project      = false
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.default = scaleway.default
  }
}
