locals {
  dns_zone_incubateur = "incubateur.anct.gouv.fr"
  domain_name         = "directus"
  fqdn                = "${local.domain_name}.${local.dns_zone_incubateur}"
}
resource "scaleway_domain_record" "directus" {
  provider = scaleway.dns
  dns_zone = local.dns_zone_incubateur
  name     = local.domain_name
  type     = "CNAME"
  data     = "${scaleway_container.directus.domain_name}."
  ttl      = 60
}
resource "scaleway_domain_record" "ddirectus" {
  provider = scaleway.dns
  dns_zone = local.dns_zone_incubateur
  name     = "ddirectus"
  type     = "CNAME"
  data     = "${scaleway_container.directus.domain_name}."
  ttl      = 60
}
