module "directus_s3_api_key" {
  source      = "../../modules/scw_api_key"
  name        = "Directus incubateur S3"
  description = "Used by directus to access object bucket for file storage"
  permissions = ["ObjectStorageReadOnly", "ObjectStorageObjectsRead", "ObjectStorageObjectsWrite", "ObjectStorageObjectsDelete"]
  project_id  = module.project.scaleway_project_config.project_id
  providers = {
    scaleway.iam = scaleway.iam
  }
}
