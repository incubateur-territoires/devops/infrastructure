locals {
  scaleway_ips = [
    "62.210.0.0/16",
    "195.154.0.0/16",
    "212.129.0.0/18",
    "62.4.0.0/19",
    "212.83.128.0/19",
    "212.83.160.0/19",
    "212.47.224.0/19",
    "163.172.0.0/16",
    "51.15.0.0/16",
    "151.115.0.0/16",
    "51.158.0.0/15",
  ]
}
resource "scaleway_rdb_instance" "directus" {
  provider  = scaleway.project
  name      = "directus"
  node_type = "DB-GP-XS"
  engine    = "PostgreSQL-15"

  volume_type       = "bssd"
  volume_size_in_gb = 10

  backup_schedule_frequency = 24
  backup_schedule_retention = 30

  is_ha_cluster = true
}
resource "scaleway_rdb_acl" "directus" {
  provider    = scaleway.project
  instance_id = scaleway_rdb_instance.directus.id
  dynamic "acl_rules" {
    for_each = local.scaleway_ips
    content {
      ip          = acl_rules.value
      description = "Scaleway IP range (managed by terraform)"
    }
  }
}

resource "random_password" "directus_db" {
  length = 64
}
resource "scaleway_rdb_user" "directus" {
  provider    = scaleway.project
  instance_id = scaleway_rdb_instance.directus.id
  name        = "directus"
  is_admin    = true
  password    = random_password.directus_db.result
}
resource "scaleway_rdb_user" "directus_read_only" {
  provider    = scaleway.project
  instance_id = scaleway_rdb_instance.directus.id
  name        = "directus-read-only"
  password    = random_password.directus_db.result
}

resource "scaleway_object_bucket" "directus" {
  provider = scaleway.project
  name     = "incubateur-directus-files"
}
resource "scaleway_object_bucket_acl" "directus" {
  provider = scaleway.project
  bucket   = scaleway_object_bucket.directus.name
  acl      = "public-read"
}

resource "scaleway_container_namespace" "directus" {
  provider = scaleway.project
  name     = "directus"
}
# imported manually
resource "random_password" "smtp_user" {
  length = 30
  lifecycle {
    ignore_changes = all
  }
}
resource "random_password" "smtp_key" {
  length = 90
  lifecycle {
    ignore_changes = all
  }
}
resource "scaleway_container" "directus" {
  provider     = scaleway.project
  name         = "directus"
  namespace_id = scaleway_container_namespace.directus.id

  cpu_limit    = 2240
  memory_limit = 1024

  registry_image = "docker.io/directus/directus:10.11.2"
  http_option    = "redirected"
  deploy         = true
  port           = 8055
  environment_variables = {
    PUBLIC_URL                = "https://${local.fqdn}"
    DB_CLIENT                 = "pg"
    DB_PORT                   = scaleway_rdb_instance.directus.load_balancer[0].port
    DB_DATABASE               = "directus"
    DB_HOST                   = scaleway_rdb_instance.directus.load_balancer[0].ip
    DB_USER                   = scaleway_rdb_user.directus.name
    CORS_ENABLED              = "true"
    CORS_ORIGIN               = "true"
    EMAIL_FROM                = "directus@incubateur.anct.gouv.fr"
    EMAIL_TRANSPORT           = "smtp"
    EMAIL_SMTP_HOST           = "smtp-relay.brevo.com"
    EMAIL_SMTP_PORT           = "587"
    EMAIL_SMTP_SECURE         = "false"
    STORAGE_LOCATIONS         = "scaleway"
    STORAGE_SCALEWAY_DRIVER   = "s3"
    STORAGE_SCALEWAY_BUCKET   = scaleway_object_bucket.directus.name
    STORAGE_SCALEWAY_ENDPOINT = "s3.fr-par.scw.cloud"
    STORAGE_SCALEWAY_REGION   = scaleway_object_bucket.directus.region
  }
  secret_environment_variables = {
    KEY                     = random_password.directus_key.result
    SECRET                  = random_password.directus_secret.result
    DB_PASSWORD             = random_password.directus_db.result
    STORAGE_SCALEWAY_KEY    = module.directus_s3_api_key.access_key
    STORAGE_SCALEWAY_SECRET = module.directus_s3_api_key.secret_key
    EMAIL_SMTP_USER         = random_password.smtp_user.result
    EMAIL_SMTP_PASSWORD     = random_password.smtp_key.result
  }
}
resource "scaleway_container_domain" "directus" {
  provider     = scaleway.project
  container_id = scaleway_container.directus.id
  hostname     = scaleway_domain_record.directus.fqdn
}
resource "scaleway_container_domain" "ddirectus" {
  provider     = scaleway.project
  container_id = scaleway_container.directus.id
  hostname     = "ddirectus.incubateur.anct.gouv.fr"
}

resource "random_password" "directus_secret" {
  length  = 128
  special = false
  upper   = false
  lifecycle {
    ignore_changes = all
  }
}
resource "random_password" "directus_key" {
  length  = 128
  special = false
  upper   = false
  lifecycle {
    ignore_changes = all
  }
}
