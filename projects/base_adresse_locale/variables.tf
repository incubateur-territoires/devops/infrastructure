variable "common" {
  type = map(string)
}

variable "iam_admin_user_ids" {
  type = list(string)
}
