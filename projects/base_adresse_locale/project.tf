module "project" {
  source           = "../generic"
  common           = var.common
  project_name     = "Base adresse locale"
  with_scw_project = true
  startup_group    = "base-adresse-locale"
  scw_custom_groups = {
    "dev"            = ["AllProductsFullAccess"]
    "only-instances" = ["SSHKeysFullAccess", "InstancesFullAccess", "ObjectStorageReadOnly"]
  }
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.default = scaleway.default
  }
}
