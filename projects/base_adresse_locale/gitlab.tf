resource "gitlab_project_variable" "bucket_policy_admin_principal" {
  project = module.project.gitlab_project_id
  key     = "TF_VAR_bucket_policy_admin_principal"
  value = jsonencode({
    SCW : concat(
      [for uid in var.iam_admin_user_ids : "user_id:${uid}"],
      ["application_id:${module.project.admin_application_id}"]
    )
  })
}

resource "gitlab_project_variable" "bucket_policy_app_principal" {
  project = module.project.gitlab_project_id
  key     = "TF_VAR_bucket_policy_app_principal"
  value = jsonencode({
    SCW : [
      "application_id:${module.bucket_api_key.application_id}"
    ]
  })
}

resource "gitlab_project_variable" "bucket_policy_dev_principal" {
  project = module.project.gitlab_project_id
  key     = "TF_VAR_bucket_policy_dev_principal"
  value = jsonencode({
    SCW : concat(
      [for uid in module.project.scaleway_iam_custom_groups.dev.user_ids : "user_id:${uid}"],
    )
  })
}
