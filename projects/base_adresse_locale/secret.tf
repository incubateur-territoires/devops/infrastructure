resource "scaleway_secret" "bucket_credentials" {
  provider = scaleway.project
  name     = "bucket-credentials"
}
resource "scaleway_secret_version" "bucket_credentials" {
  provider  = scaleway.project
  secret_id = scaleway_secret.bucket_credentials.id
  data = jsonencode({
    access_key = module.bucket_api_key.access_key
    secret_key = module.bucket_api_key.secret_key
  })
}
