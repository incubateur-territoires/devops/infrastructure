module "bucket_api_key" {
  source      = "../../modules/scw_api_key"
  project_id  = module.project.scaleway_project_config.project_id
  permissions = ["ObjectStorageObjectsRead", "ObjectStorageObjectsWrite", "ObjectStorageBucketsRead"]
  name        = "Base adresse locale bucket access"
  description = "Permission to read and write bucket objects"
  providers = {
    scaleway.iam = scaleway.iam
  }
}
