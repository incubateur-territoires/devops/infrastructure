resource "scaleway_domain_record" "dev_poc" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "poc.bal.dev"
  type     = "CNAME"
  data     = "tmppoco7xt5t6c-poc-proxy.functions.fnc.fr-par.scw.cloud."
}

resource "scaleway_domain_record" "zammad" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "zammad.base-adresse-locale"
  type     = "ALIAS"
  data     = "bases-addresses-locales-zammad.anct.cloud-ed.fr."
}

resource "scaleway_domain_record" "lab" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "lab.base-adresse-locale"
  type     = "ALIAS"
  data     = "bal-lab.osc-fr1.scalingo.io."
}
