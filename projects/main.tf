module "base_adresse_locale" {
  source             = "./base_adresse_locale"
  common             = local.common
  iam_admin_user_ids = var.iam_admin_user_ids
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.dns     = scaleway.dns
    scaleway.default = scaleway.default
  }
}

module "agents_en_intervention" {
  source = "./agents_en_intervention"
  common = local.common
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.dns     = scaleway.dns
    scaleway.default = scaleway.default
  }
}

module "ansm" {
  source = "./ansm"
  common = local.common
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.dns     = scaleway.dns
    scaleway.default = scaleway.default
  }
}

module "incubateur_directus" {
  source = "./incubateur_directus"
  common = local.common
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.dns     = scaleway.dns
    scaleway.default = scaleway.default
  }
}

module "monsuivisocial" {
  source = "./mon_suivi_social"
  domain = "monsuivisocial.${var.dns_zone_incubateur}"
  common = local.common
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.dns     = scaleway.dns
    scaleway.default = scaleway.default
  }
}

module "societe_numerique" {
  source = "./societe_numerique"
  common = local.common
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.dns     = scaleway.dns
    scaleway.default = scaleway.default
  }
}

module "suite_territoriale" {
  source                          = "./suite_territoriale"
  common                          = local.common
  annuaire_collectivite_projet_id = module.annuaire_des_collectivites.project_id
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.dns     = scaleway.dns
    scaleway.default = scaleway.default
  }
}

module "espace_sur_demande" {
  source = "./espace_sur_demande"
  common = local.common
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.dns     = scaleway.dns
    scaleway.default = scaleway.default
  }
}

module "donnees_et_territoires" {
  source = "./donnees_territoires"

  old_project_slug = "donneesetterritoires"
  project_slug     = "donnees"
  project_name     = "DonneesEtTerritoires"

  kubeconfig_development = var.kubeconfig_development
  kubeconfig_production  = var.kubeconfig_production

  common = local.common
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.dns     = scaleway.dns
    scaleway.default = scaleway.default
  }
}

module "cal_com" {
  source = "./cal_com"
  common = local.common
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.dns     = scaleway.dns
    scaleway.default = scaleway.default
  }
}

module "site_web_anct" {
  source = "./site_web_anct"
  common = local.common
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.dns     = scaleway.dns
    scaleway.default = scaleway.default
  }
}

module "mon_espace_collectivite" {
  source = "./mon_espace_collectivite"
  common = local.common
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.dns     = scaleway.dns
    scaleway.default = scaleway.default
  }
}

module "barnabe" {
  source = "./barnabe"
  common = local.common
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.dns     = scaleway.dns
    scaleway.default = scaleway.default
  }
}

module "umap" {
  source = "./umap"
  common = local.common
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.dns     = scaleway.dns
    scaleway.default = scaleway.default
  }
}

module "mattermost" {
  source                = "./mattermost"
  common                = local.common
  rdb_acl               = var.mattermost_rdb_acl
  kubeconfig_production = var.kubeconfig_production
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.dns     = scaleway.dns
    scaleway.default = scaleway.default
  }
}

module "poc_keycloak_serverless" {
  source = "./poc_keycloak_serverless"
  common = local.common
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.dns     = scaleway.dns
    scaleway.default = scaleway.default
  }
}

module "poc_keycloak_docker_compose" {
  source = "./poc_keycloak_docker_compose"
  common = local.common
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.dns     = scaleway.dns
    scaleway.default = scaleway.default
  }
}

module "pcrs" {
  source = "./pcrs"
  common = local.common
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.dns     = scaleway.dns
    scaleway.default = scaleway.default
  }
}

module "infrastructures_numeriques" {
  source = "./infrastructures_numeriques"
  common = local.common
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.dns     = scaleway.dns
    scaleway.default = scaleway.default
  }
}

module "annuaire_des_collectivites" {
  source = "./annuaire_des_collectivites"
  common = local.common

  scaleway_default_project_id = var.scaleway_default_project_id
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.dns     = scaleway.dns
    scaleway.default = scaleway.default
  }
}

module "dataspace_sonum_project" {
  source = "./dataspace_sonum"
  common = local.common
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.dns     = scaleway.dns
    scaleway.default = scaleway.default
  }
}
