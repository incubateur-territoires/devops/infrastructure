resource "scaleway_domain_record" "administration_plus_zammad" {
  provider = scaleway.dns
  dns_zone = var.dns_zone_incubateur
  name     = "zammad.aplus"
  type     = "ALIAS"
  data     = "administration-plus-zammad.anct.cloud-ed.fr."
}
