terraform {
  required_providers {
    scaleway = {
      source = "scaleway/scaleway"
      configuration_aliases = [
        scaleway.default,
        scaleway.iam,
        scaleway.dns,
      ]
    }
    grafana = {
      source = "grafana/grafana"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }
}
