variable "project_name" {
  type        = string
  description = "Full human readable name of the project."
}

variable "project_slug" {
  type        = string
  default     = null
  description = "Slug to use for fields where the human readable name is not allowed."
}

variable "project_subdomain" {
  type        = string
  default     = null
  description = "Override subdomain used by the project. By default it's the project slug"
}

variable "with_tf_project" {
  type        = bool
  default     = true
  description = "To create a terraform repo on gitlab's devops projects group for this project"
}
variable "with_scw_project" {
  type        = bool
  default     = false
  description = "To create a project on Scaleway"
}
variable "with_cluster_secrets" {
  type        = bool
  default     = true
  description = "Whether to add API keys to fully access K8S clusters Scaleway projects or not"
}
variable "scw_custom_groups" {
  type        = map(list(string))
  default     = {}
  description = "Custom groups to create with specific project resources access. Specify a map where each key is use for the group's name and policy, and a list of Scaleway IAM permissions."
}
variable "with_record" {
  type        = bool
  default     = true
  description = "To create domain records pointing to kubernetes' clusters"
}
variable "with_wildcard_record" {
  type        = bool
  default     = true
  description = "To create wildcard domain records pointing to kubernetes' clusters"
}
variable "record_ttl" {
  type    = number
  default = 3600
}
variable "startup_group" {
  type        = string
  default     = null
  description = "The ID or full path of the startup's gitlab group"
}
# alternative to startup groups, configure a single project. Startup groups take precedence
variable "gl_project_to_configure" {
  type        = string
  default     = null
  description = "The ID of full path of the gitlab project that should be configure using the devops repo"
}

variable "common" {
  type = object({
    projects_group_id                       = number
    dns_zone_incubateur                     = string
    production_cluster_cname                = string
    development_cluster_cname               = string
    development_base_domain                 = string
    production_base_domain                  = string
    scaleway_organization_id                = string
    scaleway_cluster_development_cluster_id = string
    scaleway_cluster_production_cluster_id  = string
    grafana_production_url                  = string
    grafana_production_auth                 = string
    grafana_production_loki_url             = string
    grafana_production_prometheus_url       = string
    grafana_development_url                 = string
    grafana_development_auth                = string
    grafana_development_loki_url            = string
    grafana_development_prometheus_url      = string
    scaleway_development_public_gw_ip       = string
    scaleway_production_public_gw_ip        = string
    scaleway_development_application_id     = string
    scaleway_production_application_id      = string
    scaleway_development_project_id         = string
    scaleway_production_project_id          = string
  })
}

locals {
  project_name      = var.project_name
  project_slug      = var.project_slug == null ? lower(replace(local.project_name, " ", "-")) : var.project_slug
  project_subdomain = var.project_subdomain == null ? lower(replace(local.project_slug, " ", "-")) : var.project_subdomain
}
