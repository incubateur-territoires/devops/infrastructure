locals {
  with_group_token   = var.startup_group != null && var.with_tf_project
  startup_group_full = var.startup_group == null ? null : "incubateur-territoires/startups/${var.startup_group}"
}
# create a group access token so the project's tf repo can configure repositories in the startup group
resource "gitlab_group_access_token" "terraform" {
  count        = local.with_group_token ? 1 : 0
  group        = local.startup_group_full
  name         = "terraform"
  access_level = "maintainer"
  scopes       = ["api"]
  rotation_configuration = {
    expiration_days    = 365
    rotate_before_days = 30
  }
}

resource "gitlab_project_variable" "gitlab_group_access_token" {
  count   = local.with_group_token ? 1 : 0
  project = module.gitlab_project[0].project_id
  key     = "TF_VAR_gitlab_token"
  value   = gitlab_group_access_token.terraform[0].token
}
