provider "grafana" {
  alias = "prod"
  url   = var.common.grafana_production_url
  auth  = var.common.grafana_production_auth
}

provider "grafana" {
  alias = "dev"
  url   = var.common.grafana_development_url
  auth  = var.common.grafana_development_auth
}
