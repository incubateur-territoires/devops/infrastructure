# project token when the project doesn't have a startup group
# this is the case for incubator projects (website, licence...)
locals {
  # configuring the startup group takes precedence
  gl_project_token_check = var.startup_group == null && var.gl_project_to_configure != null
}

resource "gitlab_project_access_token" "terraform" {
  count        = local.gl_project_token_check ? 1 : 0
  project      = var.gl_project_to_configure
  name         = "terraform"
  access_level = "maintainer"
  scopes       = ["api"]
  rotation_configuration = {
    expiration_days    = 365
    rotate_before_days = 30
  }
}

resource "gitlab_project_variable" "gitlab_project_access_token" {
  count   = local.gl_project_token_check ? 1 : 0
  project = module.gitlab_project[0].project_id
  key     = "TF_VAR_gitlab_token"
  value   = gitlab_project_access_token.terraform[0].token
}
