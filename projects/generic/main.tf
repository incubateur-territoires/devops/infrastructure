locals {
  tf_project_switch = var.with_tf_project ? 1 : 0
}
module "grafana_production" {
  count        = local.tf_project_switch
  source       = "../../modules/grafana_api_creation"
  project_name = local.project_name
  providers = {
    grafana = grafana.prod
  }
}

module "grafana_development" {
  count        = local.tf_project_switch
  source       = "../../modules/grafana_api_creation"
  project_name = local.project_name
  providers = {
    grafana = grafana.dev
  }
}

module "gitlab_project" {
  count   = local.tf_project_switch
  source  = "gitlab.com/vigigloo/tf-modules/gitlabterraformproject"
  version = "0.6.1"

  gitlab_project_location = var.common.projects_group_id
  project_name            = local.project_name
  project_slug            = local.project_slug

  dev_base-domain  = "${local.project_subdomain}.${var.common.development_base_domain}"
  prod_base-domain = "${local.project_subdomain}.${var.common.production_base_domain}"

  no_gitlab_tokens = true

  scaleway_organization_id                = var.common.scaleway_organization_id
  scaleway_cluster_development_cluster_id = var.common.scaleway_cluster_development_cluster_id
  scaleway_cluster_production_cluster_id  = var.common.scaleway_cluster_production_cluster_id
}

resource "gitlab_project_variable" "grafana_production" {
  count   = local.tf_project_switch
  project = module.gitlab_project[0].project_id
  key     = "TF_VAR_grafana_production_config"
  value = jsonencode({
    url            = var.common.grafana_production_url,
    api_key        = module.grafana_production[0].api_key,
    org_id         = module.grafana_production[0].org_id,
    loki_url       = var.common.grafana_production_loki_url,
    prometheus_url = var.common.grafana_production_prometheus_url,
  })
}
resource "gitlab_project_variable" "grafana_development" {
  count   = local.tf_project_switch
  project = module.gitlab_project[0].project_id
  key     = "TF_VAR_grafana_development_config"
  value = jsonencode({
    url            = var.common.grafana_development_url,
    api_key        = module.grafana_development[0].api_key,
    org_id         = module.grafana_development[0].org_id,
    loki_url       = var.common.grafana_development_loki_url,
    prometheus_url = var.common.grafana_development_prometheus_url,
  })
}

resource "gitlab_project_variable" "development_public_gw_ip" {
  count   = local.tf_project_switch
  project = module.gitlab_project[0].project_id
  key     = "TF_VAR_k8s_development_public_gw_ip"
  value   = var.common.scaleway_development_public_gw_ip
}
resource "gitlab_project_variable" "production_public_gw_ip" {
  count   = local.tf_project_switch
  project = module.gitlab_project[0].project_id
  key     = "TF_VAR_k8s_production_public_gw_ip"
  value   = var.common.scaleway_production_public_gw_ip
}

resource "gitlab_project_variable" "common" {
  for_each = local.tf_project_switch == 1 ? {
    scaleway_organization_id = var.common.scaleway_organization_id

    scaleway_cluster_development_project_id = var.common.scaleway_development_project_id
    scaleway_cluster_development_cluster_id = var.common.scaleway_cluster_development_cluster_id

    scaleway_cluster_production_project_id = var.common.scaleway_production_project_id
    scaleway_cluster_production_cluster_id = var.common.scaleway_cluster_production_cluster_id
  } : {}
  project = module.gitlab_project[0].project_id
  key     = "TF_VAR_${each.key}"
  value   = each.value
}

output "var_file" {
  value = var.with_tf_project ? module.gitlab_project[0].var_file : ""
}
output "gitlab_project_id" {
  value = var.with_tf_project ? module.gitlab_project[0].project_id : ""
}
