output "scaleway_project_config" {
  value = {
    project_id = try(scaleway_account_project.project[0].id, null)
    access_key = try(scaleway_iam_api_key.admin_key[0].access_key, null)
    secret_key = try(scaleway_iam_api_key.admin_key[0].secret_key, null)
  }
  sensitive = true
}

output "scaleway_iam_custom_groups" {
  value = scaleway_iam_group.custom_group
}
