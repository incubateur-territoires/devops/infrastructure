terraform {
  required_providers {
    scaleway = {
      source = "scaleway/scaleway"
      configuration_aliases = [
        scaleway.iam,
        scaleway.default,
      ]
    }
    grafana = {
      source = "grafana/grafana"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }
}
