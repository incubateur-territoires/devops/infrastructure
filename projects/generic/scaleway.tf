locals {
  scw_project_switch        = var.with_scw_project ? 1 : 0
  gl_scw_project_var_switch = var.with_scw_project && var.with_tf_project ? 1 : 0
  scw_managed               = "Managed by terraform"
  scw_admin                 = "${local.project_name} admin"
}

# Domain records

resource "scaleway_domain_record" "record" {
  provider = scaleway.default
  count    = var.with_record ? 1 : 0
  dns_zone = var.common.dns_zone_incubateur
  name     = local.project_subdomain
  type     = "CNAME"
  data     = var.common.production_cluster_cname
  ttl      = var.record_ttl
}

resource "scaleway_domain_record" "record_wildcard" {
  provider = scaleway.default
  count    = var.with_wildcard_record ? 1 : 0
  dns_zone = var.common.dns_zone_incubateur
  name     = "*.${local.project_subdomain}"
  type     = "CNAME"
  data     = var.common.production_cluster_cname
  ttl      = var.record_ttl
}

# Scaleway project
resource "scaleway_account_project" "project" {
  provider    = scaleway.iam
  count       = local.scw_project_switch
  name        = local.project_name
  description = local.scw_managed
}

output "scaleway_project" {
  value = var.with_scw_project ? scaleway_account_project.project[0].id : ""
}

# Scaleway admin application
resource "scaleway_iam_application" "project_admin" {
  provider    = scaleway.iam
  count       = local.scw_project_switch
  name        = local.scw_admin
  description = local.scw_managed
}
resource "scaleway_iam_group" "project_admin" {
  provider        = scaleway.iam
  count           = local.scw_project_switch
  name            = local.scw_admin
  description     = local.scw_managed
  application_ids = [scaleway_iam_application.project_admin[0].id]
  lifecycle {
    ignore_changes = [
      user_ids,
    ]
  }
}
resource "scaleway_iam_policy" "project_admin" {
  provider = scaleway.iam
  count    = local.scw_project_switch
  name     = local.scw_admin
  group_id = scaleway_iam_group.project_admin[0].id
  rule {
    project_ids          = [scaleway_account_project.project[0].id]
    permission_set_names = ["AllProductsFullAccess"]
  }
  description = local.scw_managed
}
resource "scaleway_iam_api_key" "admin_key" {
  provider           = scaleway.iam
  count              = local.scw_project_switch
  application_id     = scaleway_iam_application.project_admin[0].id
  description        = local.scw_managed
  default_project_id = scaleway_account_project.project[0].id
}

# Setting admin keys in infrastructure project
resource "gitlab_project_variable" "scaleway_project_config" {
  count   = local.gl_scw_project_var_switch
  project = module.gitlab_project[0].project_id
  key     = "TF_VAR_scaleway_project_config"
  value = jsonencode({
    project_id = scaleway_account_project.project[0].id,
    access_key = scaleway_iam_api_key.admin_key[0].access_key,
    secret_key = scaleway_iam_api_key.admin_key[0].secret_key,
  })
}

# Creating custom groups
resource "scaleway_iam_group" "custom_group" {
  for_each    = var.scw_custom_groups
  provider    = scaleway.iam
  name        = "${local.project_name} ${each.key}"
  description = local.scw_managed
  lifecycle {
    ignore_changes = [
      user_ids,
      application_ids,
    ]
  }
}
resource "scaleway_iam_policy" "custom_group" {
  for_each    = var.scw_custom_groups
  provider    = scaleway.iam
  name        = scaleway_iam_group.custom_group[each.key].name
  description = local.scw_managed
  group_id    = scaleway_iam_group.custom_group[each.key].id
  rule {
    project_ids          = [scaleway_account_project.project[0].id]
    permission_set_names = each.value
  }
}

# Creating API keys for project
resource "scaleway_iam_api_key" "dev_project" {
  provider           = scaleway.iam
  count              = var.with_tf_project && var.with_cluster_secrets ? 1 : 0
  application_id     = var.common.scaleway_development_application_id
  description        = "API key for ${local.project_name} devops project access to k8s dev scw project (${local.scw_managed})"
  default_project_id = var.common.scaleway_development_project_id
}
resource "scaleway_iam_api_key" "prod_project" {
  provider           = scaleway.iam
  count              = var.with_tf_project && var.with_cluster_secrets ? 1 : 0
  application_id     = var.common.scaleway_production_application_id
  description        = "API key for ${local.project_name} devops project access to k8s prod scw project (${local.scw_managed})"
  default_project_id = var.common.scaleway_production_project_id
}
resource "gitlab_project_variable" "scaleway_k8s_config" {
  for_each = var.with_tf_project && var.with_cluster_secrets ? {
    dev_ak = {
      env  = "development"
      name = "access_key"
      val  = scaleway_iam_api_key.dev_project[0].access_key
    }
    dev_sk = {
      env  = "development"
      name = "secret_key"
      val  = scaleway_iam_api_key.dev_project[0].secret_key
    }
    prod_ak = {
      env  = "production"
      name = "access_key"
      val  = scaleway_iam_api_key.prod_project[0].access_key
    }
    prod_sk = {
      env  = "production"
      name = "secret_key"
      val  = scaleway_iam_api_key.prod_project[0].secret_key
    }
  } : {}
  project = module.gitlab_project[0].project_id
  key     = "TF_VAR_scaleway_cluster_${each.value.env}_${each.value.name}"
  value   = each.value.val
}

output "admin_application_id" {
  value = var.with_scw_project ? scaleway_iam_application.project_admin[0].id : null
}
