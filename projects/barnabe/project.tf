locals {
  project_name = "Barnabe"
  project_slug = "barnabe"
}
module "project" {
  source               = "../generic"
  common               = var.common
  project_name         = local.project_name
  with_tf_project      = false
  with_record          = false
  with_wildcard_record = false
  with_scw_project     = true
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.default = scaleway.default
  }
}
