resource "scaleway_object_bucket" "staging" {
  provider = scaleway.project
  name     = "${local.project_slug}-staging"
}
