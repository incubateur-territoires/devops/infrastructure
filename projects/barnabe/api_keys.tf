module "bucket_api_key" {
  source      = "../../modules/scw_api_key"
  project_id  = module.project.scaleway_project_config.project_id
  permissions = ["ObjectStorageObjectsRead", "ObjectStorageObjectsDelete", "ObjectStorageObjectsWrite", "ObjectStorageBucketsRead"]
  name        = "${local.project_name} bucket access"
  description = "Permission for ${local.project_name} to read and write bucket objects"
  providers = {
    scaleway.iam = scaleway.iam
  }
}
