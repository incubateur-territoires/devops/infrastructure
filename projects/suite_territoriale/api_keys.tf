module "dns_api_key" {
  source      = "../../modules/scw_api_key"
  project_id  = module.project.scaleway_project_config.project_id
  permissions = ["DomainsDNSFullAccess"]
  name        = "Suite territoriale DNS admin"
  description = "Permissions to administer DNS records"
  providers = {
    scaleway.iam = scaleway.iam
  }
}
