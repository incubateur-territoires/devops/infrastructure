module "project" {
  source               = "../generic"
  common               = var.common
  project_name         = "Suite Territoriale"
  with_tf_project      = false
  with_scw_project     = true
  with_record          = false
  with_wildcard_record = false
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.default = scaleway.default
  }
}
