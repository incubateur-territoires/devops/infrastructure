resource "scaleway_secret" "dns_api_key" {
  name = "dns_admin_api_key"
}
resource "scaleway_secret_version" "dns_api_key" {
  data = jsonencode({
    project_id = module.project.scaleway_project_config.project_id
    access_key = module.dns_api_key.access_key
    secret_key = module.dns_api_key.secret_key
  })
  secret_id = scaleway_secret.dns_api_key.id
}
