provider "scaleway" {
  region     = "fr-par"
  zone       = "fr-par-1"
  access_key = module.project.scaleway_project_config.access_key
  secret_key = module.project.scaleway_project_config.secret_key
  project_id = module.project.scaleway_project_config.project_id
}
