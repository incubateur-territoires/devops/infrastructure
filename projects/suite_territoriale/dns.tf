resource "scaleway_domain_record" "alias" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "suite-territoriale-prod.dom"
  type     = "ALIAS"
  data     = "lasuite-territoriale.osc-fr1.scalingo.io."
}

# zones created temporarily for testing mail and DNS configuration
# delegated to the project
locals {
  tmp_dns_zone = "collectivite.fr"
}
resource "scaleway_domain_zone" "tmp_city_zone" {
  for_each = toset([
    # webmail interface
    "webmail",
    # cities
    "varzy",
    "loc-eguiner",
    "etreux",
  ])
  provider   = scaleway.dns
  domain     = local.tmp_dns_zone
  subdomain  = each.value
  project_id = var.annuaire_collectivite_projet_id
}

resource "scaleway_domain_zone" "tmp_sub_zone" {
  provider   = scaleway.dns
  domain     = local.tmp_dns_zone
  subdomain  = "m"
  project_id = var.annuaire_collectivite_projet_id
}
