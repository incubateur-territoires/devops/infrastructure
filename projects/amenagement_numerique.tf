locals {
  francethd_domain = "francethd.fr"
}

resource "scaleway_domain_record" "francethd_prod_record" {
  provider = scaleway.dns
  dns_zone = local.francethd_domain
  name     = ""
  type     = "ALIAS"
  data     = local.common.production_cluster_cname
}

resource "scaleway_domain_record" "francethd_prod_record_wildcard" {
  provider = scaleway.dns
  dns_zone = local.francethd_domain
  name     = "*"
  type     = "CNAME"
  data     = local.common.production_cluster_cname
}

resource "scaleway_domain_record" "francethd_dev_record" {
  provider = scaleway.dns
  dns_zone = local.francethd_domain
  name     = "dev"
  type     = "CNAME"
  data     = local.common.development_cluster_cname
}

resource "scaleway_domain_record" "francethd_dev_record_wildcard" {
  provider = scaleway.dns
  dns_zone = local.francethd_domain
  name     = "*.dev"
  type     = "CNAME"
  data     = local.common.development_cluster_cname
}

module "amenagement_numerique" {
  source           = "./generic"
  common           = local.common
  project_name     = "Amenagement numerique"
  with_scw_project = true

  startup_group = "amenagement-numerique"
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.default = scaleway.default
  }
}
