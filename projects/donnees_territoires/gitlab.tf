resource "gitlab_project_variable" "gitlab_pages_zone" {
  project = module.project.gitlab_project_id
  key     = "TF_VAR_gitlab_pages_zone"
  value   = "${scaleway_domain_zone.gitlab_pages.subdomain}.${scaleway_domain_zone.gitlab_pages.domain}"
}
resource "gitlab_project_variable" "donnees_vm_ip" {
  project = module.project.gitlab_project_id
  key     = "TF_VAR_vm_ip"
  value   = scaleway_instance_server.donnees_vm.public_ip
}
