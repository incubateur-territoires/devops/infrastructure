module "development" {
  source                     = "./environment/donnees"
  gitlab_environment_scope   = "development"
  kubeconfig                 = var.kubeconfig_development
  namespace                  = "${var.old_project_slug}-development"
  old_project_slug           = var.old_project_slug
  project_slug               = var.project_slug
  project_name               = var.project_name
  namespace_quota_max_cpu    = 8
  namespace_quota_max_memory = "12Gi"

  additionnal_namespace_role_rules = local.additionnal_namespace_role_rules

  providers = {
    helm       = helm.development
    kubernetes = kubernetes.development
  }
}

module "production" {
  source                     = "./environment/donnees"
  gitlab_environment_scope   = "production"
  kubeconfig                 = var.kubeconfig_production
  namespace                  = var.old_project_slug
  old_project_slug           = var.old_project_slug
  project_slug               = var.project_slug
  project_name               = var.project_name
  namespace_quota_max_cpu    = 8
  namespace_quota_max_memory = "12Gi"

  additionnal_namespace_role_rules = local.additionnal_namespace_role_rules

  providers = {
    helm       = helm.production
    kubernetes = kubernetes.production
  }
}

module "umap_development" {
  source      = "./environment/umap"
  kubeconfig  = var.kubeconfig_development
  base_domain = "umap.dev.incubateur.anct.gouv.fr"
  namespace   = "${var.project_slug}-umap"

  additionnal_namespace_role_rules = local.additionnal_namespace_role_rules

  providers = {
    helm       = helm.development
    kubernetes = kubernetes.development
  }
}

module "umap_production" {
  source      = "./environment/umap"
  kubeconfig  = var.kubeconfig_production
  base_domain = "umap.incubateur.anct.gouv.fr"
  namespace   = "${var.project_slug}-umap"

  additionnal_namespace_role_rules = local.additionnal_namespace_role_rules

  providers = {
    helm       = helm.production
    kubernetes = kubernetes.production
  }
}

module "catalog_development" {
  source    = "./environment/catalog"
  namespace = "${var.project_slug}-catalog-development"

  additionnal_namespace_role_rules = local.additionnal_namespace_role_rules

  providers = {
    helm       = helm.development
    kubernetes = kubernetes.development
  }
}

module "catalog_production" {
  source    = "./environment/catalog"
  namespace = "${var.project_slug}-catalog"

  additionnal_namespace_role_rules = local.additionnal_namespace_role_rules

  providers = {
    helm       = helm.production
    kubernetes = kubernetes.production
  }
}

module "addok" {
  source       = "./environment/addok"
  project_slug = var.old_project_slug

  additionnal_namespace_role_rules = local.additionnal_namespace_role_rules

  providers = {
    helm       = helm.development
    kubernetes = kubernetes.development
  }
}

module "grist_beta" {
  source                         = "./environment/grist"
  project_slug                   = "${var.project_slug}-grist-beta"
  kubeconfig                     = var.kubeconfig_production
  grist_doc_wk_limits_memory_mb  = 6 * 1024
  grist_doc_wk_replicas          = 12
  grist_doc_wk_requests_cpu_m    = 500
  grist_home_wk_limits_memory_mb = 0.5 * 1024
  grist_home_wk_replicas         = 2
  grist_home_wk_requests_cpu_m   = 200

  additionnal_namespace_role_rules = local.additionnal_namespace_role_rules

  providers = {
    helm       = helm.production
    kubernetes = kubernetes.production
  }
}

module "grist_prod" {
  source                         = "./environment/grist"
  kubeconfig                     = var.kubeconfig_production
  project_slug                   = "${var.project_slug}-grist-production"
  override_namespace             = "grist"
  grist_doc_wk_limits_memory_mb  = 6 * 1024
  grist_doc_wk_replicas          = 10
  grist_doc_wk_requests_cpu_m    = 500
  grist_home_wk_limits_memory_mb = 0.5 * 1024
  grist_home_wk_replicas         = 3
  grist_home_wk_requests_cpu_m   = 200

  additionnal_namespace_role_rules = local.additionnal_namespace_role_rules

  providers = {
    helm       = helm.production
    kubernetes = kubernetes.production
  }
}

locals {
  k8s_ns_access = {
    prod = {
      (module.production.namespace.namespace) = {
        host                   = var.kubeconfig_production.host
        cluster_ca_certificate = var.kubeconfig_production.cluster_ca_certificate
        token                  = module.production.namespace.token
        user                   = module.production.namespace.user
      }
      (module.umap_production.namespace.namespace) = {
        host                   = var.kubeconfig_production.host
        cluster_ca_certificate = var.kubeconfig_production.cluster_ca_certificate
        token                  = module.umap_production.namespace.token
        user                   = module.umap_production.namespace.user
      }
      (module.catalog_production.namespace.namespace) = {
        host                   = var.kubeconfig_production.host
        cluster_ca_certificate = var.kubeconfig_production.cluster_ca_certificate
        token                  = module.catalog_production.namespace.token
        user                   = module.catalog_production.namespace.user
      }
      (module.grist_beta.namespace.namespace) = {
        host                   = var.kubeconfig_production.host
        cluster_ca_certificate = var.kubeconfig_production.cluster_ca_certificate
        token                  = module.grist_beta.namespace.token
        user                   = module.grist_beta.namespace.user
      }
      (module.grist_prod.namespace.namespace) = {
        host                   = var.kubeconfig_production.host
        cluster_ca_certificate = var.kubeconfig_production.cluster_ca_certificate
        token                  = module.grist_prod.namespace.token
        user                   = module.grist_prod.namespace.user
      }
    }
    dev = {
      (module.development.namespace.namespace) = {
        host                   = var.kubeconfig_development.host
        cluster_ca_certificate = var.kubeconfig_development.cluster_ca_certificate
        token                  = module.development.namespace.token
        user                   = module.development.namespace.user
      }
      (module.umap_development.namespace.namespace) = {
        host                   = var.kubeconfig_development.host
        cluster_ca_certificate = var.kubeconfig_development.cluster_ca_certificate
        token                  = module.umap_development.namespace.token
        user                   = module.umap_development.namespace.user
      }
      (module.catalog_development.namespace.namespace) = {
        host                   = var.kubeconfig_development.host
        cluster_ca_certificate = var.kubeconfig_development.cluster_ca_certificate
        token                  = module.catalog_development.namespace.token
        user                   = module.catalog_development.namespace.user
      }
      (module.addok.namespace.namespace) = {
        host                   = var.kubeconfig_development.host
        cluster_ca_certificate = var.kubeconfig_development.cluster_ca_certificate
        token                  = module.addok.namespace.token
        user                   = module.addok.namespace.user
      }
    }
  }
}
resource "gitlab_project_variable" "donnees_kubernetes_config" {
  key     = "TF_VAR_kubernetes_config"
  value   = jsonencode(local.k8s_ns_access)
  project = module.project.gitlab_project_id
}
