variable "common" {
  type = map(string)
}

variable "old_project_slug" {
  type = string
}
variable "project_slug" {
  type = string
}

variable "project_name" {
  type = string
}

variable "kubeconfig_development" {
  type = object({
    host                   = string
    token                  = string
    cluster_ca_certificate = string
  })
}
variable "kubeconfig_production" {
  type = object({
    host                   = string
    token                  = string
    cluster_ca_certificate = string
  })
}

locals {
  additionnal_namespace_role_rules = [{
    api_groups = ["postgres-operator.crunchydata.com"]
    resources  = ["*"]
    verbs      = ["*"]
  }]

  donnees_et_territoires_project_fqdn = "${local.project_subdomain}.${var.common.production_base_domain}"
}
