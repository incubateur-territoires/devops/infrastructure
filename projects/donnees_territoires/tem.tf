module "tem" {
  source   = "../../modules/tem/"
  dns_zone = var.common.dns_zone_incubateur
  dns_name = local.project_subdomain
  providers = {
    scaleway.iam        = scaleway.iam
    scaleway.dns        = scaleway.dns
    scaleway.tem_domain = scaleway.project
  }
}
