terraform {
  required_providers {
    scaleway = {
      source = "scaleway/scaleway"
      configuration_aliases = [
        scaleway.iam,
        scaleway.dns,
        scaleway.default,
      ]
    }
    helm = {
      source = "hashicorp/helm"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }
  required_version = ">= 0.14"
}
