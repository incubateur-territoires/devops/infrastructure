resource "scaleway_instance_server" "donnees_vm" {
  provider = scaleway.default
  name     = "donnees-territoires"
  zone     = "fr-par-2"

  type  = "GP1-M"
  image = "fr-par-2/6a3b3183-358f-4426-b29e-9266690f97bf"

  ip_id       = "fr-par-2/e6bfae88-e774-48fb-a41c-8f441df667a4"
  enable_ipv6 = true

  tags = [
    "SMTP_HOST=${module.tem.tem_domain.smtp_host}",
    "SMTP_PORT=${module.tem.tem_domain.smtps_port}",
    "SMTP_PROTOCOL=smtps",
    "SMTP_USERNAME=${module.project.scaleway_project_config.project_id}",
    "SMTP_PASSWORD=${scaleway_iam_api_key.mail.secret_key}",
    "SMTP_SENDER_HOST=${local.donnees_et_territoires_project_fqdn}",

    "CERTBOT_SCALEWAY_ACCESS_KEY=${module.api_key_gitlab_pages_certbot.access_key}",
    "CERTBOT_SCALEWAY_SECRET_KEY=${module.api_key_gitlab_pages_certbot.secret_key}",
  ]

  lifecycle {
    ignore_changes  = [enable_ipv6, ip_id, image]
    prevent_destroy = true
  }
}
