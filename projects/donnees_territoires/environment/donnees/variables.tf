variable "old_project_slug" {
  type = string
}

variable "project_slug" {
  type = string
}

variable "project_name" {
  type = string
}

variable "gitlab_environment_scope" {
  type = string
}
locals {
  environment_slug = var.gitlab_environment_scope == "*" ? "review" : var.gitlab_environment_scope
}

variable "namespace" {
  type = string
}

variable "namespace_quota_max_cpu" {
  type    = number
  default = 2
}

variable "namespace_quota_max_memory" {
  type    = string
  default = "12Gi"
}

variable "kubeconfig" {
  type = object({
    host                   = string
    token                  = string
    cluster_ca_certificate = string
  })
}

variable "additionnal_namespace_role_rules" {
  type = list(map(list(string)))
}
