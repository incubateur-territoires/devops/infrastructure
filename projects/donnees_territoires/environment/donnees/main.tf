module "namespace" {
  source            = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version           = "2.1.0"
  namespace         = var.namespace
  max_cpu_requests  = var.namespace_quota_max_cpu
  max_memory_limits = var.namespace_quota_max_memory
  project_name      = var.project_name
  project_slug      = var.old_project_slug

  additionnal_role_rules = var.additionnal_namespace_role_rules

  default_container_cpu_requests  = "20m"
  default_container_memory_limits = "16Mi"
}

module "kubeconfig_vincentlara" {
  source  = "gitlab.com/vigigloo/tf-modules/generatescopedkubeconfig"
  version = "0.2.0"

  filename               = "donnees-vincentlara-${local.environment_slug}.yml"
  namespace              = module.namespace.namespace
  username               = "vincentlara"
  project_slug           = var.project_slug
  project_name           = var.project_name
  role_name              = module.namespace.role_name
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = var.kubeconfig.cluster_ca_certificate
}
module "kubeconfig_ybon" {
  source  = "gitlab.com/vigigloo/tf-modules/generatescopedkubeconfig"
  version = "0.2.0"

  filename               = "donnees-ybon-${local.environment_slug}.yml"
  namespace              = module.namespace.namespace
  username               = "ybon"
  project_slug           = var.project_slug
  project_name           = var.project_name
  role_name              = module.namespace.role_name
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = var.kubeconfig.cluster_ca_certificate
}
module "kubeconfig_ronanamicel" {
  source  = "gitlab.com/vigigloo/tf-modules/generatescopedkubeconfig"
  version = "0.2.0"

  filename               = "donnees-ronanamicel-${local.environment_slug}.yml"
  namespace              = module.namespace.namespace
  username               = "ronanamicel"
  project_slug           = var.project_slug
  project_name           = var.project_name
  role_name              = module.namespace.role_name
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = var.kubeconfig.cluster_ca_certificate
}

module "kubeconfig_camille_legeron" {
  source  = "gitlab.com/vigigloo/tf-modules/generatescopedkubeconfig"
  version = "1.0.0"

  filename               = "donnees-camille-legeron-${local.environment_slug}.yml"
  namespace              = module.namespace.namespace
  username               = "camille-legeron"
  project_name           = var.project_name
  role_name              = module.namespace.role_name
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = var.kubeconfig.cluster_ca_certificate
}

output "namespace" {
  value = module.namespace
}
