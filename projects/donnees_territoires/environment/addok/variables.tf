variable "project_slug" {
  type = string
}

variable "additionnal_namespace_role_rules" {
  type = list(map(list(string)))
}
