module "namespace" {
  source  = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version = "2.1.0"

  namespace         = "${var.project_slug}-addok"
  max_cpu_requests  = 4
  max_memory_limits = "17Gi"
  project_name      = "Addok"
  project_slug      = "addok"

  additionnal_role_rules = var.additionnal_namespace_role_rules

  default_container_cpu_requests  = "100m"
  default_container_memory_limits = "128Mi"
}

output "namespace" {
  value = module.namespace
}
