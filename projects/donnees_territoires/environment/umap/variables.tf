variable "kubeconfig" {
  type = object({
    host                   = string
    token                  = string
    cluster_ca_certificate = string
  })
}

variable "base_domain" {
  type = string
}

variable "namespace" {
  type = string
}
variable "namespace_quota_max_cpu" {
  type    = number
  default = 2
}
variable "namespace_quota_max_memory" {
  type    = string
  default = "12Gi"
}

variable "project_slug" {
  type    = string
  default = "umap"
}
variable "project_name" {
  type    = string
  default = "umap"
}

variable "additionnal_namespace_role_rules" {
  type = list(map(list(string)))
}
