module "namespace" {
  source            = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version           = "2.1.0"
  namespace         = var.namespace
  max_cpu_requests  = var.namespace_quota_max_cpu
  max_memory_limits = var.namespace_quota_max_memory
  project_name      = var.project_name
  project_slug      = var.project_slug

  default_container_cpu_requests  = "20m"
  default_container_memory_limits = "16Mi"

  additionnal_role_rules = var.additionnal_namespace_role_rules
}

module "kubeconfig_yohan_boniface" {
  source  = "gitlab.com/vigigloo/tf-modules/generatescopedkubeconfig"
  version = "1.0.0"

  filename               = "donnees-yohan-boniface-${var.base_domain}.yml"
  namespace              = module.namespace.namespace
  username               = "yohan-boniface"
  project_name           = var.project_name
  role_name              = module.namespace.role_name
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = var.kubeconfig.cluster_ca_certificate
}

output "namespace" {
  value = module.namespace
}
