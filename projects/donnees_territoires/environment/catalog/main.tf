module "namespace" {
  source                          = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version                         = "2.1.0"
  namespace                       = var.namespace
  max_cpu_requests                = var.namespace_quota_max_cpu_requests
  max_memory_limits               = var.namespace_quota_max_memory_limits
  default_container_cpu_requests  = "100m"
  default_container_memory_limits = "128Mi"
  project_name                    = var.project_name
  project_slug                    = var.project_slug

  additionnal_role_rules = var.additionnal_namespace_role_rules
}

output "namespace" {
  value = module.namespace
}
