variable "project_slug" {
  type    = string
  default = "catalogue-indicateurs"
}

variable "project_name" {
  type    = string
  default = "Catalogue Indicateurs"
}

variable "namespace" {
  type = string
}

variable "namespace_quota_max_cpu_requests" {
  type    = string
  default = "2"
}

variable "namespace_quota_max_memory_limits" {
  type    = string
  default = "12Gi"
}

variable "additionnal_namespace_role_rules" {
  type = list(map(list(string)))
}
