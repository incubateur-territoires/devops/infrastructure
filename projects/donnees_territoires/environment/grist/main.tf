module "namespace" {
  source            = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version           = "2.1.0"
  max_cpu_requests  = local.namespace_requests_cpu
  max_memory_limits = local.namespace_limits_memory
  namespace         = var.override_namespace != null ? var.override_namespace : var.project_slug
  project_name      = "Grist"
  project_slug      = var.override_namespace != null ? var.override_namespace : var.project_slug

  default_container_cpu_requests  = "200m"
  default_container_memory_limits = "128Mi"

  additionnal_role_rules = var.additionnal_namespace_role_rules
}

module "kubeconfig" {
  for_each = var.generate_kubeconfigs
  source   = "gitlab.com/vigigloo/tf-modules/generatescopedkubeconfig"
  version  = "0.3.1"

  filename               = "${var.project_slug}-${each.key}.yml"
  namespace              = module.namespace.namespace
  username               = each.key
  project_name           = var.project_slug
  role_name              = module.namespace.role_name
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = var.kubeconfig.cluster_ca_certificate
}

output "namespace" {
  value = module.namespace
}
