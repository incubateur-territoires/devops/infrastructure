variable "project_slug" {
  type = string
}
variable "override_namespace" {
  type    = string
  default = null
}

variable "kubeconfig" {
  type = object({
    host                   = string
    token                  = string
    cluster_ca_certificate = string
  })
  default = null
}

variable "generate_kubeconfigs" {
  type    = set(string)
  default = []
}

variable "grist_doc_wk_replicas" {
  type = number
}
variable "grist_doc_wk_requests_cpu_m" {
  type = number
}
variable "grist_doc_wk_limits_memory_mb" {
  type = number
}
variable "grist_home_wk_replicas" {
  type = number
}
variable "grist_home_wk_requests_cpu_m" {
  type = number
}
variable "grist_home_wk_limits_memory_mb" {
  type = number
}
locals {
  grist_doc_wk_total_limits_memory_mb  = var.grist_doc_wk_replicas * var.grist_doc_wk_limits_memory_mb
  grist_home_wk_total_limits_memory_mb = var.grist_home_wk_replicas * var.grist_home_wk_limits_memory_mb
  grist_doc_wk_total_requests_cpu_m    = var.grist_doc_wk_replicas * var.grist_doc_wk_requests_cpu_m
  grist_home_wk_total_requests_cpu_m   = var.grist_home_wk_replicas * var.grist_home_wk_requests_cpu_m
  namespace_headroom_limits_memory_mb  = 12 * 1024
  namespace_headroom_requests_cpu_m    = 4 * 1000
  namespace_limits_memory              = "${local.grist_doc_wk_total_limits_memory_mb + local.grist_home_wk_total_limits_memory_mb + local.namespace_headroom_limits_memory_mb}Mi"
  namespace_requests_cpu               = "${local.grist_doc_wk_total_requests_cpu_m + local.grist_home_wk_total_requests_cpu_m + local.namespace_headroom_requests_cpu_m}m"
}

variable "additionnal_namespace_role_rules" {
  type = list(map(list(string)))
}
