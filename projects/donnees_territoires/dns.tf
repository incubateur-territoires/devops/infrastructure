resource "scaleway_domain_record" "sit" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "sit"
  type     = "A"
  data     = scaleway_instance_server.donnees_vm.public_ip
}

# Le site web statique de Données et Territoires (servi via GitLab Pages)
resource "scaleway_domain_record" "donnees" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "donnees"
  type     = "A"
  data     = scaleway_instance_server.donnees_vm.public_ip
}

# Le site web statique des fiches territoriales (servi via GitLab Pages)
resource "scaleway_domain_record" "fiches" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "fiches"
  type     = "A"
  data     = scaleway_instance_server.donnees_vm.public_ip
}

resource "scaleway_domain_record" "formulaires" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "formulaires"
  type     = "CNAME"
  data     = var.common.production_cluster_cname
}

resource "scaleway_domain_record" "subdomains" {
  provider = scaleway.dns
  for_each = toset(["gitlab", "fichiers", "servitu", "templates-fiches", "dashboards-builder"])
  dns_zone = var.common.dns_zone_incubateur
  name     = "${each.value}.donnees"
  type     = "A"
  data     = scaleway_instance_server.donnees_vm.public_ip
}

resource "scaleway_domain_record" "addok" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "addok.donnees"
  type     = "A"
  data     = scaleway_instance_server.donnees_vm.public_ip
}
resource "scaleway_domain_record" "servitu" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "servitu-preprod.donnees"
  type     = "A"
  data     = scaleway_instance_server.donnees_vm.public_ip
}

resource "scaleway_domain_zone" "gitlab_pages" {
  provider   = scaleway.dns
  domain     = var.common.dns_zone_incubateur
  subdomain  = "pages.gitlab.donnees"
  project_id = module.project.scaleway_project_config.project_id
}

resource "scaleway_domain_record" "gitlab_pages_verification" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "_gitlab-pages-verification-code.donnees"
  type     = "TXT"
  data     = "gitlab-pages-verification-code=6c11661347a05505ba4b770a95fc2724"
}

resource "scaleway_domain_record" "sit_wildcard" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "*.sit"
  type     = "A"
  data     = scaleway_instance_server.donnees_vm.public_ip
}

resource "scaleway_domain_record" "grist" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "grist"
  type     = "ALIAS"
  data     = "hedy-lamarr.beta.numerique.gouv.fr."
}
resource "scaleway_domain_record" "grist_dinum" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "grist-dinum.dom"
  type     = "ALIAS"
  data     = var.common.production_cluster_cname
}

resource "scaleway_domain_record" "fiches_sgar44" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "fiches-sgar44"
  type     = "A"
  data     = "51.159.153.132"
}

resource "scaleway_domain_record" "servitu_sgar44" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "servitu-sgar44.donnees"
  type     = "A"
  data     = "51.159.153.132"
}

resource "scaleway_domain_record" "catalog_dev" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "catalogue-indicateurs-dev.donnees"
  type     = "ALIAS"
  data     = "catalogue-donnees-anct-staging.osc-fr1.scalingo.io."
  ttl      = 60
}
resource "scaleway_domain_record" "catalog" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "catalogue-indicateurs.donnees"
  type     = "ALIAS"
  data     = "catalogue-donnees-anct-prod.osc-fr1.scalingo.io."
  ttl      = 60
}
