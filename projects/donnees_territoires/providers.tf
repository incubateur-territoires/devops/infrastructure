provider "scaleway" {
  alias      = "project"
  region     = "fr-par"
  zone       = "fr-par-1"
  access_key = module.project.scaleway_project_config.access_key
  secret_key = module.project.scaleway_project_config.secret_key
  project_id = module.project.scaleway_project_config.project_id
}

provider "kubernetes" {
  alias                  = "development"
  host                   = var.kubeconfig_development.host
  token                  = var.kubeconfig_development.token
  cluster_ca_certificate = base64decode(var.kubeconfig_development.cluster_ca_certificate)
}
provider "helm" {
  alias = "development"
  kubernetes {
    host                   = var.kubeconfig_development.host
    token                  = var.kubeconfig_development.token
    cluster_ca_certificate = base64decode(var.kubeconfig_development.cluster_ca_certificate)
  }
}

provider "kubernetes" {
  alias                  = "production"
  host                   = var.kubeconfig_production.host
  token                  = var.kubeconfig_production.token
  cluster_ca_certificate = base64decode(var.kubeconfig_production.cluster_ca_certificate)
}
provider "helm" {
  alias = "production"
  kubernetes {
    host                   = var.kubeconfig_production.host
    token                  = var.kubeconfig_production.token
    cluster_ca_certificate = base64decode(var.kubeconfig_production.cluster_ca_certificate)
  }
}
