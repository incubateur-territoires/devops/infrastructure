locals {
  project_name      = "Donnees Et Territoires"
  project_subdomain = "donnees"
}
module "project" {
  source               = "../generic"
  common               = var.common
  project_name         = local.project_name
  project_slug         = "donneesetterritoires"
  project_subdomain    = local.project_subdomain
  with_record          = false
  with_scw_project     = true
  with_cluster_secrets = false
  startup_group        = "donnees-et-territoires"
  scw_custom_groups = {
    "read backups" = ["ObjectStorageReadOnly", "ObjectStorageObjectsRead", "ObjectStorageBucketsRead"],
    "use cockpit"  = ["ObservabilityFullAccess"],
  }
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.default = scaleway.default
  }
}
