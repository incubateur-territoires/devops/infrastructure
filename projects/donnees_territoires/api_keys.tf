resource "scaleway_iam_application" "mail" {
  provider    = scaleway.iam
  name        = "${local.project_name} tem mail"
  description = "Application used for API keys allowing sending mails using TEM (${var.common.managed})"
}

resource "scaleway_iam_api_key" "mail" {
  provider       = scaleway.iam
  application_id = scaleway_iam_application.mail.id
  description    = "API key to send emails via transactional API for ${local.project_name} (${var.common.managed})"
}

resource "scaleway_iam_policy" "mail" {
  provider       = scaleway.iam
  name           = "Email sending policy for ${local.project_name}"
  description    = "Allow to send email (${var.common.managed})"
  application_id = scaleway_iam_application.mail.id
  rule {
    project_ids          = [module.project.scaleway_project_config.project_id]
    permission_set_names = ["TransactionalEmailEmailFullAccess"]
  }
}

module "api_key_metabase_instances_deployment" {
  source      = "../../modules/scw_api_key"
  name        = "Donnees metabase instances deployment"
  description = "Key used with terraform to provision ressources for metabase instances"
  permissions = ["ContainersFullAccess", "RelationalDatabasesFullAccess", "DomainsDNSFullAccess", "TransactionalEmailDomainFullAccess"]
  project_id  = module.project.scaleway_project_config.project_id
  providers = {
    scaleway.iam = scaleway.iam
  }
}
module "api_key_metabase_instances_tem_user" {
  source      = "../../modules/scw_api_key"
  name        = "Donnees metabase instances tem user"
  description = "Key used by metabase instances to send emails using scaleway's tem"
  permissions = ["TransactionalEmailEmailFullAccess"]
  project_id  = module.project.scaleway_project_config.project_id
  providers = {
    scaleway.iam = scaleway.iam
  }
}
module "api_key_gitlab_pages_certbot" {
  source      = "../../modules/scw_api_key"
  name        = "Donnees gitlab pages certbot"
  description = "Key used for certbot challenges to create certificates for gitlab pages"
  permissions = ["DomainsDNSFullAccess"]
  project_id  = module.project.scaleway_project_config.project_id
  providers = {
    scaleway.iam = scaleway.iam
  }
}

module "api_key_vm_backups" {
  source      = "../../modules/scw_api_key"
  name        = "Donnees backups VM"
  description = "Used for backing up the VM and its PG instance to S3"
  permissions = ["ObjectStorageObjectsRead", "ObjectStorageObjectsWrite", "ObjectStorageBucketsRead"]
  project_id  = module.project.scaleway_project_config.project_id
  providers = {
    scaleway.iam = scaleway.iam
  }
}
