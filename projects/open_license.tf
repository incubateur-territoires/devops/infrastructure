module "licence_libre" {
  source       = "./generic"
  common       = local.common
  project_name = "Licence libre"
  with_record  = false

  gl_project_to_configure = "incubateur-territoires/incubateur/legal-design-license-ouverte"
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.default = scaleway.default
  }
}

resource "scaleway_domain_record" "licence_libre_alias" {
  provider = scaleway.dns
  dns_zone = var.dns_zone_incubateur
  name     = "licence-libre"
  type     = "ALIAS"
  data     = "incubateur-territoires.gitlab.io."
}

resource "scaleway_domain_record" "licence_libre_verification_code" {
  provider = scaleway.dns
  dns_zone = var.dns_zone_incubateur
  name     = "_gitlab-pages-verification-code.licence-libre"
  type     = "TXT"
  data     = "gitlab-pages-verification-code=3d8e4f64dcc827473f07296a4a85141c"
}
