resource "scaleway_domain_record" "dotations_locales" {
  provider = scaleway.dns
  dns_zone = var.dns_zone_incubateur
  name     = "dotations"
  type     = "CNAME"
  data     = "dotations-locales-app.osc-fr1.scalingo.io."
}
