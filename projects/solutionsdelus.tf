module "solutionsdelus" {
  source               = "./generic"
  common               = local.common
  project_name         = "Solutions D Elus"
  with_tf_project      = false
  with_scw_project     = true
  with_record          = false
  with_wildcard_record = false
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.default = scaleway.default
  }
}

resource "scaleway_domain_record" "solutionsdelus_validation" {
  provider = scaleway.dns
  dns_zone = var.dns_zone_incubateur
  name     = "_scaleway-challenge.solutionsdelus"
  type     = "TXT"
  data     = "ba2a4f0c-9c02-46aa-9b09-286e75b7489b"
}
