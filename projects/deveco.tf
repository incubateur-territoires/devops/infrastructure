module "deveco" {
  source               = "./generic"
  common               = local.common
  project_name         = "Deveco"
  with_record          = false
  with_wildcard_record = false
  with_scw_project     = true
  startup_group        = "deveco"
  scw_custom_groups = {
    "read backups" = ["ObjectStorageReadOnly"],
    "all readonly" = ["AllProductsReadOnly"],
    "use cockpit"  = ["ObservabilityFullAccess"],
  }
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.default = scaleway.default
  }
}

resource "scaleway_domain_record" "deveco" {
  provider = scaleway.dns
  dns_zone = var.dns_zone_incubateur
  name     = "deveco"
  type     = "ALIAS"
  data     = "deveco-prod-front.osc-secnum-fr1.scalingo.io."
}

resource "scaleway_domain_record" "deveco_dev" {
  provider = scaleway.dns
  dns_zone = var.dns_zone_incubateur
  name     = "deveco.dev"
  type     = "ALIAS"
  data     = "deveco-preprod-front.osc-fr1.scalingo.io."
}

resource "scaleway_domain_record" "deveco_zammad" {
  provider = scaleway.dns
  dns_zone = var.dns_zone_incubateur
  name     = "zammad.deveco"
  type     = "ALIAS"
  data     = "deveco-zammad.anct.cloud-ed.fr."
}

resource "scaleway_domain_record" "deveco_google_search_console" {
  provider = scaleway.dns
  dns_zone = var.dns_zone_incubateur
  name     = "deveco"
  type     = "TXT"
  data     = "google-site-verification=L2JQ-fN4ulue8tUkvSsJqXUR6JoL0KvlhieQMp0olCA"
}

resource "scaleway_domain_record" "deveco_sib_code" {
  provider = scaleway.dns
  dns_zone = var.dns_zone_incubateur
  name     = "deveco"
  type     = "TXT"
  data     = "Sendinblue-code:8ff5d53433fed3070b3f1359d24abb8b"
}

resource "scaleway_domain_record" "deveco_ovh_autodiscover" {
  provider = scaleway.dns
  dns_zone = var.dns_zone_incubateur
  name     = "_autodiscover._tcp.deveco"
  type     = "SRV"
  data     = "0 0 443 pro1.mail.ovh.net."
}

resource "scaleway_domain_record" "deveco_ovh_mx0" {
  provider = scaleway.dns
  dns_zone = var.dns_zone_incubateur
  name     = "deveco"
  type     = "MX"
  data     = "mx0.mail.ovh.net."
  priority = 1
}
resource "scaleway_domain_record" "deveco_ovh_mx1" {
  provider = scaleway.dns
  dns_zone = var.dns_zone_incubateur
  name     = "deveco"
  type     = "MX"
  data     = "mx1.mail.ovh.net."
  priority = 5
}
resource "scaleway_domain_record" "deveco_ovh_mx2" {
  provider = scaleway.dns
  dns_zone = var.dns_zone_incubateur
  name     = "deveco"
  type     = "MX"
  data     = "mx2.mail.ovh.net."
  priority = 50
}
resource "scaleway_domain_record" "deveco_ovh_mx3" {
  provider = scaleway.dns
  dns_zone = var.dns_zone_incubateur
  name     = "deveco"
  type     = "MX"
  data     = "mx3.mail.ovh.net."
  priority = 100
}
resource "scaleway_domain_record" "deveco_ovh_dkim_1" {
  provider = scaleway.dns
  dns_zone = var.dns_zone_incubateur
  name     = "ovhemp1074972-selector1._domainkey.deveco"
  type     = "CNAME"
  data     = "ovhemp1074972-selector1._domainkey.50491.ae.dkim.mail.ovh.net."
}
resource "scaleway_domain_record" "deveco_ovh_dkim_2" {
  provider = scaleway.dns
  dns_zone = var.dns_zone_incubateur
  name     = "ovhemp1074972-selector2._domainkey.deveco"
  type     = "CNAME"
  data     = "ovhemp1074972-selector2._domainkey.50492.af.dkim.mail.ovh.net."
}

resource "scaleway_domain_record" "deveco_spf" {
  provider = scaleway.dns
  dns_zone = var.dns_zone_incubateur
  name     = "deveco"
  type     = "TXT"
  data     = "v=spf1 include:mx.ovh.com include:spf.sendinblue.com include:spf.tipimail.com mx ~all"
}
resource "scaleway_domain_record" "deveco_dmarc" {
  provider = scaleway.dns
  dns_zone = var.dns_zone_incubateur
  name     = "_dmarc.deveco"
  type     = "TXT"
  data     = "v=DMARC1; p=none; sp=none; rua=mailto:dmarc@mailinblue.com!10m; ruf=mailto:dmarc@mailinblue.com!10m; rf=afrf; pct=100; ri=86400"
}

resource "scaleway_domain_record" "deveco_brevo_dkim_1" {
  provider = scaleway.dns
  dns_zone = var.dns_zone_incubateur
  name     = "brevo1._domainkey.deveco"
  type     = "CNAME"
  data     = "b1.deveco-incubateur-anct-gouv-fr.dkim.brevo.com."
}
resource "scaleway_domain_record" "deveco_brevo_dkim_2" {
  provider = scaleway.dns
  dns_zone = var.dns_zone_incubateur
  name     = "brevo2._domainkey.deveco"
  type     = "CNAME"
  data     = "b2.deveco-incubateur-anct-gouv-fr.dkim.brevo.com."
}

resource "scaleway_domain_record" "deveco_metabase" {
  provider = scaleway.dns
  dns_zone = var.dns_zone_incubateur
  name     = "metabase.deveco"
  type     = "ALIAS"
  data     = "deveco-prod-metabase.osc-secnum-fr1.scalingo.io."
}
