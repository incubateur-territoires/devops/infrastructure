resource "scaleway_object_bucket" "prod_data" {
  provider = scaleway.project
  name     = "${local.project_slug}-data-prod"
}
resource "scaleway_object_bucket_acl" "prod_data" {
  provider = scaleway.project
  bucket   = resource.scaleway_object_bucket.prod_data.id
  acl      = "public-read"
}

resource "scaleway_object_bucket" "prod_cache" {
  provider = scaleway.project
  name     = "${local.project_slug}-cache-prod"
}
resource "scaleway_object_bucket_acl" "prod_cache" {
  provider = scaleway.project
  bucket   = resource.scaleway_object_bucket.prod_cache.id
  acl      = "public-read"
}
