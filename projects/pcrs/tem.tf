module "tem" {
  source       = "../../modules/tem/"
  dns_zone     = local.dns_zone
  create_mx    = false
  create_dmarc = false
  create_spf   = false
  providers = {
    scaleway.iam        = scaleway.iam
    scaleway.dns        = scaleway.dns
    scaleway.tem_domain = scaleway.project
  }
}
