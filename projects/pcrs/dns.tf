locals {
  dns_zone = "pcrs.beta.gouv.fr"
}

resource "scaleway_domain_record" "pcrs" {
  provider = scaleway.project
  dns_zone = local.dns_zone
  type     = "ALIAS"
  data     = "pcrsbetagouv.osc-fr1.scalingo.io."
}
resource "scaleway_domain_record" "docs" {
  provider = scaleway.project
  dns_zone = local.dns_zone
  name     = "docs"
  type     = "ALIAS"
  data     = "78b5dab77a-hosting.gitbook.io."
}
resource "scaleway_domain_record" "scanner" {
  provider = scaleway.project
  dns_zone = local.dns_zone
  name     = "scanner"
  type     = "ALIAS"
  data     = "pcrs-scanner.osc-fr1.scalingo.io."
}
