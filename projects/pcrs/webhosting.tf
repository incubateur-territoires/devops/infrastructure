resource "scaleway_webhosting" "mail_contact" {
  provider = scaleway.project
  domain   = "pcrs.beta.gouv.fr"
  offer_id = var.common.webhosting_offer_lite
  email    = var.common.webhosting_contact_email
}

# An MX record can't point directly to the IP of the mail server, so we use an intermediary record
resource "scaleway_domain_record" "webhosting_mx" {
  provider = scaleway.dns
  dns_zone = local.dns_zone
  name     = ""
  type     = "MX"
  data     = "mx.${local.dns_zone}."
}
resource "scaleway_domain_record" "webhosting_mx_a" {
  provider = scaleway.dns
  dns_zone = local.dns_zone
  name     = "mx"
  type     = "A"
  data     = "51.159.173.17"
}
resource "scaleway_domain_record" "webhosting_dmarc" {
  provider = scaleway.dns
  dns_zone = local.dns_zone
  name     = "_dmarc"
  type     = "TXT"
  data     = "51.159.173.17"
}
resource "scaleway_domain_record" "webhosting_dkim" {
  provider = scaleway.dns
  dns_zone = local.dns_zone
  name     = "default._domainkey"
  type     = "TXT"
  data     = "v=DKIM1; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsv076qJJXo0NkhmOyFfqkMc7iQab5krXDSpq8w5VfeT2pmscUvD7yHs1dANFh+GwgQrLi+dfYLrQ2lLTtp/T4kKjZVDNCWvyqEJzVlTy6cR5fehptURM3Dlz3LIqcK8nyTjTtvTj7qiECFM+XF/kb1TYFQmNwunqVubT7NTBiU4IUHQzBJHsRW0hcGEufDSmgFkJBqCgk/O5VEvdiWBBGbiFPQCPZeLgidyFvy6KAViFsc48ZNf3mHouMeFzwIWD+pyk2YXPFLAM5iTV0EUqKf4vlXOgn3GS5CvbSlM84gYixkA11mty1IBCos51Ns2y7jqxPUxB5akeWUYjCWLbaQIDAQAB;"
}
resource "scaleway_domain_record" "webhosting_spf" {
  provider = scaleway.dns
  dns_zone = local.dns_zone
  type     = "TXT"
  data     = "v=spf1 ip4:51.159.173.17 +a +mx ~all"
}
