variable "common" {
  type = map(string)
}

variable "rdb_acl" {}

variable "kubeconfig_production" {
  type = object({
    host                   = string
    token                  = string
    cluster_ca_certificate = string
  })
}
