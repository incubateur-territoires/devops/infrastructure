resource "scaleway_domain_record" "chat" {
  provider = scaleway.dns
  dns_zone = local.dns_zone_incubateur
  name     = "chat"
  type     = "A"
  data     = var.common.lb_ip_prod
  ttl      = 60
}

locals {
  dns_zone_incubateur = "incubateur.anct.gouv.fr"
}
