resource "random_password" "mattermost_postgresql" {
  length = 1
  lifecycle {
    ignore_changes = all
  }
}

module "mattermost_namespace" {
  source            = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version           = "1.0.1"
  max_cpu_requests  = "6"
  max_memory_limits = "8Gi"
  namespace         = "mattermost"
  project_name      = "Mattermost"
  project_slug      = "mattermost"

  default_container_cpu_requests  = "200m"
  default_container_memory_limits = "128Mi"
  providers = {
    kubernetes = kubernetes.production
  }
}

# Releases:
# https://docs.mattermost.com/about/mattermost-server-releases.html
resource "helm_release" "mattermost" {
  provider = helm.production

  name       = "mattermost"
  namespace  = module.mattermost_namespace.namespace
  repository = "https://gitlab.com/api/v4/projects/30792282/packages/helm/stable"
  chart      = "mattermost-team-edition"
  version    = "6.3.1"
  values = [
    <<-EOT
    image:
      repository: mattermost/mattermost-team-edition
      tag: "10.5.1"
    externalDB:
      enabled: true
      externalDriverType: postgres
      externalConnectionString: "${random_password.mattermost_postgresql.result}"
    ingress:
      host:
    configJSON:
      ServiceSettings:
        SiteURL: https://chat.incubateur.anct.gouv.fr
    resources:
      requests:
        cpu: 500m
      limits:
        memory: 2048Mi
    affinity:
      podAffinity:
        requiredDuringSchedulingIgnoredDuringExecution:
        - labelSelector:
            matchExpressions:
            - key: app.kubernetes.io/name
              operator: In
              values:
              - mattermost-team-edition
          topologyKey: kubernetes.io/hostname
    EOT
    ,
    file("${path.module}/mattermost.yaml")
  ]
}
