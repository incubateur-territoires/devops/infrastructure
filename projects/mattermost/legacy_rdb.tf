data "scaleway_rdb_instance" "incubateur_outillage" {
  provider = scaleway.default
  name     = "incubateur-outillage"
}
resource "scaleway_rdb_acl" "incubateur_outillage" {
  provider    = scaleway.default
  instance_id = data.scaleway_rdb_instance.incubateur_outillage.id
  dynamic "acl_rules" {
    for_each = var.rdb_acl
    content {
      ip          = "${acl_rules.value.public_ip}/32"
      description = "${acl_rules.value.name} managed with terraform"
    }
  }
}
