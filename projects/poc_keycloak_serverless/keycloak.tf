resource "random_password" "admin" {
  length = 128
}
resource "scaleway_container_namespace" "ns" {
  name        = local.project_slug
  description = local.managed
}
locals {
  dbhost  = scaleway_rdb_instance.rdb.load_balancer[0].ip
  dbport  = scaleway_rdb_instance.rdb.load_balancer[0].port
  dbname  = scaleway_rdb_database.rdb.name
  dbextra = "?sslmode=require"
  dburi   = "jdbc:postgresql://${local.dbhost}:${local.dbport}/${local.dbname}${local.dbextra}"
}
resource "scaleway_container" "container" {
  namespace_id   = scaleway_container_namespace.ns.id
  name           = local.project_slug
  description    = local.managed
  registry_image = "${scaleway_container_namespace.ns.registry_endpoint}/keycloak:latest"
  port           = 8080
  cpu_limit      = 1120
  memory_limit   = 2048
  min_scale      = 1
  max_scale      = 1
  environment_variables = {
    KC_DB_URL                         = local.dburi
    KC_DB_USERNAME                    = scaleway_rdb_user.rdb.name
    KC_HOSTNAME                       = local.hostname
    KC_HTTP_ENABLED                   = true
    KC_PROXY_HEADERS                  = "xforwarded"
    KEYCLOAK_BOOTSTRAP_ADMIN_USERNAME = "admin"
  }
  secret_environment_variables = {
    KC_DB_PASSWORD                    = scaleway_rdb_user.rdb.password
    KEYCLOAK_BOOTSTRAP_ADMIN_PASSWORD = random_password.admin.result
  }
  http_option = "redirected"
  deploy      = true
}
resource "scaleway_container_domain" "domain" {
  container_id = scaleway_container.container.id
  hostname     = local.hostname
}

