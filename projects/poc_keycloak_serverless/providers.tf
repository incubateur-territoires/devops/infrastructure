# alias default provider to use the project to create resources as we will be creating quite a lot
provider "scaleway" {
  region     = "fr-par"
  zone       = "fr-par-1"
  access_key = module.project.scaleway_project_config.access_key
  secret_key = module.project.scaleway_project_config.secret_key
  project_id = module.project.scaleway_project_config.project_id
}
provider "scaleway" {
  alias      = "project"
  region     = "fr-par"
  zone       = "fr-par-1"
  access_key = module.project.scaleway_project_config.access_key
  secret_key = module.project.scaleway_project_config.secret_key
  project_id = module.project.scaleway_project_config.project_id
}
