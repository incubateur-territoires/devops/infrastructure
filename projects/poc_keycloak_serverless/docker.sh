#/bin/env bash

REGISTRY_ENDPOINT=$(scw container namespace list -o json | jq '.[] | select(.name == "poc-keycloak-serverless") | .registry_endpoint' -r)

docker build -t "$REGISTRY_ENDPOINT/keycloak:latest" -f keycloak.docker .
docker push "$REGISTRY_ENDPOINT/keycloak:latest"
