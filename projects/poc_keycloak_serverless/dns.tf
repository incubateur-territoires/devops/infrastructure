locals {
  subdomain = "sso-poc-1.dev"
  dns_zone  = scaleway_domain_zone.zone.id
  hostname  = local.dns_zone
}
resource "scaleway_domain_zone" "zone" {
  provider   = scaleway.dns
  domain     = var.common.dns_zone_incubateur
  subdomain  = local.subdomain
  project_id = module.project.scaleway_project_config.project_id
}

resource "scaleway_domain_record" "container" {
  dns_zone = local.dns_zone
  name     = ""
  type     = "ALIAS"
  data     = "${scaleway_container.container.domain_name}."
}
