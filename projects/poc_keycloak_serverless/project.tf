locals {
  project_name = "PoC Keycloak Serverless"
  # tflint-ignore: terraform_unused_declarations
  project_slug = "poc-keycloak-serverless"
  managed      = "${local.project_name} (Managed by Terraform)"
}
module "project" {
  source               = "../generic"
  common               = var.common
  project_name         = local.project_name
  with_tf_project      = false
  with_record          = false
  with_wildcard_record = false
  with_scw_project     = true
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.default = scaleway.default
  }
}
