resource "scaleway_rdb_instance" "rdb" {
  name          = local.project_slug
  engine        = "PostgreSQL-15"
  node_type     = "db-dev-s"
  is_ha_cluster = false
}
resource "random_password" "rdb" {
  override_special = "-_+=/"
  length           = 128
}
resource "scaleway_rdb_user" "rdb" {
  instance_id = scaleway_rdb_instance.rdb.id
  name        = local.project_slug
  password    = random_password.rdb.result
}
resource "scaleway_rdb_database" "rdb" {
  instance_id = scaleway_rdb_instance.rdb.id
  name        = local.project_slug
}
resource "scaleway_rdb_privilege" "rdb" {
  instance_id   = scaleway_rdb_instance.rdb.id
  user_name     = scaleway_rdb_user.rdb.name
  database_name = scaleway_rdb_database.rdb.name
  permission    = "all"
}
