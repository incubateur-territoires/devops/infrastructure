resource "scaleway_domain_record" "mon_stage_de_troisieme_zammad" {
  provider = scaleway.dns
  dns_zone = var.dns_zone_incubateur
  name     = "zammad.mon-stage-de-troisieme"
  type     = "CNAME"
  data     = "stage-3eme-zammad.anct.cloud-ed.fr."
}
