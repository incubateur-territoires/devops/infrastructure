terraform {
  required_providers {
    scaleway = {
      source = "scaleway/scaleway"
      configuration_aliases = [
        scaleway.iam,
        scaleway.dns,
        scaleway.default,
      ]
    }
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }
  required_version = ">= 0.14"
}
