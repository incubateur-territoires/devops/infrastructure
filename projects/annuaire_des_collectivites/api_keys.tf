resource "scaleway_iam_application" "collectivite_fr_mail" {
  provider    = scaleway.iam
  name        = "${local.collectivite_fr_project_name} tem mail"
  description = "API key to send emails via transactional API for ${local.collectivite_fr_project_name} (${var.common.managed})"
}
resource "scaleway_iam_api_key" "collectivite_fr_mail_prod" {
  provider       = scaleway.iam
  application_id = scaleway_iam_application.collectivite_fr_mail.id
  description    = "API key to send emails via transactional API for ${local.collectivite_fr_project_name} prod (${var.common.managed})"
}
resource "scaleway_iam_api_key" "collectivite_fr_mail_dev" {
  provider       = scaleway.iam
  application_id = scaleway_iam_application.collectivite_fr_mail.id
  description    = "API key to send emails via transactional API for ${local.collectivite_fr_project_name} dev (${var.common.managed})"
}
resource "scaleway_iam_policy" "collectivite_fr_mail" {
  provider       = scaleway.iam
  name           = "Email sending policy for ${local.collectivite_fr_project_name}"
  description    = "Allow to send email (${var.common.managed})"
  application_id = scaleway_iam_application.collectivite_fr_mail.id
  rule {
    project_ids          = [var.scaleway_default_project_id]
    permission_set_names = ["TransactionalEmailEmailFullAccess"]
  }
}
