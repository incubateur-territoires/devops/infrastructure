resource "scaleway_tem_domain" "collectivite_fr" {
  provider   = scaleway.default
  accept_tos = true
  name       = local.collectivite_fr_dns_zone
}
resource "scaleway_domain_record" "collectivite_fr_dkim_mail" {
  provider = scaleway.dns
  dns_zone = local.collectivite_fr_dns_zone
  name     = "${var.scaleway_default_project_id}._domainkey"
  type     = "TXT"
  data     = scaleway_tem_domain.collectivite_fr.dkim_config
}
resource "scaleway_domain_record" "collectivite_fr_dmarc_mail" {
  provider = scaleway.dns
  dns_zone = local.collectivite_fr_dns_zone
  name     = scaleway_tem_domain.collectivite_fr.dmarc_name
  type     = "TXT"
  data     = scaleway_tem_domain.collectivite_fr.dmarc_config
}

resource "gitlab_project_variable" "collectivite_fr_prod_tem_secret_key" {
  project = module.project.gitlab_project_id
  key     = "TF_VAR_production_collectivite_fr_tem_secret_key"
  value   = scaleway_iam_api_key.collectivite_fr_mail_prod.secret_key
}
resource "gitlab_project_variable" "collectivite_fr_prod_tem_host" {
  project = module.project.gitlab_project_id
  key     = "TF_VAR_production_collectivite_fr_tem_host"
  value   = "smtp.tem.scw.cloud"
}
resource "gitlab_project_variable" "collectivite_fr_prod_tem_user" {
  project = module.project.gitlab_project_id
  key     = "TF_VAR_production_collectivite_fr_tem_user"
  value   = var.scaleway_default_project_id
}
resource "gitlab_project_variable" "collectivite_fr_prod_tem_port" {
  project = module.project.gitlab_project_id
  key     = "TF_VAR_production_collectivite_fr_tem_port"
  value   = "465"
}
resource "gitlab_project_variable" "collectivite_fr_dev_tem_secret_key" {
  project = module.project.gitlab_project_id
  key     = "TF_VAR_development_collectivite_fr_tem_secret_key"
  value   = scaleway_iam_api_key.collectivite_fr_mail_dev.secret_key
}
resource "gitlab_project_variable" "collectivite_fr_dev_tem_host" {
  project = module.project.gitlab_project_id
  key     = "TF_VAR_development_collectivite_fr_tem_host"
  value   = "smtp.tem.scw.cloud"
}
resource "gitlab_project_variable" "collectivite_fr_dev_tem_user" {
  project = module.project.gitlab_project_id
  key     = "TF_VAR_development_collectivite_fr_tem_user"
  value   = var.scaleway_default_project_id
}
resource "gitlab_project_variable" "collectivite_fr_dev_tem_port" {
  project = module.project.gitlab_project_id
  key     = "TF_VAR_development_collectivite_fr_tem_port"
  value   = "465"
}
