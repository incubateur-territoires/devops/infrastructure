resource "scaleway_webhosting" "mail_contact" {
  provider = scaleway.default
  domain   = "collectivite.fr"
  offer_id = var.common.webhosting_offer_personal
  email    = var.common.webhosting_contact_email
}
# An MX record can't point directly to the IP of the mail server, so we use an intermediary record
resource "scaleway_domain_record" "collectivite_fr_mx_mx" {
  provider = scaleway.dns
  dns_zone = local.collectivite_fr_dns_zone
  name     = ""
  type     = "MX"
  data     = "mx.collectivite.fr."
}
resource "scaleway_domain_record" "collectivite_fr_mx" {
  provider = scaleway.dns
  dns_zone = local.collectivite_fr_dns_zone
  name     = "mx"
  type     = "A"
  data     = "51.159.128.187"
}
resource "scaleway_domain_record" "collectivite_fr_dkim" {
  provider = scaleway.dns
  dns_zone = local.collectivite_fr_dns_zone
  name     = "default._domainkey"
  type     = "TXT"
  data     = "v=DKIM1; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApxgJjReltM0RgPv9IvCFhK7B1SGbpQRTh4kRcubOkfIyxR0w9IDyA5AL1LOcEECt55Sov8DQK2dsybQflvmiQlw/7/IZRES51yzBPEDYqcopmBulGXyY70G8X84iC9MR5MjY1rrSRnxnQ3BlnE9KD5QblnNFKKgSsTtaHgGesYmjohk6xBtZ5wICmjpiUad+V32YA1M7cNm3GfcJIQIts0+OrqUmHJvym4iwoSOwKJiwjNSh0A/U4UNNuc8tvDN2vSHf9Vd6NSazWepK4QjOn3ssXKSGkWGCcM4Mr5Svu2KrAPknfMhyQYZvUKJGzHCVymZVfOPnJr4fg56kcpknaQIDAQAB;"
}

resource "scaleway_webhosting" "mail_prospection" {
  provider = scaleway.project
  domain   = local.dns_zone_annuaire
  offer_id = var.common.webhosting_offer_lite
  email    = var.common.webhosting_contact_email
}
resource "scaleway_domain_record" "prospection_mx_mx" {
  provider = scaleway.dns
  dns_zone = local.dns_zone_annuaire
  type     = "MX"
  data     = "mx.${local.dns_zone_annuaire}."
}
resource "scaleway_domain_record" "prospection_mx" {
  provider = scaleway.dns
  dns_zone = local.dns_zone_annuaire
  type     = "A"
  name     = "mx"
  data     = "51.159.128.187"
}
resource "scaleway_domain_record" "prospection_dkim" {
  provider = scaleway.dns
  dns_zone = local.dns_zone_annuaire
  type     = "TXT"
  name     = "default._domainkey"
  data     = "v=DKIM1; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAupl+kuyv1jncgUpvEZQtb0eSf/er2snPXqjuYtX7qIDtz8s5BEhL1JvpXwe9DuipV10WjZMbAlkj+QC6i6hyiPi5cVbbO1725cUpaQb3eWyP9794+s3dzUg9dkCfSALyfUw/YShqNhwhXg3jBZ7xD2Dbxf/IfvMv+06dHG3aEwGeBZZpuzGvEILbH1WVAsdz7bkb7aC5uAu8GFjnpxBfXrQ0sXjljRKad3eUWeZlbgkrtH1e2N1gZ+dsCfB3u+TgmqHeKjVnK1VHGGtwdnT8T/PQJ20GonKtCCMvcKTWISjgFlEL09toxqx+Hl/MqM0k+isUAFS4vA5aIK7fffQexQIDAQAB;"
}
resource "scaleway_domain_record" "prospection_spf" {
  provider = scaleway.dns
  dns_zone = local.dns_zone_annuaire
  type     = "TXT"
  data     = "v=spf1 ip4:51.159.128.187 +a +mx ~all"
}
