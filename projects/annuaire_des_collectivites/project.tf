locals {
  # tflint-ignore: terraform_unused_declarations
  project_name = "Annuaire des Collectivites"
  # tflint-ignore: terraform_unused_declarations
  project_slug                 = "annuaire-des-collectivites"
  collectivite_fr_project_name = "Annuaire des Collectivites"
}
module "project" {
  source       = "../generic"
  common       = var.common
  project_name = local.collectivite_fr_project_name
  project_slug = "annuaire-des-collectivites"

  startup_group        = "annuaire-des-collectivites"
  with_record          = false
  with_wildcard_record = false
  with_scw_project     = true
  scw_custom_groups = {
    "read backups"  = ["ObjectStorageReadOnly"],
    "configure dns" = ["DomainsDNSFullAccess"],
  }
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.default = scaleway.default
  }
}
