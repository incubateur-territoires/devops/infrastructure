locals {
  collectivite_fr_dns_zone = "collectivite.fr"
}

resource "scaleway_domain_record" "annuaire_des_collectivites_gsite_verification" {
  provider = scaleway.dns
  dns_zone = local.collectivite_fr_dns_zone
  name     = ""
  type     = "TXT"
  data     = "google-site-verification=5czdtdebjHmPb8k6_ahd9eAFkHr97DD4ycgFLTi02WQ"
}

resource "scaleway_domain_record" "annuaire_des_collectivites_record" {
  provider = scaleway.dns
  dns_zone = local.collectivite_fr_dns_zone
  name     = ""
  type     = "ALIAS"
  data     = "annuaire-collectivite-prod-front.osc-secnum-fr1.scalingo.io."
}

resource "scaleway_domain_record" "annuaire_des_collectivites_record_api" {
  provider = scaleway.dns
  dns_zone = local.collectivite_fr_dns_zone
  name     = "api"
  type     = "ALIAS"
  data     = "annuaire-collectivite-prod-api.osc-secnum-fr1.scalingo.io."
}
resource "scaleway_domain_record" "annuaire_des_collectivites_record_www" {
  provider = scaleway.dns
  dns_zone = local.collectivite_fr_dns_zone
  name     = "www"
  type     = "ALIAS"
  data     = "annuaire-collectivite-prod-front.osc-secnum-fr1.scalingo.io."
}

# SPF needs configuration for TEM and webhosting at the same time
resource "scaleway_domain_record" "collectivite_fr_spf" {
  provider = scaleway.dns
  dns_zone = local.collectivite_fr_dns_zone
  name     = ""
  type     = "TXT"
  data     = "v=spf1 ${scaleway_tem_domain.collectivite_fr.spf_config} ip4:51.159.128.187 +a +mx ~all"
}

resource "scaleway_domain_zone" "annuaire_incubateur" {
  provider   = scaleway.dns
  project_id = module.project.scaleway_project
  domain     = var.common.dns_zone_incubateur
  subdomain  = "annuaire"
}
locals {
  dns_zone_annuaire = "${scaleway_domain_zone.annuaire_incubateur.subdomain}.${scaleway_domain_zone.annuaire_incubateur.domain}"
}
