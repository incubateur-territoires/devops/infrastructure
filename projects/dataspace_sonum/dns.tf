resource "scaleway_domain_zone" "project" {
  provider   = scaleway.dns
  project_id = module.project.scaleway_project

  domain    = var.common.dns_zone_incubateur
  subdomain = local.project_slug
}
moved {
  from = scaleway_domain_zone.zone
  to   = scaleway_domain_zone.project
}
locals {
  dns_zone = "${scaleway_domain_zone.project.subdomain}.${scaleway_domain_zone.project.domain}"
}
