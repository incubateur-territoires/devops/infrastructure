locals {
  project_name = "Dataspace Sonum"
  # tflint-ignore: terraform_unused_declarations
  project_slug = "dataspace-sonum"
}

module "project" {
  source               = "../generic"
  common               = var.common
  project_name         = local.project_name
  with_tf_project      = false
  with_record          = false
  with_wildcard_record = false
  with_scw_project     = true
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.default = scaleway.default
  }
}

locals {
  dataspace_sonum_gitlab_infrastructure_project_id = 61533889
}

resource "gitlab_project_variable" "scaleway_config" {
  for_each = {
    scaleway_organization_id = var.common.scaleway_organization_id
    scaleway_project_id      = module.project.scaleway_project_config.project_id
    scaleway_access_key      = module.project.scaleway_project_config.access_key
    scaleway_secret_key      = module.project.scaleway_project_config.secret_key
  }
  project     = local.dataspace_sonum_gitlab_infrastructure_project_id
  key         = "TF_VAR_${each.key}"
  value       = each.value
  raw         = true
  protected   = true
  masked      = true
  description = "Managed by terraform"
}
