module "tem" {
  source   = "../../modules/tem/"
  dns_zone = local.dns_zone
  providers = {
    scaleway.iam        = scaleway.iam
    scaleway.dns        = scaleway.dns
    scaleway.tem_domain = scaleway.project
  }
}
