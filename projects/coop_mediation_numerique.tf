module "coop_mediation_numerique" {
  source               = "./generic"
  common               = local.common
  project_name         = "Coop Mediation Numerique"
  with_record          = false
  with_wildcard_record = false
  with_scw_project     = true
  with_tf_project      = false
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.default = scaleway.default
  }
}
