module "la_base" {
  source               = "./generic"
  common               = local.common
  project_name         = "La Base"
  with_tf_project      = false
  with_scw_project     = true
  with_record          = false
  with_wildcard_record = false
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.default = scaleway.default
  }
}

resource "scaleway_domain_record" "inclusion_numerique_labase_ns0" {
  provider = scaleway.dns
  dns_zone = var.dns_zone_incubateur
  name     = "v2.labase"
  type     = "NS"
  data     = "ns0.dom.scw.cloud"
}
resource "scaleway_domain_record" "inclusion_numerique_labase_ns1" {
  provider = scaleway.dns
  dns_zone = var.dns_zone_incubateur
  name     = "v2.labase"
  type     = "NS"
  data     = "ns1.dom.scw.cloud"
}
