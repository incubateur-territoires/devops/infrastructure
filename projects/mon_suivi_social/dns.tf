resource "scaleway_domain_record" "dev" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "monsuivisocial.dev"
  type     = "ALIAS"
  data     = "anct-monsuivisocial-dev.osc-fr1.scalingo.io."
}
resource "scaleway_domain_record" "dev_app" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "monsuivisocial-app.dev"
  type     = "ALIAS"
  data     = "anct-monsuivisocial-dev.osc-fr1.scalingo.io."
}

resource "scaleway_domain_record" "demo" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "monsuivisocial-demo.dev"
  type     = "ALIAS"
  data     = "anct-monsuivisocial-demo.osc-fr1.scalingo.io."
}
resource "scaleway_domain_record" "demo_app" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "monsuivisocial-app-demo.dev"
  type     = "ALIAS"
  data     = "anct-monsuivisocial-demo.osc-fr1.scalingo.io."
}

resource "scaleway_domain_record" "monsuivisocial" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "monsuivisocial"
  type     = "ALIAS"
  data     = "anct-monsuivisocial-prod.osc-secnum-fr1.scalingo.io."
}
resource "scaleway_domain_record" "monsuivisocial_app" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "monsuivisocial-app"
  type     = "ALIAS"
  data     = "anct-monsuivisocial-prod.osc-secnum-fr1.scalingo.io."
}

resource "scaleway_domain_record" "metabase" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "metabase.monsuivisocial"
  type     = "CNAME"
  data     = "anct-monsuivisocial-metabase-prod.osc-secnum-fr1.scalingo.io."
}
resource "scaleway_domain_record" "zammad" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "zammad.monsuivisocial"
  type     = "CNAME"
  data     = "suivi-social-zammad.anct.cloud-ed.fr."
}

resource "scaleway_domain_record" "ovh_autodiscover" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "_autodiscover._tcp.monsuivisocial"
  type     = "SRV"
  data     = "0 0 443 pro1.mail.ovh.net."
}

resource "scaleway_domain_record" "ovh_mx0" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "monsuivisocial"
  type     = "MX"
  data     = "mx0.mail.ovh.net."
  priority = 1
}
resource "scaleway_domain_record" "ovh_mx1" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "monsuivisocial"
  type     = "MX"
  data     = "mx1.mail.ovh.net."
  priority = 5
}
resource "scaleway_domain_record" "ovh_mx2" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "monsuivisocial"
  type     = "MX"
  data     = "mx2.mail.ovh.net."
  priority = 50
}
resource "scaleway_domain_record" "ovh_mx3" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "monsuivisocial"
  type     = "MX"
  data     = "mx3.mail.ovh.net."
  priority = 100
}

resource "scaleway_domain_record" "scw_tem_spf" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "monsuivisocial"
  type     = "TXT"
  data     = "v=spf1 ${scaleway_tem_domain.domain.spf_config} include:mx.ovh.com -all"
}
resource "scaleway_domain_record" "scw_tem_dkim" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "${module.project.scaleway_project_config.project_id}._domainkey.monsuivisocial"
  type     = "TXT"
  data     = scaleway_tem_domain.domain.dkim_config
}
resource "scaleway_domain_record" "scw_tem_dmarc" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "_dmarc.monsuivisocial"
  type     = "TXT"
  data     = scaleway_tem_domain.domain.dmarc_config
}

resource "scaleway_domain_record" "baleen_acme_challenge" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "_acme-challenge.monsuivisocial"
  type     = "CNAME"
  data     = "_acme.baleen.cshield.net."
  ttl      = 60
}
