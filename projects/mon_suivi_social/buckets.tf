resource "scaleway_object_bucket" "website_prod" {
  provider = scaleway.project
  name     = "${local.project_slug}-website-prod"
  lifecycle {
    ignore_changes = [tags]
  }
}
resource "scaleway_object_bucket" "website_demo" {
  provider = scaleway.project
  name     = "${local.project_slug}-website-demo"
  lifecycle {
    ignore_changes = [tags]
  }
}
resource "scaleway_object_bucket" "website_dev" {
  provider = scaleway.project
  name     = "${local.project_slug}-website-dev"
  lifecycle {
    ignore_changes = [tags]
  }
}
