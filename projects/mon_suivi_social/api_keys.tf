module "tem_api_key" {
  source      = "../../modules/scw_api_key"
  name        = "Monsuivisocial TEM key"
  project_id  = module.project.scaleway_project_config.project_id
  description = "Used by the application to send mail using scaleway's TEM"
  permissions = ["TransactionalEmailEmailFullAccess"]
  providers = {
    scaleway.iam = scaleway.iam
  }
}

module "object_storage_api_key" {
  source      = "../../modules/scw_api_key"
  name        = "Monsuivisocial Object storage api key"
  project_id  = module.project.scaleway_project_config.project_id
  permissions = ["ObjectStorageBucketsRead", "ObjectStorageObjectsRead", "ObjectStorageObjectsWrite", "ObjectStorageObjectsDelete"]
  description = "Used by the showcase website to access object storage"
  providers = {
    scaleway.iam = scaleway.iam
  }
}
