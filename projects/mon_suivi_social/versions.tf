terraform {
  required_providers {
    scaleway = {
      source = "scaleway/scaleway"
      configuration_aliases = [
        scaleway.iam,
        scaleway.dns,
        scaleway.default,
      ]
    }
  }
  required_version = ">= 0.14"
}
