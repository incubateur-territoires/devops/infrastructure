resource "scaleway_tem_domain" "domain" {
  provider   = scaleway.project
  accept_tos = true
  name       = var.domain
}
