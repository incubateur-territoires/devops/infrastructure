locals {
  project_slug = "mon-suivi-social"
}
module "project" {
  source               = "../generic"
  common               = var.common
  project_name         = "Monsuivisocial"
  startup_group        = "monsuivisocial"
  with_record          = false
  with_wildcard_record = false
  with_scw_project     = true
  with_tf_project      = false
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.default = scaleway.default
  }
}
