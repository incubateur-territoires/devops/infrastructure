resource "scaleway_vpc_private_network" "cal_com" {
  provider = scaleway.project
  name     = local.project_slug
}

resource "scaleway_instance_ip" "vm" {
  provider = scaleway.project
}
resource "scaleway_instance_server" "vm" {
  provider = scaleway.project
  type     = "GP1-S"
  name     = "cal-com"
  image    = "ubuntu_noble"
  ip_id    = scaleway_instance_ip.vm.id
  private_network {
    pn_id = scaleway_vpc_private_network.cal_com.id
  }
}

resource "scaleway_domain_record" "domain_record" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  data     = scaleway_instance_ip.vm.address
  type     = "A"
  name     = "calcom"
}
resource "scaleway_tem_domain" "tem_domain" {
  provider   = scaleway.project
  accept_tos = true
  name       = scaleway_domain_record.domain_record.fqdn
}
resource "scaleway_domain_record" "spf" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = scaleway_domain_record.domain_record.name
  type     = "TXT"
  data     = "v=spf1 ${scaleway_tem_domain.tem_domain.spf_config} -all"
}
resource "scaleway_domain_record" "mx" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = scaleway_domain_record.domain_record.name
  type     = "MX"
  data     = "blackhole.scw-tem.cloud."
}
resource "scaleway_domain_record" "dkim" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "${module.project.scaleway_project_config.project_id}._domainkey.${scaleway_domain_record.domain_record.name}"
  type     = "TXT"
  data     = scaleway_tem_domain.tem_domain.dkim_config
}
resource "scaleway_domain_record" "dmarc" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "_dmarc.${scaleway_domain_record.domain_record.name}"
  type     = "TXT"
  data     = scaleway_tem_domain.tem_domain.dmarc_config
}
module "tem_api_key" {
  source      = "../../modules/scw_api_key"
  project_id  = module.project.scaleway_project_config.project_id
  name        = "Cal Com TEM API key"
  description = "Used for Cal.com deployment to send notification emails using TEM (${var.common.managed})"
  permissions = ["TransactionalEmailEmailFullAccess"]
  providers = {
    scaleway.iam = scaleway.iam
  }
}
