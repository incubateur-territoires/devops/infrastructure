data "gitlab_group" "projects" {
  group_id = 15577471
}

variable "dns_zone_incubateur" {
  type = string
}
variable "production_cluster_cname" {
  type = string
}
variable "development_cluster_cname" {
  type = string
}

variable "development_base_domain" {
  type = string
}
variable "production_base_domain" {
  type = string
}
variable "scaleway_organization_id" {
  type = string
}
variable "scaleway_cluster_development_cluster_id" {
  type = string
}
variable "scaleway_cluster_production_cluster_id" {
  type = string
}
variable "scaleway_default_project_id" {
  type = string
}

variable "grafana_production_url" {
  type = string
}
variable "grafana_production_auth" {
  type = string
}
variable "grafana_production_loki_url" {
  type = string
}
variable "grafana_production_prometheus_url" {
  type = string
}

variable "grafana_development_url" {
  type = string
}
variable "grafana_development_auth" {
  type = string
}
variable "grafana_development_loki_url" {
  type = string
}
variable "grafana_development_prometheus_url" {
  type = string
}

variable "scaleway_development_public_gw_ip" {
  type = string
}
variable "scaleway_production_public_gw_ip" {
  type = string
}

variable "scaleway_production_application_id" {
  type = string
}
variable "scaleway_development_application_id" {
  type = string
}
variable "scaleway_cluster_development_project_id" {
  type = string
}
variable "scaleway_cluster_production_project_id" {
  type = string
}

variable "loadbalancer_ips" {
  type = object({
    haproxy_dev      = string
    haproxy_dev_bis  = string
    haproxy_prod     = string
    haproxy_prod_bis = string
  })
}

variable "kubeconfig_development" {
  type = object({
    host                   = string
    token                  = string
    cluster_ca_certificate = string
  })
}
variable "kubeconfig_production" {
  type = object({
    host                   = string
    token                  = string
    cluster_ca_certificate = string
  })
}

variable "iam_admin_user_ids" {
  type        = list(string)
  description = "List of users that are defined as admins in IAM. Can be used to give those users specific rights over each project's resources."
}

locals {
  common = {
    projects_group_id                       = data.gitlab_group.projects.id
    dns_zone_incubateur                     = var.dns_zone_incubateur
    production_cluster_cname                = var.production_cluster_cname
    development_cluster_cname               = var.development_cluster_cname
    development_base_domain                 = var.development_base_domain
    production_base_domain                  = var.production_base_domain
    scaleway_organization_id                = var.scaleway_organization_id
    scaleway_cluster_development_cluster_id = var.scaleway_cluster_development_cluster_id
    scaleway_cluster_production_cluster_id  = var.scaleway_cluster_production_cluster_id
    grafana_production_url                  = var.grafana_production_url
    grafana_production_auth                 = var.grafana_production_auth
    grafana_production_loki_url             = var.grafana_production_loki_url
    grafana_production_prometheus_url       = var.grafana_production_prometheus_url
    grafana_development_url                 = var.grafana_development_url
    grafana_development_auth                = var.grafana_development_auth
    grafana_development_loki_url            = var.grafana_development_loki_url
    grafana_development_prometheus_url      = var.grafana_development_prometheus_url
    scaleway_development_public_gw_ip       = var.scaleway_development_public_gw_ip
    scaleway_production_public_gw_ip        = var.scaleway_production_public_gw_ip
    scaleway_development_application_id     = var.scaleway_development_application_id
    scaleway_production_application_id      = var.scaleway_production_application_id
    scaleway_development_project_id         = var.scaleway_cluster_development_project_id
    scaleway_production_project_id          = var.scaleway_cluster_production_project_id
    lb_ip_dev                               = var.loadbalancer_ips.haproxy_dev
    lb_ip_dev_bis                           = var.loadbalancer_ips.haproxy_dev_bis
    lb_ip_prod                              = var.loadbalancer_ips.haproxy_prod
    lb_ip_prod_bis                          = var.loadbalancer_ips.haproxy_prod_bis
    managed                                 = "Managed by Terraform"
    webhosting_contact_email                = "support@incubateur.anct.gouv.fr"
    webhosting_offer_lite                   = "fr-par/f5c2ae8f-7625-4bca-b711-b44bb3d08694"
    webhosting_offer_personal               = "fr-par/d86c61a0-a9e9-11ec-b909-0242ac120002"
  }
}

variable "mattermost_rdb_acl" {}
