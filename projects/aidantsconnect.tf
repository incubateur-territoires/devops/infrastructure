resource "scaleway_domain_record" "aidantsconnect_zammad" {
  provider = scaleway.dns
  dns_zone = var.dns_zone_incubateur
  name     = "zammad.aidantsconnect"
  type     = "ALIAS"
  data     = "aidants-connect-zammad.anct.cloud-ed.fr."
}
