module "inclusion_numerique" {
  source               = "./generic"
  common               = local.common
  project_name         = "Inclusion Numerique"
  with_tf_project      = false
  with_scw_project     = true
  with_record          = false
  with_wildcard_record = false
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.default = scaleway.default
  }
}

resource "scaleway_domain_record" "n8n_inclusion_numerique" {
  provider = scaleway.dns
  dns_zone = var.dns_zone_incubateur
  name     = "n8n.inclusion-numerique"
  type     = "CNAME"
  data     = "inclusion-numerique-n8n.osc-fr1.scalingo.io."
}
resource "scaleway_domain_record" "dashlord_inclusion_numerique" {
  provider = scaleway.dns
  dns_zone = var.dns_zone_incubateur
  name     = "dashlord.inclusion-numerique"
  type     = "CNAME"
  data     = "inclusion-numerique.github.io."
}
