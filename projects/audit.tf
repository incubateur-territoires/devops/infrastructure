resource "scaleway_domain_record" "audit_prod" {
  provider = scaleway.dns
  dns_zone = var.dns_zone_incubateur
  name     = "audit"
  type     = "ALIAS"
  data     = "survey-builder-anct-prod.osc-secnum-fr1.scalingo.io."
}
