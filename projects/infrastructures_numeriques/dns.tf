resource "scaleway_domain_record" "infrastructures_numeriques_mongodb_development" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "mongodb.infrastructures-numeriques.dev"
  type     = "A"
  data     = "51.159.112.110"
}

resource "scaleway_domain_record" "infrastructures_numeriques_mongodb_production" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "mongodb.infrastructures-numeriques"
  type     = "A"
  data     = "51.159.206.74"
}

resource "scaleway_domain_record" "infrastructures_numeriques" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "infrastructures-numeriques"
  type     = "A"
  data     = var.common.lb_ip_prod
}

resource "scaleway_domain_record" "infrastructures_numeriques_listmonk" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "listmonk.infrastructures-numeriques"
  type     = "A"
  data     = var.common.lb_ip_prod
}

resource "scaleway_domain_zone" "infrastructures_numeriques_dev" {
  provider   = scaleway.dns
  subdomain  = "tous-connectes.dev"
  domain     = var.common.dns_zone_incubateur
  project_id = module.project.scaleway_project_config.project_id
}

resource "scaleway_domain_zone" "infrastructures_numeriques" {
  provider   = scaleway.dns
  project_id = module.project.scaleway_project_config.project_id
  domain     = "tous-connectes.anct.gouv.fr"
  subdomain  = ""
}

resource "scaleway_domain_record" "infrastructures_numeriques_prod" {
  provider = scaleway.dns
  dns_zone = scaleway_domain_zone.infrastructures_numeriques.id
  type     = "ALIAS"
  data     = "toutes-tous-connectes-production.osc-secnum-fr1.scalingo.io."
}
resource "scaleway_domain_record" "infrastructures_numeriques_google_verification" {
  provider = scaleway.dns
  dns_zone = scaleway_domain_zone.infrastructures_numeriques.id
  type     = "TXT"
  data     = "\"google-site-verification=HnS7FgPaoCSyCtlj8SzleuUi4dENpG_1hOT9U2PzJw8\""
}
resource "scaleway_domain_record" "infrastructures_numeriques_acm_validation" {
  provider = scaleway.dns
  dns_zone = scaleway_domain_zone.infrastructures_numeriques.id
  name     = "_d218eca682607e62962e07ff4c1c6741"
  type     = "CNAME"
  data     = "_706bcc2e5d02ad677055398f6aee2281.tftwdmzmwn.acm-validations.aws."
}
resource "scaleway_domain_record" "infrastructures_numeriques_dev" {
  provider = scaleway.dns
  dns_zone = scaleway_domain_zone.infrastructures_numeriques.id
  name     = "dev"
  type     = "ALIAS"
  data     = "d-6x8tgmltld.execute-api.eu-west-3.amazonaws.com."
}
resource "scaleway_domain_record" "infrastructures_numeriques_doc" {
  provider = scaleway.dns
  dns_zone = scaleway_domain_zone.infrastructures_numeriques.id
  name     = "documentation"
  type     = "CNAME"
  data     = "11ba819e2f-hosting.gitbook.io."
}
resource "scaleway_domain_record" "infrastructures_numeriques_staging" {
  provider = scaleway.dns
  dns_zone = scaleway_domain_zone.infrastructures_numeriques.id
  name     = "staging"
  type     = "CNAME"
  data     = "toutes-tous-connectes-staging.osc-fr1.scalingo.io."
}
