resource "scaleway_object_bucket" "prod" {
  provider = scaleway.project
  name     = "${local.project_slug}-uploads-prod"
}
