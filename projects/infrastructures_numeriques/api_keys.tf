resource "scaleway_iam_application" "infrastructures_numeriques" {
  provider    = scaleway.iam
  name        = "Infrastructures numeriques listmonk"
  description = "Managed by terraform"
}

resource "scaleway_iam_api_key" "infrastructures_numeriques" {
  provider           = scaleway.iam
  application_id     = scaleway_iam_application.infrastructures_numeriques.id
  default_project_id = module.project.scaleway_project
  description        = "Managed by terraform"
}

output "infrastructures_numeriques_api" {
  value = scaleway_iam_api_key.infrastructures_numeriques.secret_key
}

module "infrastructures_numeriques_dev_api_key" {
  source      = "../../modules/scw_api_key"
  name        = "Infrastructures numeriques dev API key"
  description = "Used by infrastructure numerique developpers"
  permissions = ["AllProductsFullAccess"]
  project_id  = module.project.scaleway_project_config.project_id
  providers = {
    scaleway.iam = scaleway.iam
  }
}

module "infrastructures_numeriques_mail_api_key" {
  source      = "../../modules/scw_api_key"
  name        = "Infrastructures numeriques mail API key"
  description = "Used by infrastructure numerique application to use SCW TEM"
  permissions = ["TransactionalEmailEmailFullAccess"]
  project_id  = module.project.scaleway_project_config.project_id
  providers = {
    scaleway.iam = scaleway.iam
  }
}

module "bucket_api_key" {
  source      = "../../modules/scw_api_key"
  project_id  = module.project.scaleway_project_config.project_id
  permissions = ["ObjectStorageObjectsRead", "ObjectStorageObjectsDelete", "ObjectStorageObjectsWrite", "ObjectStorageBucketsRead"]
  name        = "${local.project_name} bucket access"
  description = "Permission for ${local.project_name} to read and write bucket objects"
  providers = {
    scaleway.iam = scaleway.iam
  }
}
