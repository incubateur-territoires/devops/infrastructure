locals {
  project_name = "Tous Connectes"
  # tflint-ignore: terraform_unused_declarations
  project_slug = "tous-connectes"
}
module "project" {
  source           = "../generic"
  common           = var.common
  project_name     = local.project_name
  project_slug     = "infrastructures-numeriques"
  startup_group    = "infrastructures-numeriques"
  with_record      = false
  with_scw_project = true
  scw_custom_groups = {
    "read backups" = ["ObjectStorageReadOnly"],
    "listmonk"     = ["TransactionalEmailEmailFullAccess", "ObjectStorageObjectsWrite", "ObjectStorageObjectsRead", "ObjectStorageObjectsDelete"]
    "dev"          = ["AllProductsFullAccess"]
  }
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.default = scaleway.default
  }
}
