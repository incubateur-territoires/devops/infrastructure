resource "scaleway_object_bucket" "test" {
  provider = scaleway.project
  name     = "umap-test"
  versioning {
    enabled = true
  }
}
