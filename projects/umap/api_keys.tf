module "bucket_api_key" {
  source      = "../../modules/scw_api_key"
  project_id  = module.project.scaleway_project_config.project_id
  permissions = ["ObjectStorageObjectsRead", "ObjectStorageObjectsWrite", "ObjectStorageBucketsRead", "ObjectStorageObjectsDelete"]
  name        = "${local.project_name} bucket access"
  description = "Permission for ${local.project_name} to read, write and delete bucket objects"
  providers = {
    scaleway.iam = scaleway.iam
  }
}
