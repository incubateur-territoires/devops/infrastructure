resource "scaleway_object_bucket" "staging" {
  provider = scaleway.project
  name     = "${local.project_slug}-staging"
}
resource "scaleway_object_bucket" "prod" {
  provider = scaleway.project
  name     = "${local.project_slug}-prod"
}
