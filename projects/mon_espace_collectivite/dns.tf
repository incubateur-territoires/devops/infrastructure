resource "scaleway_domain_zone" "project" {
  provider   = scaleway.dns
  project_id = module.project.scaleway_project_config.project_id
  domain     = var.common.dns_zone_incubateur
  subdomain  = "monespacecollectivite"
}
locals {
  dns_zone      = "${scaleway_domain_zone.project.subdomain}.${scaleway_domain_zone.project.domain}"
  anct_dns_zone = "monespacecollectivite.anct.gouv.fr"
}

resource "scaleway_domain_record" "project" {
  provider = scaleway.project
  dns_zone = local.dns_zone
  type     = "ALIAS"
  data     = "mon-espace-collectivite.recoconseil.fr."
  ttl      = 60
}

resource "scaleway_domain_record" "cname" {
  provider = scaleway.project
  dns_zone = local.anct_dns_zone
  type     = "ALIAS"
  data     = "mon-espace-collectivite-server.osc-secnum-fr1.scalingo.io."
}
