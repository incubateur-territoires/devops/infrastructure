# configuration that needs to be saved locally to run ansible
# extract the result from the state to use in
resource "null_resource" "ansible_config" {
  triggers = {
    "ansm_domain"         = "demo-ansm.dev.incubateur.anct.gouv.fr"
    "ansm_admin_username" = "adminansm"
    "ansm_admin_password" = random_password.admin_ansm.result
    "ansm_admin_fullname" = "Admin ANSM"
  }
}

resource "random_password" "admin_ansm" {
  length  = 64
  special = false # not sure it can work nicely when templated in yaml in ansible
}
