resource "scaleway_domain_record" "ansm_demo" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "demo-ansm.dev"
  type     = "A"
  data     = scaleway_instance_ip.yunohost.address
  ttl      = 60
}

resource "scaleway_domain_record" "ansm_demo_wildcard" {
  provider = scaleway.dns
  dns_zone = scaleway_domain_record.ansm_demo.dns_zone
  name     = "*.${scaleway_domain_record.ansm_demo.name}"
  type     = scaleway_domain_record.ansm_demo.type
  data     = scaleway_domain_record.ansm_demo.data
  ttl      = scaleway_domain_record.ansm_demo.ttl
}
