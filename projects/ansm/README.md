# ANSM demo instance

The ANSM project has a VM running Yunohost to showcase some open source tools.
This instance is created with terraform and the different tools on it are installed with ansible.

Being only a demo instance, this allows it to be recreated easily instead of trying to manage upgrades.

## Adding an SSH key to access the VM

For ansible to run correctly, you need your local ssh key to be [configured in Scaleway](https://console.scaleway.com/project/ssh-keys) for the ANSM project.
In order to trigger the VM to reload the SSH keys, reboot it from the Scaleway console.

## Generating secret config

The ansible roles used to install software on the VM need some configuration values and secrets.
These are generated in terraform, and need to be exported in a `.secret_config.json` file.

Run from the infrastructure repository root directory:
```bash
terraform state pull | jq '.resources[] | select(.name == "ansible_config" and .type == "null_resource" and .module == "module.projects.module.ansm_project") | .instances[].attributes.triggers' > projects/ansm/ansible/.secret_config.json
```

## Pulling roles dependency

Run from the ansible directory:
```bash
ansible-galaxy install -r requirements.yaml
```

## Running ansible

Once the config file is created, you can run ansible.
Run from the ansible directory:
```bash
ansible-playbook -i inventory.yaml playbook.yaml
```

## Additionnal manual configuration

Once ansible is done running, some manual steps were taken:
- In vaultwarden, user sign up is allowed (it is still behind the Yunohost SSO)
- In nextcloud, some apps were installed: office, collabora CODE server, agenda
- In nocodb, the admin user is created
