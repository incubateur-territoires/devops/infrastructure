resource "scaleway_instance_ip" "yunohost" {
  provider = scaleway.project
}
resource "scaleway_instance_server" "yunohost" {
  provider = scaleway.project
  type     = "GP1-S"
  name     = "demo-ansm"
  root_volume {
    size_in_gb  = 150
    volume_type = "b_ssd"
  }
  ip_id = scaleway_instance_ip.yunohost.id
  image = "debian_bullseye"
}
