locals {
  ansm_project_name = "ANSM"
}
module "project" {
  source       = "../generic"
  common       = var.common
  project_name = local.ansm_project_name

  with_record          = false
  with_wildcard_record = false
  with_scw_project     = true
  with_tf_project      = false
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.default = scaleway.default
  }
}
