locals {
  project_name = "PoC Keycloak Docker Compose"
  # tflint-ignore: terraform_unused_declarations
  project_slug = "poc-keycloak-docker-compose"
}
module "project" {
  source               = "../generic"
  common               = var.common
  project_name         = local.project_name
  with_tf_project      = false
  with_record          = false
  with_wildcard_record = false
  with_scw_project     = true
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.default = scaleway.default
  }
}
