locals {
  subdomain = "sso-poc-2.dev"
  dns_zone  = scaleway_domain_zone.zone.id
}
resource "scaleway_domain_zone" "zone" {
  provider   = scaleway.dns
  domain     = var.common.dns_zone_incubateur
  subdomain  = local.subdomain
  project_id = module.project.scaleway_project_config.project_id
}
resource "scaleway_domain_record" "keycloak" {
  dns_zone = local.dns_zone
  name     = ""
  type     = "A"
  data     = scaleway_instance_ip.server.address
}
resource "scaleway_domain_record" "mattermost" {
  dns_zone = local.dns_zone
  name     = "mm"
  type     = "A"
  data     = scaleway_instance_ip.server.address
}
resource "scaleway_domain_record" "sentry" {
  dns_zone = local.dns_zone
  name     = "sentry"
  type     = "A"
  data     = "163.172.169.25"
  ttl      = 60
}
