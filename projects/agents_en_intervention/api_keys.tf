# Setting up permissions for a different kind of deployment, where we use Scaleway containers instead of Kubernetes
module "infra_api_key" {
  source      = "../../modules/scw_api_key"
  name        = "aei terraform container deployment"
  description = "API key used to set up infrastructure for AeI's deployment using Scaleway containers"
  permissions = ["AllProductsFullAccess"]
  project_id  = module.project.scaleway_project
  providers = {
    scaleway.iam = scaleway.iam
  }
}
module "app_api_key" {
  source      = "../../modules/scw_api_key"
  name        = "aei application s3"
  description = "API key used by the application to access the S3 API"
  permissions = ["ObjectStorageObjectsRead", "ObjectStorageObjectsWrite", "ObjectStorageObjectsDelete"]
  project_id  = module.project.scaleway_project
  providers = {
    scaleway.iam = scaleway.iam
  }
}

module "tem_api_key" {
  source      = "../../modules/scw_api_key"
  name        = "${local.project_slug} TEM key"
  project_id  = module.project.scaleway_project_config.project_id
  description = "Used by the ${local.project_name} application to send mail using scaleway's TEM"
  permissions = ["TransactionalEmailEmailFullAccess"]
  providers = {
    scaleway.iam = scaleway.iam
  }
}

data "scaleway_secret" "terraform_vars" {
  provider = scaleway.project
  name     = "variables"
  path     = "/terraform"
}
data "scaleway_secret_version" "terraform_vars" {
  provider  = scaleway.project
  secret_id = data.scaleway_secret.terraform_vars.id
  revision  = "latest"
}
resource "scaleway_secret_version" "terraform_vars" {
  provider  = scaleway.project
  secret_id = data.scaleway_secret.terraform_vars.id
  data      = local.new_secret_value
}
locals {
  old_secret_value = jsondecode(base64decode(data.scaleway_secret_version.terraform_vars.data))
  new_secret_value = jsonencode(merge(local.old_secret_value, local.to_merge_secret_value))
  to_merge_secret_value = {
    scaleway_config = {
      organization_id = var.common.scaleway_organization_id
      project_id      = module.project.scaleway_project
      access_key      = module.infra_api_key.access_key
      secret_key      = module.infra_api_key.secret_key
    }
    scaleway_s3_config = {
      organization_id = var.common.scaleway_organization_id
      project_id      = module.project.scaleway_project
      access_key      = module.app_api_key.access_key
      secret_key      = module.app_api_key.secret_key
    }
    scaleway_tem_config = {
      secret_key = module.tem_api_key.secret_key
    }
  }
}

