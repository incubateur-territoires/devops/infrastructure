locals {
  project_name = "Agents En Intervention"
  project_slug = "agents-intervention"
}
module "project" {
  source               = "../generic"
  common               = var.common
  project_name         = local.project_name
  project_slug         = local.project_slug
  with_record          = true
  with_scw_project     = true
  with_wildcard_record = true
  startup_group        = local.project_slug
  scw_custom_groups = {
    "developer" = ["AllProductsReadOnly", "SecretManagerFullAccess", "ServerlessJobsFullAccess"]
  }
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.default = scaleway.default
  }
}
