resource "scaleway_domain_record" "espace_demande_dev" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "espace-demande.dev"
  type     = "CNAME"
  data     = "dev.espace-sur-demande.fr."
}
resource "scaleway_domain_record" "espace_demande_dev_wildcard" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "*.espace-demande.dev"
  type     = "A"
  data     = var.common.lb_ip_dev_bis
}

resource "scaleway_domain_record" "espace_demande_demo" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "esd-demo.dev"
  type     = "CNAME"
  data     = "demo.espace-sur-demande.fr."
}

resource "scaleway_domain_record" "zammad" {
  provider = scaleway.dns
  dns_zone = var.common.dns_zone_incubateur
  name     = "zammad.espace-sur-demande"
  type     = "ALIAS"
  data     = "zammad.espace-sur-demande.anct.cloud-ed.fr."
}
