locals {
  project_name           = "Espace Sur Demande"
  gitlab_repo_project_id = 55950625
}

module "project" {
  source           = "../generic"
  common           = var.common
  project_name     = local.project_name
  project_slug     = "espace-demande"
  with_scw_project = true

  with_wildcard_record = false
  with_record          = false

  scw_custom_groups = {
    "dev" = ["AllProductsReadOnly", "ObservabilityFullAccess", "SecretManagerFullAccess"]
  }
  startup_group = "espace-sur-demande"
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.default = scaleway.default
  }
}

resource "gitlab_project_variable" "scaleway_access_key" {
  project = local.gitlab_repo_project_id
  key     = "TF_VAR_scaleway_access_key"
  value   = module.project.scaleway_project_config.access_key
  masked  = true
}

resource "gitlab_project_variable" "scaleway_secret_key" {
  project = local.gitlab_repo_project_id
  key     = "TF_VAR_scaleway_secret_key"
  value   = module.project.scaleway_project_config.secret_key
  masked  = true
}
