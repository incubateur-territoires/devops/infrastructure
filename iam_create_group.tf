locals {
  support_manager = {
    name        = "Support tickets manager"
    description = "Managed by terraform"
  }
}
# Creating a generic policy to manually assign users to use support tickets
resource "scaleway_iam_group" "support_manager" {
  provider    = scaleway.iam
  name        = local.support_manager.name
  description = local.support_manager.description
  lifecycle { ignore_changes = [user_ids] }
}
resource "scaleway_iam_policy" "support_manager" {
  provider    = scaleway.iam
  name        = local.support_manager.name
  description = local.support_manager.description
  group_id    = scaleway_iam_group.support_manager.id
  rule {
    organization_id      = var.scaleway_organization_id
    permission_set_names = ["SupportTicketManager"]
  }
}
