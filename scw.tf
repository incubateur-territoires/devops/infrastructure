locals {
  managed = "Managed by Terraform"
}
resource "scaleway_account_project" "default" {
  provider    = scaleway.iam
  name        = "default"
  description = local.managed
  lifecycle {
    prevent_destroy = true
  }
}

resource "scaleway_account_project" "k8s_prod" {
  provider    = scaleway.iam
  name        = "Kubernetes production"
  description = local.managed
  lifecycle {
    prevent_destroy = true
  }
}
locals {
  scaleway_cluster_development_project_config = {
    project_id = scaleway_account_project.k8s_dev.id
    access_key = scaleway_iam_api_key.k8s_development.access_key
    secret_key = scaleway_iam_api_key.k8s_development.secret_key
  }
  scaleway_cluster_production_project_config = {
    project_id = scaleway_account_project.k8s_prod.id
    access_key = scaleway_iam_api_key.k8s_production.access_key
    secret_key = scaleway_iam_api_key.k8s_production.secret_key
  }
  scaleway_default_project_config = {
    project_id = scaleway_account_project.default.id
    access_key = module.default_project_api_key.access_key
    secret_key = module.default_project_api_key.secret_key
  }
}

resource "scaleway_account_project" "k8s_dev" {
  provider    = scaleway.iam
  name        = "Kubernetes development"
  description = local.managed
  lifecycle {
    prevent_destroy = true
  }
}
