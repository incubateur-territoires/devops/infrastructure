# Infrastructure Incubateur des territoires

A large part of this documentation is shared for both the main infrastructure projet (this repository) and startup projects (the repositories under the `projects` group).
To avoid duplicating instructions across multiple repositories, this contains the instructions for all repositories.
Some parts can be skipped depending on the type of repository these instructions apply to.
These are marked in the titles.

## Project setup

This project uses [OpenTofu version 1.7.2](https://opentofu.org/docs/intro/install/) to manage infrastructure.
Follow these instructions if you need to run `tofu` locally instead of relying on the CI/CD pipeline.

### Configure state backend (for both infra and startup projects)

Go to the repository's terraform states in Gitlab (Left menu > Operate > Terraform states) and under Actions copy the terraform init command.
Replace the `terraform` command by `tofu`.
Run it by providing *your own personal access token*.

This step is also necessary when working on startup infrastructure projects.

### Gitlab token (only for infra project)

This terraform code requires a GITLAB_TOKEN environment variable to be able to run.
This token must have permissions over the `incubateur-territoires` group.
When the token used by the CI gets revoked or expires, create a new one under [the group's access tokens](https://gitlab.com/groups/incubateur-territoires/-/settings/access_tokens) with the Maintainer role and api scope and fill it in the [project's CI variables](https://gitlab.com/incubateur-territoires/devops/infrastructure/-/settings/ci_cd).

This step is not required for startup infrastructure projects.

### Fetching variables

#### Variable file (for both infra and startup project)

The non public variables used by `tofu` are available in the project's CI/CD variables.
These can be fetched manually, or by using the [`glab`](https://gitlab.com/gitlab-org/cli/#installation) command line tool.

```bash
_UMASK=$(umask) ; umask 077
glab variable get VAR_FILE > terraform.tfvars
umask $_UMASK
```

#### Discrete variables (only for startup projects)

For startup projects the main var file can be fetched with the same instructions as above.
However, there are also additionnal values that are configured using CI environment variables.
These can all be fetched automatically by using the [`glab`](https://gitlab.com/gitlab-org/cli/#installation) and [`jq`](https://jqlang.github.io/jq/download/) command line tools.

```bash
_UMASK=$(umask) ; umask 077
glab variable list -F json | \
  jq '[.[] | select(.variable_type == "env_var") | {(.key | sub("^TF_VAR_";"")) : (.value as $val | ($val | try fromjson catch $val) )}] | add' \
  > env.auto.tfvars.json
umask $_UMASK
```

The generated `terraform.tfvars` and `env.auto.tfvars.json` files are **sensitive** and **must be kept secret**.

### Basic usage (for both infra and startup projects)

A more detailed explanation can be found [in OpenTofu's docs](https://opentofu.org/docs/cli/commands/).

#### Plan changes and save the plan to a file

```bash
tofu plan -out plan.tfplan
```

#### Show the changes in a plan file

```bash
tofu show plan.tfplan
```

#### Apply a plan file

```bash
tofu apply plan.tfplan
```

#### Plan changes for a specific resource or module

```bash
tofu plan -out plan.tfplan -target module.dev # adapt the module name
# or
tofu plan -out plan.tfplan -target kubernetes_secret.db_settings # adapt the resource type and module name
```

### Advanced usage

#### Show custom diff including sensitive values

Easier to see changes in helm releases.
```bash
./utils/tfplandiff plan.tfplan
```

## Misc documentation (only for infra project)

- [How to add an external DNS record](./documentation/add_external_dns.md)
- [How to create a new project](./documentation/create_new_project.md)
- [Common issues with certificate renewal](./documentation/issues_cert_renewal.md)
- [Configuring monitoring for a specific team](./documentation/monitoring.md)
- [Our guidelines for resource quotas](./documentation/resource_quotas.md)
