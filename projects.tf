module "projects" {
  source = "./projects"
  providers = {
    scaleway.default = scaleway.default_project
    scaleway.iam     = scaleway.iam
    scaleway.dns     = scaleway.dns
  }
  development_cluster_cname = module.cluster_development.cluster_cname
  dns_zone_incubateur       = local.dns_zone_incubateur
  production_cluster_cname  = module.cluster_production.cluster_cname
  development_base_domain   = var.development_base_domain
  production_base_domain    = var.production_base_domain

  scaleway_organization_id                = var.scaleway_organization_id
  scaleway_cluster_development_cluster_id = module.cluster_development.scaleway_cluster_id
  scaleway_cluster_production_cluster_id  = module.cluster_production.scaleway_cluster_id
  scaleway_default_project_id             = scaleway_account_project.default.id

  grafana_development_auth           = module.cluster_development.grafana_auth
  grafana_development_url            = module.cluster_development.grafana_url
  grafana_development_loki_url       = module.cluster_development.loki_url
  grafana_development_prometheus_url = module.cluster_development.prometheus_url

  grafana_production_auth           = module.cluster_production.grafana_auth
  grafana_production_url            = module.cluster_production.grafana_url
  grafana_production_loki_url       = module.cluster_production.loki_url
  grafana_production_prometheus_url = module.cluster_production.prometheus_url

  scaleway_development_public_gw_ip = module.cluster_development.public_gw_ip
  scaleway_production_public_gw_ip  = module.cluster_production.public_gw_ip

  scaleway_production_application_id      = scaleway_iam_application.k8s_production.id
  scaleway_development_application_id     = scaleway_iam_application.k8s_development.id
  scaleway_cluster_development_project_id = scaleway_account_project.k8s_dev.id
  scaleway_cluster_production_project_id  = scaleway_account_project.k8s_prod.id

  kubeconfig_development = module.cluster_development.kubeconfig
  kubeconfig_production  = module.cluster_production.kubeconfig

  iam_admin_user_ids = data.scaleway_iam_group.admin.user_ids

  mattermost_rdb_acl = module.cluster_production.mattermost_legacy_rdb_acl

  loadbalancer_ips = {
    haproxy_dev      = module.cluster_development.haproxy_ip
    haproxy_dev_bis  = module.cluster_development.haproxy_ip_bis
    haproxy_prod     = module.cluster_production.haproxy_ip
    haproxy_prod_bis = module.cluster_production.haproxy_ip_bis
  }
}
