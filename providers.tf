# bogus provider to make sure we don't use the local scw configuration
provider "scaleway" {
  region          = "fr-par"
  zone            = "fr-par-1"
  organization_id = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
  project_id      = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
  access_key      = "SCWXXXXXXXXXXXXXXXXX"
  secret_key      = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
}

provider "scaleway" {
  alias           = "default_project"
  region          = "fr-par"
  zone            = "fr-par-1"
  organization_id = var.scaleway_organization_id
  project_id      = scaleway_account_project.default.id
  access_key      = module.default_project_api_key.access_key
  secret_key      = module.default_project_api_key.secret_key
}

provider "scaleway" {
  alias           = "iam"
  region          = "fr-par"
  zone            = "fr-par-1"
  organization_id = var.scaleway_organization_id
  project_id      = var.scaleway_organization_id # it is the same id for the org and the default project
  access_key      = var.scaleway_iam_access_key
  secret_key      = var.scaleway_iam_secret_key
}

# special provider with rights on DNS across all projects to handle zones
provider "scaleway" {
  alias           = "dns"
  region          = "fr-par"
  zone            = "fr-par-1"
  organization_id = var.scaleway_organization_id
  project_id      = scaleway_account_project.default.id
  access_key      = scaleway_iam_api_key.allprojectsdns_key.access_key
  secret_key      = scaleway_iam_api_key.allprojectsdns_key.secret_key
}

provider "grafana" {
  alias = "production"
  url   = module.cluster-production.grafana_url
  auth  = module.cluster-production.grafana_auth
}

provider "grafana" {
  alias = "development"
  url   = module.cluster-development.grafana_url
  auth  = module.cluster-development.grafana_auth
}

provider "ovh" {
  endpoint           = "ovh-eu"
  application_key    = var.ovh_application_key
  application_secret = var.ovh_application_secret
  consumer_key       = var.ovh_consumer_key
}

provider "gitlab" {
  early_auth_check = false
}
