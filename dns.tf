locals {
  dns_zone_incubateur = "incubateur.anct.gouv.fr"
}

resource "scaleway_domain_record" "sib_code" {
  provider = scaleway.default_project
  dns_zone = local.dns_zone_incubateur
  name     = ""
  type     = "TXT"
  data     = "Sendinblue-code:91c9094242d88d2c66c093f32d44895d"
}
resource "scaleway_domain_record" "sib_img" {
  provider = scaleway.default_project
  dns_zone = local.dns_zone_incubateur
  name     = "img"
  type     = "CNAME"
  data     = "img.mailin.fr."
}
resource "scaleway_domain_record" "sib_r" {
  provider = scaleway.default_project
  dns_zone = local.dns_zone_incubateur
  name     = "r"
  type     = "CNAME"
  data     = "r.mailin.fr."
}

resource "scaleway_domain_record" "spf" {
  provider = scaleway.default_project
  dns_zone = local.dns_zone_incubateur
  name     = ""
  type     = "TXT"
  data     = "v=spf1 include:_spf.ox.numerique.gouv.fr include:spf.sendinblue.com mx ~all"
  ttl      = 60
}
resource "scaleway_domain_record" "dmarc" {
  provider = scaleway.default_project
  dns_zone = local.dns_zone_incubateur
  name     = "_dmarc"
  type     = "TXT"
  data     = "v=DMARC1; p=none; sp=none; rua=mailto:dmarc@mailinblue.com!10m; ruf=mailto:dmarc@mailinblue.com!10m; rf=afrf; pct=100; ri=86400"
}
resource "scaleway_domain_record" "dkim_mail" {
  provider = scaleway.default_project
  dns_zone = local.dns_zone_incubateur
  name     = "mail._domainkey"
  type     = "TXT"
  data     = "k=rsa;p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDeMVIzrCa3T14JsNY0IRv5/2V1/v2itlviLQBwXsa7shBD6TrBkswsFUToPyMRWC9tbR/5ey0nRBH0ZVxp+lsmTxid2Y2z+FApQ6ra2VsXfbJP3HE6wAO0YTVEJt1TmeczhEd2Jiz/fcabIISgXEdSpTYJhb0ct0VJRxcg4c8c7wIDAQAB"
}

resource "scaleway_domain_record" "dashlord" {
  provider = scaleway.default_project
  dns_zone = local.dns_zone_incubateur
  name     = "dashlord"
  data     = "incubateur-territoires.github.io."
  type     = "CNAME"
}

resource "scaleway_domain_record" "support" {
  provider = scaleway.default_project
  dns_zone = local.dns_zone_incubateur
  name     = "support"
  data     = "support-incubateur.anct.cloud-ed.fr."
  type     = "CNAME"
  ttl      = 60
}

moved {
  from = module.cluster_production.scaleway_domain_record.zammad
  to   = module.projects.scaleway_domain_record.mon_stage_de_seconde_zammad
}

resource "scaleway_domain_record" "verify_email" {
  provider = scaleway.default_project
  dns_zone = local.dns_zone_incubateur
  name     = "verify-email"
  data     = "51.159.157.231"
  type     = "A"
}
