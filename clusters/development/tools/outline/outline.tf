module "namespace" {
  source            = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version           = "1.0.1"
  max_cpu_requests  = "8"
  max_memory_limits = "10Gi"
  namespace         = "outline"
  project_name      = "Outline"
  project_slug      = "outline"

  default_container_cpu_requests  = "20m"
  default_container_memory_limits = "16Mi"
}
resource "random_password" "secret_key" {
  length           = 64
  override_special = "0123456789abcdef"
  min_special      = 64
}
resource "random_password" "utils_secret" {
  length           = 64
  override_special = "0123456789abcdef"
  min_special      = 64
}

resource "scaleway_object_bucket" "outline" {
  provider = scaleway.default-project
  name     = "incubateur-dev-outline"
  region   = "fr-par"
  cors_rule {
    allowed_headers = []
    allowed_methods = [
      "GET",
      "POST",
    ]
    allowed_origins = [
      "https://${var.hostname}",
    ]
    expose_headers  = []
    max_age_seconds = 0
  }
}
resource "scaleway_object_bucket_acl" "outline" {
  provider = scaleway.default-project
  bucket   = scaleway_object_bucket.outline.name
  acl      = "public-read"
}
resource "scaleway_object_bucket_website_configuration" "outline" {
  provider = scaleway.default-project
  bucket   = scaleway_object_bucket.outline.name
  index_document {
    suffix = "index.html"
  }
  error_document {
    key = "index.html"
  }
}

module "outline" {
  source  = "gitlab.com/vigigloo/tools-k8s/outline"
  version = "0.1.2"

  chart_name    = "outline"
  chart_version = "0.1.1"
  namespace     = module.namespace.namespace

  image_repository = "registry.gitlab.com/incubateur-territoires/incubateur/outline-mattermost-support"
  image_tag        = "v0.70.2-rc1"
  values = [
    file("${path.module}/outline.yaml"),
    <<-EOT
    postgresql:
      postgresqlUrl: ${module.postgresql.uri}
    EOT
  ]

  requests_cpu    = "10m"
  requests_memory = "500Mi"
  limits_memory   = "500Mi"

  ingress_host        = var.hostname
  outline_secretKey   = random_password.secret_key.result
  outline_utilsSecret = random_password.utils_secret.result
  outline_url         = "https://${var.hostname}"

  outline_storage_s3_bucket        = scaleway_object_bucket.outline.name
  outline_storage_s3_endpoint      = "https://s3.fr-par.scw.cloud"
  outline_storage_s3_region        = "fr-par"
  outline_storage_s3_uploadMaxSize = "5368709120" # 5G
  outline_storage_s3_key           = var.scaleway_default_access_key
  outline_storage_s3_secret        = var.scaleway_default_secret_key
  outline_storage_s3_acl           = scaleway_object_bucket_acl.outline.acl

  outline_redis_redisUrl = "redis://${helm_release.redis.metadata[0].name}-master.${helm_release.redis.metadata[0].namespace}:6379"

  outline_thirdPartyAuth_openidConnect_tokenUri    = "https://${var.oauth_domain}/oauth/access_token"
  outline_thirdPartyAuth_openidConnect_authUri     = "https://${var.oauth_domain}/oauth/authorize"
  outline_thirdPartyAuth_openidConnect_userInfoUri = "https://${var.oauth_domain}/api/v4/users/me"

  outline_email_reply         = "outline@incubateur.anct.gouv.fr"
  outline_email_from          = "outline@incubateur.anct.gouv.fr"
  outline_email_smtp_host     = var.outline_email_smtp_host
  outline_email_smtp_user     = var.outline_email_smtp_user
  outline_email_smtp_password = var.outline_email_smtp_password

  outline_thirdPartyAuth_mattermost_tokenUri     = "https://${var.oauth_domain}/oauth/access_token"
  outline_thirdPartyAuth_mattermost_authorizeUri = "https://${var.oauth_domain}/oauth/authorize"
  outline_thirdPartyAuth_mattermost_profileUri   = "https://${var.oauth_domain}/api/v4/users/me"
  outline_thirdPartyAuth_mattermost_clientId     = var.outline_client_id
  outline_thirdPartyAuth_mattermost_clientSecret = var.outline_client_secret
}
