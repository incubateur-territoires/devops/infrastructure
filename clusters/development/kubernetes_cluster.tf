resource "scaleway_vpc_private_network" "kubernetes" {
  name = "kubernetes-incubateur-development"
  tags = ["kubernetes", "development"]
}

resource "scaleway_vpc_public_gateway_ip" "public_gateway" {
}
resource "scaleway_vpc_public_gateway" "gateway" {
  name        = "kubernetes-gateway"
  type        = "VPC-GW-M"
  enable_smtp = true
  ip_id       = scaleway_vpc_public_gateway_ip.public_gateway.id
}

resource "scaleway_vpc_gateway_network" "main" {
  gateway_id         = scaleway_vpc_public_gateway.gateway.id
  private_network_id = scaleway_vpc_private_network.kubernetes.id
  ipam_config {
    push_default_route = true
  }
}

resource "scaleway_k8s_cluster" "cluster" {
  name               = "Incubateur-development"
  description        = "Managed by terraform"
  version            = "1.28"
  cni                = "calico"
  tags               = ["development", "terraform"]
  project_id         = var.scaleway_project_config.project_id
  private_network_id = scaleway_vpc_private_network.kubernetes.id
  auto_upgrade {
    enable                        = true
    maintenance_window_day        = "monday"
    maintenance_window_start_hour = 2
  }

  delete_additional_resources = false
  lifecycle {
    prevent_destroy = true
  }
}

resource "scaleway_k8s_pool" "small_isolated" {
  cluster_id          = scaleway_k8s_cluster.cluster.id
  name                = "small-isolated"
  node_type           = "GP1-XS"
  size                = 1
  autohealing         = true
  wait_for_pool_ready = true
  autoscaling         = true
  min_size            = 11
  max_size            = 16
  public_ip_disabled  = true
  lifecycle {
    prevent_destroy = true
  }
}

module "cluster_configuration" {
  source     = "../../modules/sysctl_configuration"
  depends_on = [scaleway_k8s_cluster.cluster]
}

output "public_gw_ip" {
  value = scaleway_vpc_public_gateway_ip.public_gateway.address
}
