variable "dns_zone_incubateur" {
  type = string
}
variable "scaleway_cert_manager_api_key" {
  type = object({
    access_key = string
    secret_key = string
  })
}

variable "scaleway_organization_id" {
  type = string
}

variable "scaleway_default_project_config" {
  type = object({
    project_id = string
    access_key = string
    secret_key = string
  })
  sensitive = true
}

variable "scaleway_project_config" {
  type = object({
    project_id = string
    access_key = string
    secret_key = string
  })
  sensitive = true
}

variable "sendinblue_smtp_host" {
  type = string
}
variable "sendinblue_smtp_user" {
  type = string
}
variable "sendinblue_smtp_password" {
  type      = string
  sensitive = true
}
variable "sendinblue_smtp_port" {
  type = string
}

variable "outline_client_id" {
  type = string
}
variable "outline_client_secret" {
  type      = string
  sensitive = true
}
variable "outline_oauth_domain" {
  type = string
}
