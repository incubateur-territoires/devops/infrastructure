provider "kubernetes" {
  host                   = scaleway_k8s_cluster.cluster.kubeconfig[0].host
  token                  = scaleway_k8s_cluster.cluster.kubeconfig[0].token
  cluster_ca_certificate = base64decode(scaleway_k8s_cluster.cluster.kubeconfig[0].cluster_ca_certificate)
}

provider "helm" {
  kubernetes {
    host                   = scaleway_k8s_cluster.cluster.kubeconfig[0].host
    token                  = scaleway_k8s_cluster.cluster.kubeconfig[0].token
    cluster_ca_certificate = base64decode(scaleway_k8s_cluster.cluster.kubeconfig[0].cluster_ca_certificate)
  }
}

provider "scaleway" {
  region          = "fr-par"
  zone            = "fr-par-1"
  organization_id = var.scaleway_organization_id
  project_id      = var.scaleway_project_config.project_id
  access_key      = var.scaleway_project_config.access_key
  secret_key      = var.scaleway_project_config.secret_key
}
