locals {
  cluster_cname = "${scaleway_domain_record.cluster_cname.name}.${scaleway_domain_record.cluster_cname.dns_zone}."
  dev_subdomain = "${scaleway_domain_record.dev.name}.${scaleway_domain_record.dev.dns_zone}"
}
resource "scaleway_domain_record" "cluster_cname" {
  provider = scaleway.default_project
  dns_zone = var.dns_zone_incubateur
  name     = "lb-kubernetes-dev"
  type     = "A"
  data     = scaleway_lb_ip.haproxy_ip.ip_address
}
resource "scaleway_domain_record" "cluster_cname_bis" {
  provider = scaleway.default_project
  dns_zone = var.dns_zone_incubateur
  name     = "lb-kubernetes-dev-bis"
  type     = "A"
  data     = scaleway_lb_ip.haproxy_ip_bis.ip_address
}

resource "scaleway_domain_record" "dev" {
  provider = scaleway.default_project
  dns_zone = var.dns_zone_incubateur
  name     = "dev"
  type     = "CNAME"
  data     = local.cluster_cname
}

resource "scaleway_domain_record" "dev_wildcard" {
  provider = scaleway.default_project
  dns_zone = var.dns_zone_incubateur
  name     = "*.dev"
  type     = "CNAME"
  data     = local.cluster_cname
}
