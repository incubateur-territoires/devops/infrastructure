resource "kubernetes_namespace" "cert_manager" {
  metadata {
    name = "cert-manager"
  }
}

resource "helm_release" "cert_manager" {
  name       = "cert-manager"
  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  version    = "v1.13.2"
  wait       = true
  namespace  = kubernetes_namespace.cert_manager.metadata[0].name
  set {
    name  = "installCRDs"
    value = true
  }
}

resource "helm_release" "cert_manager_scaleway_webhook" {
  name       = "cert-manager-scaleway-webhook"
  repository = "https://gitlab.com/api/v4/projects/41792297/packages/helm/stable"
  chart      = "scaleway-webhook"
  version    = "0.0.1"
  namespace  = helm_release.cert_manager.namespace
  set_sensitive {
    name  = "secret.accessKey"
    value = var.scaleway_cert_manager_api_key.access_key
  }
  set_sensitive {
    name  = "secret.secretKey"
    value = var.scaleway_cert_manager_api_key.secret_key
  }
}

resource "kubernetes_manifest" "cert_manager_clusterissuer_letsencrypt_staging" {
  depends_on = [helm_release.cert_manager]

  manifest = {
    "apiVersion" = "cert-manager.io/v1"
    "kind"       = "ClusterIssuer"
    "metadata" = {
      "name" = "letsencrypt-staging"
    }
    "spec" = {
      "acme" = {
        "privateKeySecretRef" = {
          "name" = "letsencrypt-staging"
        }
        "server" = "https://acme-staging-v02.api.letsencrypt.org/directory"
        "solvers" = [
          {
            "http01" = {
              "ingress" = {
                "ingressClassName" = "haproxy"
              }
            }
            "selector" = {}
          },
          {
            "dns01" = {
              "webhook" = {
                "groupName"  = "acme.scaleway.com"
                "solverName" = "scaleway"
              }
            }
          }
        ]
      }
    }
  }
}

resource "kubernetes_manifest" "cert_manager_clusterissuer_letsencrypt_prod" {
  depends_on = [helm_release.cert_manager]

  manifest = {
    "apiVersion" = "cert-manager.io/v1"
    "kind"       = "ClusterIssuer"
    "metadata" = {
      "name" = "letsencrypt-prod"
    }
    "spec" = {
      "acme" = {
        "privateKeySecretRef" = {
          "name" = "letsencrypt-prod"
        }
        "server" = "https://acme-v02.api.letsencrypt.org/directory"
        "solvers" = [
          {
            "http01" = {
              "ingress" = {
                "ingressClassName" = "haproxy"
              }
            }
            "selector" = {}
          },
          {
            "dns01" = {
              "webhook" = {
                "groupName"  = "acme.scaleway.com"
                "solverName" = "scaleway"
              }
            }
          }
        ]
      }
    }
  }
}

resource "kubernetes_manifest" "cert_manager_clusterissuer_letsencrypt_prod_dns01" {
  depends_on = [helm_release.cert_manager]

  manifest = {
    "apiVersion" = "cert-manager.io/v1"
    "kind"       = "ClusterIssuer"
    "metadata" = {
      "name" = "letsencrypt-prod-dns01"
    }
    "spec" = {
      "acme" = {
        "privateKeySecretRef" = {
          "name" = "letsencrypt-prod-dns01"
        }
        "server" = "https://acme-v02.api.letsencrypt.org/directory"
        "solvers" = [
          {
            "dns01" = {
              "webhook" = {
                "groupName"  = "acme.scaleway.com"
                "solverName" = "scaleway"
              }
            }
            "selector" = {}
          }
        ]
      }
    }
  }
}
