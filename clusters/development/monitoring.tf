locals {
  grafana_host = "${scaleway_domain_record.grafana.name}.${var.dns_zone_incubateur}"
}

resource "scaleway_domain_record" "grafana" {
  provider = scaleway.default_project
  dns_zone = var.dns_zone_incubateur
  name     = "grafana.dev"
  type     = "CNAME"
  data     = local.cluster_cname
}

resource "scaleway_object_bucket" "logs" {
  name = "anct-incubateur-logs-dev"
}
resource "scaleway_object_bucket" "mimir_alertmanager_storage" {
  name = "anct-incubateur-mimir-alertmanager-storage-dev"
}
resource "scaleway_object_bucket" "mimir_blocks_storage" {
  name = "anct-incubateur-mimir-blocks-storage-dev"
}
resource "scaleway_object_bucket" "mimir_ruler_storage" {
  name = "anct-incubateur-mimir-ruler-storage-dev"
}
resource "scaleway_object_bucket" "grafana_db_backups" {
  name = "anct-incubateur-grafana-db-backups-dev"
}

module "monitoring" {
  source   = "../../modules/monitoring"
  hostname = local.grafana_host

  loki_logs_bucket          = scaleway_object_bucket.logs.name
  mimir_alertmanager_bucket = scaleway_object_bucket.mimir_alertmanager_storage.name
  mimir_blocks_bucket       = scaleway_object_bucket.mimir_blocks_storage.name
  mimir_ruler_bucket        = scaleway_object_bucket.mimir_ruler_storage.name
  grafana_db_backup_bucket  = scaleway_object_bucket.grafana_db_backups.name
  bucket_region             = "fr-par"
  bucket_endpoint           = "https://s3.fr-par.scw.cloud"
  bucket_access_key         = var.scaleway_project_config.access_key
  bucket_secret_key         = var.scaleway_project_config.secret_key

  smtp_host     = var.sendinblue_smtp_host
  smtp_port     = var.sendinblue_smtp_port
  smtp_user     = var.sendinblue_smtp_user
  smtp_password = var.sendinblue_smtp_password
  smtp_from     = var.sendinblue_smtp_user
}
