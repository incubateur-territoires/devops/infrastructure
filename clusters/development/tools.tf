module "outline" {
  source                      = "./tools/outline"
  scaleway_default_access_key = var.scaleway_default_project_config.access_key
  scaleway_default_secret_key = var.scaleway_default_project_config.secret_key
  scaleway_default_project_id = var.scaleway_default_project_config.project_id
  oauth_domain                = var.outline_oauth_domain
  outline_client_id           = var.outline_client_id
  outline_client_secret       = var.outline_client_secret
  hostname                    = "outline.${local.dev_subdomain}"
  outline_email_smtp_host     = var.sendinblue_smtp_host
  outline_email_smtp_password = var.sendinblue_smtp_password
  outline_email_smtp_user     = var.sendinblue_smtp_user
  scaleway_access_key         = var.scaleway_project_config.access_key
  scaleway_secret_key         = var.scaleway_project_config.secret_key
}
