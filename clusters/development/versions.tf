terraform {
  required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
    }
    grafana = {
      source = "grafana/grafana"
    }
    helm = {
      source = "hashicorp/helm"
    }
    scaleway = {
      source = "scaleway/scaleway"
      configuration_aliases = [
        scaleway.iam,
        scaleway.default_project
      ]
    }
  }
}
