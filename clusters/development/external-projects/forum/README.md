# Forum

## Deployment

The forum is deployed using [dokku](https://dokku.com/) with the [dokku-discourse](https://github.com/badsyntax/dokku-discourse) plugin.

## Management tasks

To manage the deployment, open an SSH connection on the VM and navigate to `/home/dokku`.

### Changing configuration

This usually causes a few seconds of downtime.

```bash
dokku config:set discourse ENV_VAR_NAME=value
```

### Updating Discourse

This causes several minutes of downtime.

```bash
dokku discourse:upgrade discourse
```

### Renewing certificates

```bash
dokku letsencrypt:auto-renew
```
