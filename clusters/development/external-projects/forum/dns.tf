locals {
  forum_vm_ip = "163.172.138.130"
}
resource "scaleway_domain_record" "forum" {
  dns_zone = var.dns_zone_incubateur
  name     = "forum.dev"
  type     = "A"
  data     = local.forum_vm_ip
}
