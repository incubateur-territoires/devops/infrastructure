module "forum" {
  source              = "./external-projects/forum"
  dns_zone_incubateur = var.dns_zone_incubateur
  scaleway_access_key = var.scaleway_default_project_config.access_key
  scaleway_secret_key = var.scaleway_default_project_config.secret_key
  scaleway_project_id = var.scaleway_default_project_config.project_id
}
