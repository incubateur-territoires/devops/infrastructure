resource "kubernetes_namespace" "gateway" {
  metadata {
    name = "gateway"
  }
}

resource "scaleway_lb_ip" "haproxy_ip" {
  lifecycle {
    prevent_destroy = true
  }
}

module "haproxy" {
  source        = "gitlab.com/vigigloo/tools-k8s/haproxy"
  version       = "0.1.2"
  namespace     = kubernetes_namespace.gateway.metadata[0].name
  chart_name    = "haproxy"
  chart_version = "1.33.1"
  image_tag     = "1.10.8"
  haproxy_lb_ip = scaleway_lb_ip.haproxy_ip.ip_address
  haproxy_config_response_set_header = [
    {
      name  = "X-Frame-Options"
      value = "SAMEORIGIN"
    },
    {
      name  = "Referrer-Policy"
      value = "no-referrer"
    },
    {
      name  = "Strict-Transport-Security"
      value = "max-age=31536000"
    }
  ]
  haproxy_replicas = 5
  values = [<<-EOT
    controller:
      topologySpreadConstraints:
      - maxSkew: 1
        topologyKey: kubernetes.io/hostname
        whenUnsatisfiable: DoNotSchedule
        labelSelector:
          matchLabels:
            app.kubernetes.io/name: kubernetes-ingress
            app.kubernetes.io/instance: haproxy
      service:
        externalTrafficPolicy: "Local"
      config:
        global-config-snippet: |
          h2-workaround-bogus-websocket-clients
  EOT
  ]
}

resource "kubernetes_namespace" "gateway_bis" {
  metadata {
    name = "gateway-bis"
  }
}

resource "scaleway_lb_ip" "haproxy_ip_bis" {
  lifecycle {
    prevent_destroy = true
  }
}

module "haproxy_bis" {
  source        = "gitlab.com/vigigloo/tools-k8s/haproxy"
  version       = "0.1.2"
  namespace     = kubernetes_namespace.gateway_bis.metadata[0].name
  chart_name    = "haproxy-bis"
  chart_version = "1.33.1"
  image_tag     = "1.10.8"
  haproxy_lb_ip = scaleway_lb_ip.haproxy_ip_bis.ip_address
  haproxy_config_response_set_header = [
    {
      name  = "X-Frame-Options"
      value = "SAMEORIGIN"
    },
    {
      name  = "Referrer-Policy"
      value = "no-referrer"
    },
    {
      name  = "Strict-Transport-Security"
      value = "max-age=31536000"
    }
  ]
  haproxy_replicas      = 2
  haproxy_ingress_class = "haproxy-bis"
  values = [<<-EOT
    controller:
      ingressClassResource:
        name: haproxy-bis
      topologySpreadConstraints:
      - maxSkew: 1
        topologyKey: kubernetes.io/hostname
        whenUnsatisfiable: DoNotSchedule
        labelSelector:
          matchLabels:
            app.kubernetes.io/name: kubernetes-ingress
            app.kubernetes.io/instance: haproxy-bis
      service:
        externalTrafficPolicy: "Local"
        annotations:
          "service.beta.kubernetes.io/scw-loadbalancer-proxy-protocol-v2": "true"
      config:
        proxy-protocol: "0.0.0.0/0"
        global-config-snippet: |
          h2-workaround-bogus-websocket-clients
  EOT
  ]
}
