terraform {
  required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
    }
    grafana = {
      source = "grafana/grafana"
    }
    helm = {
      source = "hashicorp/helm"
    }
    gpg = {
      source = "Olivr/gpg"
    }
    scaleway = {
      source = "scaleway/scaleway"
      configuration_aliases = [
        scaleway,
        scaleway.iam,
        scaleway.default_project
      ]
    }
  }
}
