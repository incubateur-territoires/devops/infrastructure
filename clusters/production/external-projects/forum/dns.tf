locals {
  forum_vm_ip = "51.159.157.165"
}
resource "scaleway_domain_record" "forum" {
  dns_zone = var.dns_zone_incubateur
  name     = "forum"
  type     = "A"
  data     = local.forum_vm_ip
}
