data "scaleway_instance_server" "incubateur_outillage" {
  provider = scaleway.default_project
  zone     = "fr-par-2"
  name     = "incubateur-outillage"
}

# tflint-ignore: terraform_unused_declarations
data "scaleway_instance_server" "forum_dev_incubateur" {
  provider = scaleway.default_project
  name     = "forum-dev-incubateur"
}

# tflint-ignore: terraform_unused_declarations
data "scaleway_instance_server" "donnees_territoires" {
  provider = scaleway.default_project
  zone     = "fr-par-2"
  name     = "donnees-territoires"
}
