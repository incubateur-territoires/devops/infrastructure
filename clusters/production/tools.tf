locals {
  ed_domain = "anct.cloud-ed.fr"
}
module "decidim" {
  source                      = "./tools/decidim"
  scaleway_default_access_key = var.scaleway_default_project_config.access_key
  scaleway_default_secret_key = var.scaleway_default_project_config.secret_key
  scaleway_default_project_id = var.scaleway_default_project_config.project_id
}

module "decidim_maires" {
  source              = "./tools/decidim_maires"
  scaleway_access_key = var.scaleway_project_config.access_key
  scaleway_secret_key = var.scaleway_project_config.secret_key
  scaleway_project_id = var.scaleway_project_config.project_id
}

module "decidim_inclusion_numerique" {
  source                = "../../tools/decidim"
  scaleway_access_key   = var.scaleway_project_config.access_key
  scaleway_secret_key   = var.scaleway_project_config.secret_key
  scaleway_project_id   = var.scaleway_project_config.project_id
  decidim_host          = "cnr-numerique.anct.gouv.fr"
  decidim_smtp_user     = var.decidim_inclusion_numerique_smtp_user
  decidim_smtp_password = var.decidim_inclusion_numerique_smtp_password
  sendinblue_smtp_host  = var.sendinblue_smtp_host
  project_name          = "DecidimInclusionNumerique"
  project_slug          = "decidim-inclusion-numerique"
  image_repository      = "registry.gitlab.com/incubateur-territoires/incubateur/decidim-ditp"
  image_tag             = "0.1.1"
  backup_offset_min     = 20
  extra_env = {
    GEOCODER_LOOKUP_API_KEY = random_password.decidim_inclusion_numerique_here_api_key.result
  }
}
resource "random_password" "decidim_inclusion_numerique_here_api_key" {
  length = 43
}

module "decidim_nfnv" {
  source                = "../../tools/decidim"
  scaleway_access_key   = var.scaleway_project_config.access_key
  scaleway_secret_key   = var.scaleway_project_config.secret_key
  scaleway_project_id   = var.scaleway_project_config.project_id
  decidim_host          = "numeriquefrancenationverte.anct.gouv.fr"
  decidim_smtp_user     = var.decidim_nfnv_smtp_user
  decidim_smtp_password = var.decidim_nfnv_smtp_password
  sendinblue_smtp_host  = var.sendinblue_smtp_host
  project_name          = "DecidimNFNV"
  project_slug          = "decidim-nfnv"
  backup_offset_min     = 59
  memcached_enabled     = true
  image_repository      = "registry.gitlab.com/incubateur-territoires/incubateur/decidim-ditp/main"
  image_tag             = "ecf5266917ee7357b553fe10cc75fa3cb23cd597"
}

resource "scaleway_domain_record" "outline" {
  provider = scaleway.default_project
  dns_zone = var.dns_zone_incubateur
  name     = "outline"
  type     = "CNAME"
  data     = local.cluster_cname
}
module "outline" {
  source                      = "./tools/outline"
  scaleway_default_access_key = var.scaleway_default_project_config.access_key
  scaleway_default_secret_key = var.scaleway_default_project_config.secret_key
  scaleway_default_project_id = var.scaleway_default_project_config.project_id
  mattermost_host             = "chat.incubateur.anct.gouv.fr"
  outline_postgresql          = var.outline_postgresql
  outline_client_id           = var.outline_client_id
  outline_client_secret       = var.outline_client_secret
  outline_email_smtp_host     = var.outline_email_smtp_host
  outline_email_smtp_password = var.outline_email_smtp_password
  outline_email_smtp_user     = var.outline_email_smtp_user
  hostname                    = "${scaleway_domain_record.outline.name}.${scaleway_domain_record.outline.dns_zone}"
}

resource "scaleway_domain_record" "matomo" {
  provider = scaleway.default_project
  dns_zone = var.dns_zone_incubateur
  name     = "matomo"
  type     = "CNAME"
  data     = "stats.anct.cloud-ed.fr."
}
resource "scaleway_object_bucket" "save_matomo_db_backups" {
  name = "incubateur-matomo-backups-databases"
}

resource "gpg_private_key" "passbolt_legacy" {
  name     = "Passbolt Incubateur des Territoires"
  email    = var.sendinblue_smtp_user
  rsa_bits = 4096
}
resource "scaleway_object_bucket" "passbolt_legacy_backups" {
  name = "incubateur-passbolt-backups-databases"
}

resource "scaleway_object_bucket" "save_mobilizon_database_backups" {
  name = "incubateur-mobilizon-backups-databases"
}
resource "scaleway_object_bucket" "save_mobilizon_uploads_backups" {
  name = "incubateur-mobilizon-backups-uploads"
}
module "backup_gitlab" {
  source = "./tools/backup_gitlab"
  providers = {
    scaleway     = scaleway.default_project
    scaleway.iam = scaleway.iam
  }
}

resource "scaleway_domain_record" "coffre" {
  provider = scaleway.default_project
  dns_zone = var.dns_zone_incubateur
  name     = "coffre"
  type     = "CNAME"
  data     = "vault.${local.ed_domain}."
}

resource "scaleway_domain_record" "sentry" {
  provider = scaleway.default_project
  dns_zone = var.dns_zone_incubateur
  name     = "sentry"
  type     = "CNAME"
  data     = "sentry.${local.ed_domain}."
}
