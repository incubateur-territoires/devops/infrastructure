module "namespace" {
  source            = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version           = "1.0.1"
  max_cpu_requests  = "9"
  max_memory_limits = "20Gi"
  namespace         = "decidim"
  project_name      = "Decidim"
  project_slug      = "decidim"

  default_container_cpu_requests  = "200m"
  default_container_memory_limits = "128Mi"
}

resource "scaleway_object_bucket" "decidim" {
  name   = "anct-decidim"
  region = "fr-par"
}
resource "scaleway_object_bucket_acl" "decidim" {
  bucket = scaleway_object_bucket.decidim.name
  acl    = "public-read"
}

module "postgresql" {
  source     = "gitlab.com/vigigloo/tools-k8s/postgresql"
  version    = "0.1.0"
  chart_name = "database"
  namespace  = module.namespace.namespace
}

resource "random_password" "decidim_secret" {
  length  = 128
  special = false
  upper   = false
  lifecycle {
    ignore_changes = all
  }
}

