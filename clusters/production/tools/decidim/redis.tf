resource "helm_release" "redis" {
  name       = "redis"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "redis"
  version    = "16.13.2"
  namespace  = module.namespace.namespace

  set {
    name  = "auth.enabled"
    value = false
  }

  set {
    name  = "master.resources.limits.memory"
    value = "1024Mi"
  }

  set {
    name  = "master.resources.requests.cpu"
    value = "40m"
  }

  set {
    name  = "replica.resources.limits.memory"
    value = "1024Mi"
  }

  set {
    name  = "replica.resources.requests.cpu"
    value = "40m"
  }
}
