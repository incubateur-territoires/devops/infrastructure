variable "scaleway_default_access_key" {
  type = string
}

variable "scaleway_default_secret_key" {
  type      = string
  sensitive = true
}

variable "scaleway_default_project_id" {
  type = string
}

variable "outline_postgresql" {
  type = string
}

variable "outline_client_id" {
  type = string
}

variable "outline_client_secret" {
  type      = string
  sensitive = true
}

variable "outline_email_smtp_host" {
  type = string
}
variable "outline_email_smtp_user" {
  type = string
}
variable "outline_email_smtp_password" {
  type      = string
  sensitive = true
}

variable "hostname" {
  type = string
}

variable "mattermost_host" {
  type = string
}
