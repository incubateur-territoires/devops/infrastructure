terraform {
  required_providers {
    scaleway = {
      source = "scaleway/scaleway"
      configuration_aliases = [
        scaleway,
        scaleway.iam
      ]
    }
  }
}
