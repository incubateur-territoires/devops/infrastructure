locals {
  cluster_cname = "${scaleway_domain_record.cluster_cname.name}.${scaleway_domain_record.cluster_cname.dns_zone}."
}
resource "scaleway_domain_record" "cluster_cname" {
  provider = scaleway.default_project
  dns_zone = var.dns_zone_incubateur
  name     = "lb-kubernetes-prod"
  type     = "A"
  data     = scaleway_lb_ip.haproxy_ip.ip_address
}
resource "scaleway_domain_record" "cluster_cname_bis" {
  provider = scaleway.default_project
  dns_zone = var.dns_zone_incubateur
  name     = "lb-kubernetes-prod-bis"
  type     = "A"
  data     = scaleway_lb_ip.haproxy_ip_bis.ip_address
}

resource "scaleway_domain_record" "prod" {
  provider = scaleway.default_project
  dns_zone = var.dns_zone_incubateur
  name     = ""
  type     = "A"
  data     = scaleway_lb_ip.haproxy_ip.ip_address
}
