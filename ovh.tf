data "ovh_domain_zone" "anct_beta_gouv_fr" {
  name = "anct.beta.gouv.fr"
}

resource "ovh_domain_zone_record" "trail" {
  zone      = data.ovh_domain_zone.anct_beta_gouv_fr.name
  subdomain = "trail"
  fieldtype = "CNAME"
  target    = "custom.lemlist.com."
}

resource "ovh_domain_zone_record" "root_dkim" {
  zone      = data.ovh_domain_zone.anct_beta_gouv_fr.name
  subdomain = "anct.beta.gouv.fr._domainkey"
  fieldtype = "TXT"
  target    = "v=DKIM1;t=s;p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAk1PXXxuMYuw8k5va2u2d6IxHyXOnufEbeJJqsYWqyVRAbXy9A1N5iM4qvjrm9GR6cU82ZolD2FXYOCcrvuphCbfqWUCyfPbhUOCh131pj5y7g+l6UeBSqPMdLz0vaIN34f2EE7gNtIC/StkTx9wVxNfrAozE3OerFMhpILHO7RGXTHkuKKrw4sZQY3a/WBEy/6YzocKoOPuAcezdtFr/2oUAae79kyKdxZqELN5uGQzmMS3UgBR853DrkN2rCNWCCdZwaapYccL+bu3NAlINhIg8NUUq687QSIo7d7tq2nupby5ICraFnWohz3D0ggR1eiRbR/T+IKG39VtALcZ6BQIDAQAB"
}
