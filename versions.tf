terraform {
  required_version = "~> 1.7"
  required_providers {
    scaleway = {
      source  = "scaleway/scaleway"
      version = "~> 2.46"
    }
    grafana = {
      source  = "grafana/grafana"
      version = "~> 2.2.0"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 17.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "~> 3.5.1"
    }
    ovh = {
      source  = "ovh/ovh"
      version = "~> 0.32.0"
    }
    kubernetes = {
      version = "~> 2.13.0"
      source  = "hashicorp/kubernetes"
    }
    helm = {
      version = "~> 2.5.0"
      source  = "hashicorp/helm"
    }
  }
}
