## Create new project

On a new branch, adjust the project name and run the following commands:

```bash
PROJECT_NAME="My Project Name" # only ASCII alphanumeric, no accents or special characters

PROJECT_SLUG="$(echo "$PROJECT_NAME" | sed 's/ /-/g' |  sed 's/.*/\L&/g')"
PROJECT_NAME_SNAKE_CASE="$(echo "$PROJECT_NAME" | sed 's/ /_/g' |  sed 's/.*/\L&/g')"

cp -R projects/template "projects/$PROJECT_NAME_SNAKE_CASE"
sed -i "s/REPLACE_WITH_PROJECT_NAME/\"$PROJECT_NAME\"/" "projects/$PROJECT_NAME_SNAKE_CASE/project.tf"
sed -i "s/REPLACE_WITH_PROJECT_SLUG/\"$PROJECT_SLUG\"/" "projects/$PROJECT_NAME_SNAKE_CASE/project.tf"
cat << EOF >> projects/main.tf

module "$PROJECT_NAME_SNAKE_CASE" {
  source = "./$PROJECT_NAME_SNAKE_CASE"
  common = local.common
  providers = {
    scaleway.iam     = scaleway.iam
    scaleway.dns     = scaleway.dns
    scaleway.default = scaleway.default
  }
}
EOF
```

Your project is created under the `projects/<name>` folder.
Edit `projects/<name>/project.tf` to enable the features you want.
