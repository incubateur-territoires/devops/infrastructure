# Create a token for an application to access the Directus API

Tokens need to be scoped with specific permissions to access only the data the application needs without impacting other startups.
Also, it shouldn't be tied to a user which might later be deactivated.

Because of this, a specific procedure should be followed.
This can only be done by a Directus administrators, and it can't be delegated to a startup member.

## Creating a new role

This may not be necessary if a role with satisfying permissions already exists.
In this case, skip to creating a new user.

Under the [Directus access control settings](https://directus.incubateur.anct.gouv.fr/admin/settings/roles), create a new role with app access and without admin access.
Once the role is created, configure its permissions on the role's page.

## Creating a new user with a token

Under the [Directus access control settings](https://directus.incubateur.anct.gouv.fr/admin/settings/roles), open the role you need to bind to your user.
At the bottom of the role's page is the list of users with this role.
Click the Create New button.

Fill in the First Name with the name of the application, and keep the email and passwords empty.
At the bottom of the form, in the token field, click the + icon to generate a new token.
Save this token in the password manager to later share it, and make sure to click the Save icon on the top right to save your new user.
