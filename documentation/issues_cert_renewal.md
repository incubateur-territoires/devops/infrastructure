# Issues with cert renewal

For some specific cases, cert renewal fails to be done automatically.
For example, the certificate for Grafana can't pass the ACME challenge in normal conditions.
To renew the certificate, you need to:
- edit the Service used as backend by the Ingress
- remove the annotation for HAProxy to restrict access on certain routes. Save it to put it back later though. Then close the editor
- delete the pending Challenge
- wait a few seconds for the new challenge to be valid
- edit the Service once again to put the annotation back

Alternatively, this can also be done with terraform:
- comment out the service annotation
- run terraform to apply the change
- use kubectl to delete the pending challenge
- wait a few seconds
- undo the service annotation commenting
- run terraform to apply the change
