## Resource quotas

In order to prevent workloads from potentially endlessly scaling horizontally and triggering cluster scale ups, resource quotas are usually set for each namespace.
The [namespace module](https://gitlab.com/vigigloo/tf-modules/k8s_limited_namespace) allows setting these resource quotas easily.

Unless justified by a specific use case, resource quotas should be defined as follows:
- Namespaces should specify a maximum amount of memory limits and a maximum amount of CPU requests
- Namespaces can specify default values for container memory limits and CPU requests
- Containers should specify memory limits and CPU requests. The memory limit should be the maximum amount it can use, while the CPU requests should be a typical average load during daytime.

Example values:
```hcl
# for namespace
module "namespace" {
  source            = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version           = "2.0.2"
  max_cpu_requests  = "4"
  max_memory_limits = "8Gi"
  namespace         = var.namespace
  project_name      = var.namespace
  project_slug      = var.namespace

  default_container_cpu_requests  = "20m"
  default_container_memory_limits = "16Mi"
}
```
```yaml
# for helm values
resources:
  limits:
    memory: 1Gi
  requests:
    cpu: 10m
```
