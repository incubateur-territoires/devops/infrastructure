# Configuring the GitLab token for Sonarcloud

Sonarcloud requires a GitLab token for it's integration features.
However, GitLab enforces a token expiry, so we need to regularly update the token in Sonarcloud.

## Create a new token in GitLab

Under [the main group's access token settings](https://gitlab.com/groups/incubateur-territoires/-/settings/access_tokens), create a new token.
Call it `sonarcloud` for example, set an expiry far off in the future, select the `Owner` role and `api` scope.

## Configure the token in Sonarcloud

Under [Sonarcloud's organization settings](https://sonarcloud.io/organizations/incubateur-territoires/edit?category=binding), click `Edit token`, paste the token, then `Update token`.
