# Upgrading grafana

Because the Grafana URI is used to configure the Grafana provider, any change to the Grafana release will cause Terraform to show error about the Grafana provider.
To perform the upgrade, use the `-target` option to only target the `grafana` module or Helm release.
