module "postgresql" {
  source  = "gitlab.com/vigigloo/tools-k8s-crunchydata/pgcluster"
  version = "0.0.22"

  chart_name = "postgresql"
  namespace  = kubernetes_namespace.monitoring.metadata[0].name

  pg_volume_size = "5Gi"
  pg_replicas    = 2

  pg_backups_volume_enabled             = true
  pg_backups_volume_size                = "40Gi"
  pg_backups_volume_full_schedule       = "45 2 * * 0"
  pg_backups_volume_incr_schedule       = "45 2 * * 1-6"
  pg_backups_volume_full_retention      = 4
  pg_backups_volume_full_retention_type = "count"

  pg_backups_s3_enabled       = true
  pg_backups_s3_bucket        = var.grafana_db_backup_bucket
  pg_backups_s3_region        = "fr-par"
  pg_backups_s3_endpoint      = local.bucket_endpoint_without_scheme
  pg_backups_s3_access_key    = var.bucket_access_key
  pg_backups_s3_secret_key    = var.bucket_secret_key
  pg_backups_s3_full_schedule = "45 3 * * 0"
  pg_backups_s3_incr_schedule = "45 3 * * 1-6"
  values = [
    <<-EOT
    postgresVersion: 14
    imagePgBackRest: null
    imagePostgres: null
    EOT
    ,
    file("${path.module}/db_resources.yaml"),
  ]
}
