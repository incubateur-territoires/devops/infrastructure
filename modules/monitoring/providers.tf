provider "grafana" {
  url  = module.grafana.url
  auth = local.grafana_auth
}
