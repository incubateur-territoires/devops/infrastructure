output "main_org_id" {
  value     = random_password.main_org_id.result
  sensitive = true
}

locals {
  grafana_auth = "${module.grafana.grafana_admin_user}:${module.grafana.grafana_admin_password}"
}
output "grafana_auth" {
  value     = local.grafana_auth
  sensitive = true
}

output "grafana_url" {
  value = module.grafana.url
}
output "loki_service_url" {
  value = local.loki_service_url
}
output "mimir_internal_prometheus_url" {
  value = module.mimir.internal_prometheus_url
}
