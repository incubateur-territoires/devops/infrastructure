variable "hostname" {
  type = string
}

variable "loki_logs_bucket" {
  type = string
}
variable "mimir_alertmanager_bucket" {
  type = string
}
variable "mimir_blocks_bucket" {
  type = string
}
variable "mimir_ruler_bucket" {
  type = string
}
variable "grafana_db_backup_bucket" {
  type = string
}

variable "bucket_endpoint" {
  type = string
}
locals {
  bucket_endpoint_without_scheme = trimprefix(trimprefix(var.bucket_endpoint, "https://"), "http://")
}
variable "bucket_region" {
  type = string
}
variable "bucket_access_key" {
  type      = string
  sensitive = true
}
variable "bucket_secret_key" {
  type      = string
  sensitive = true
}

variable "smtp_host" {
  type = string
}
variable "smtp_port" {
  type = string
}
variable "smtp_user" {
  type      = string
  sensitive = true
}
variable "smtp_password" {
  type      = string
  sensitive = true
}
variable "smtp_from" {
  type = string
}
