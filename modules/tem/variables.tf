variable "dns_zone" {}
variable "dns_name" {
  default     = ""
  description = "Used if the TEM domain is not created at the root of the zone."
}
variable "ttl" {
  default = 3600
}
variable "create_mx" {
  description = "Wether to create a MX record using Scaleway's blackhole. This can be disabled in case the domain already has a mailing configuration."
  default     = true
}
variable "create_spf" {
  description = "Wether to create a TXT record with the SPF configuration. This can be disabled in case the domain already has a mailing configuration."
  default     = true
}
variable "create_dmarc" {
  description = "Wether to create a TXT record with the DMARC configuration. This can be disabled in case the domain already has a mailing configuration."
  default     = true
}
