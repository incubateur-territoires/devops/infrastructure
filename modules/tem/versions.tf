terraform {
  required_providers {
    scaleway = {
      source = "scaleway/scaleway"
      configuration_aliases = [
        scaleway.iam,        # used to create api keys
        scaleway.dns,        # used to configure dns records for TEM validation, depends on which scaleway project contains the DNS zone
        scaleway.tem_domain, # used to create the TEM domain, usually inside the startup project
      ]
    }
  }
}
