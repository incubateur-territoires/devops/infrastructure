locals {
  tem_domain_name = var.dns_name == "" ? var.dns_zone : "${var.dns_name}.${var.dns_zone}"
}
resource "scaleway_tem_domain" "tem" {
  provider   = scaleway.tem_domain
  name       = local.tem_domain_name
  accept_tos = true
}
resource "scaleway_tem_domain_validation" "tem" {
  provider  = scaleway.tem_domain
  domain_id = scaleway_tem_domain.tem.id
  depends_on = [
    scaleway_domain_record.tem_mx,
    scaleway_domain_record.tem_spf,
    scaleway_domain_record.tem_dkim,
    scaleway_domain_record.tem_dmarc,
  ]
}

