locals {
  # reworking the record names depending on wether they're in the zone root or not
  tem_dkim_name  = var.dns_name == "" ? "${scaleway_tem_domain.tem.project_id}._domainkey" : "${scaleway_tem_domain.tem.project_id}._domainkey.${var.dns_name}"
  tem_dmarc_name = var.dns_name == "" ? "_dmarc" : "_dmarc.${var.dns_name}"
}
resource "scaleway_domain_record" "tem_mx" {
  count    = var.create_mx ? 1 : 0
  provider = scaleway.dns
  dns_zone = var.dns_zone
  name     = var.dns_name
  type     = "MX"
  data     = scaleway_tem_domain.tem.mx_blackhole
  ttl      = var.ttl
}
resource "scaleway_domain_record" "tem_dkim" {
  provider = scaleway.dns
  dns_zone = var.dns_zone
  name     = local.tem_dkim_name
  type     = "TXT"
  data     = scaleway_tem_domain.tem.dkim_config
  ttl      = var.ttl
}
resource "scaleway_domain_record" "tem_spf" {
  count    = var.create_spf ? 1 : 0
  provider = scaleway.dns
  dns_zone = var.dns_zone
  name     = var.dns_name
  type     = "TXT"
  data     = "v=spf1 ${scaleway_tem_domain.tem.spf_config} -all"
  ttl      = var.ttl
}
resource "scaleway_domain_record" "tem_dmarc" {
  count    = var.create_dmarc ? 1 : 0
  provider = scaleway.dns
  dns_zone = var.dns_zone
  name     = local.tem_dmarc_name
  type     = "TXT"
  data     = scaleway_tem_domain.tem.dmarc_config
  ttl      = var.ttl
}
