variable "name" {
  type        = string
  description = "Name of API key, can be verbose and contain spaces"
}
variable "description" {
  type        = string
  description = "Longer description for the API key. Will be appended to show it's managed by terraform automatically."
}
locals {
  description = "${var.description} (managed by Terraform)"
}

variable "permissions" {
  type        = list(string)
  description = "See https://www.scaleway.com/en/docs/identity-and-access-management/iam/reference-content/permission-sets/"
}
variable "project_id" {
  type = string
}
