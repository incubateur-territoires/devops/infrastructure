resource "scaleway_iam_application" "application" {
  provider    = scaleway.iam
  name        = var.name
  description = local.description
}
resource "scaleway_iam_group" "group" {
  provider        = scaleway.iam
  name            = var.name
  description     = local.description
  application_ids = [scaleway_iam_application.application.id]
}
resource "scaleway_iam_policy" "policy" {
  provider = scaleway.iam
  name     = var.name
  group_id = scaleway_iam_group.group.id
  rule {
    project_ids          = [var.project_id]
    permission_set_names = var.permissions
  }
  description = local.description
}
resource "scaleway_iam_api_key" "key" {
  provider           = scaleway.iam
  application_id     = scaleway_iam_application.application.id
  description        = local.description
  default_project_id = var.project_id
}
