output "access_key" {
  value     = scaleway_iam_api_key.key.access_key
  sensitive = true
}

output "secret_key" {
  value     = scaleway_iam_api_key.key.secret_key
  sensitive = true
}

output "application_id" {
  value = scaleway_iam_application.application.id
}
