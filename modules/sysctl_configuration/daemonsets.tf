resource "kubernetes_namespace_v1" "infra" {
  metadata {
    name = "configure-sysctl"
  }
}

resource "kubernetes_daemon_set_v1" "vm_custom_kernel_config" {
  metadata {
    namespace = kubernetes_namespace_v1.infra.metadata[0].name
    name      = "set-kernel-configs"
  }
  spec {
    selector {
      match_labels = {
        "app.kubernetes.io/instance" = "vm-custom-kernel-config"
        "app.kubernetes.io/name"     = "sysctl"
      }
    }
    template {
      metadata {
        labels = {
          "app.kubernetes.io/instance" = "vm-custom-kernel-config"
          "app.kubernetes.io/name"     = "sysctl"
        }
      }
      spec {
        init_container {
          name  = "sysctl-map-count"
          image = "busybox:1.36.1"
          command = [
            "sysctl",
            "-w",
            "vm.max_map_count=262144",
          ]
          security_context {
            privileged  = true
            run_as_user = 0
          }
        }
        init_container {
          name  = "sysctl-kernel-core"
          image = "busybox:1.36.1"
          command = [
            "sysctl",
            "-w",
            "kernel.core_pattern=/tmp/core_dumps/core_%e.%p",
          ]
          security_context {
            privileged  = true
            run_as_user = 0
          }
        }
        container {
          name  = "sleep"
          image = "busybox:1.36.1"
          command = [
            "sleep",
            "infinity",
          ]
        }
      }
    }
  }
}
