output "org_id" {
  value = grafana_organization.org.org_id
}

output "api_key" {
  value = grafana_service_account_token.terraform.key
}
