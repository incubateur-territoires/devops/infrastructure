resource "grafana_organization" "org" {

  name       = var.project_name
  admin_user = "terraform"
  lifecycle {
    ignore_changes = [editors, admins, viewers]
  }
}

resource "grafana_service_account" "terraform" {
  name   = "terraform-org-${grafana_organization.org.org_id}"
  role   = "Admin"
  org_id = grafana_organization.org.org_id
}
resource "grafana_service_account_token" "terraform" {
  name               = "sa-terraform-org-${grafana_organization.org.org_id}"
  service_account_id = grafana_service_account.terraform.id
}
