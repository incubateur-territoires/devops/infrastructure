# bootstraping the different API keys required for the infrastructure projects
locals {
  cert_manager = {
    name        = "Cert manager token"
    description = "Managed by terraform"
  }
  k8s_production = {
    name        = "Kubernetes production"
    description = "Managed by terraform"
  }
  k8s_development = {
    name        = "Kubernetes development"
    description = "Managed by terraform"
  }
  allprojectsdns_key = {
    name        = "All projects DNS API key"
    description = "Managed by terraform"
  }
}

# Cert manager Scaleway webhook for DNS01 challenges requires permissions for domains across several projects
resource "scaleway_iam_application" "cert_manager" {
  provider    = scaleway.iam
  name        = local.cert_manager.name
  description = local.cert_manager.description
}
resource "scaleway_iam_policy" "cert_manager" {
  provider       = scaleway.iam
  name           = local.cert_manager.name
  description    = local.cert_manager.description
  application_id = scaleway_iam_application.cert_manager.id
  rule {
    organization_id      = var.scaleway_organization_id
    permission_set_names = ["DomainsDNSFullAccess"]
  }
}
resource "scaleway_iam_api_key" "cert_manager" {
  provider       = scaleway.iam
  application_id = scaleway_iam_application.cert_manager.id
  description    = "${local.cert_manager.name} (${local.cert_manager.description})"
}

# Applications for kubernetes projects to refactor IAM API keys
resource "scaleway_iam_application" "k8s_production" {
  provider    = scaleway.iam
  name        = local.k8s_production.name
  description = local.k8s_production.description
}
resource "scaleway_iam_application" "k8s_development" {
  provider    = scaleway.iam
  name        = local.k8s_development.name
  description = local.k8s_development.description
}
resource "scaleway_iam_policy" "k8s_production" {
  provider       = scaleway.iam
  application_id = scaleway_iam_application.k8s_production.id
  name           = local.k8s_production.name
  description    = local.k8s_production.description
  rule {
    project_ids          = [scaleway_account_project.k8s_prod.id]
    permission_set_names = ["AllProductsFullAccess"]
  }
}
resource "scaleway_iam_policy" "k8s_development" {
  provider       = scaleway.iam
  application_id = scaleway_iam_application.k8s_development.id
  name           = local.k8s_development.name
  description    = local.k8s_development.description
  rule {
    project_ids          = [scaleway_account_project.k8s_dev.id]
    permission_set_names = ["AllProductsFullAccess"]
  }
}
resource "scaleway_iam_api_key" "k8s_production" {
  provider           = scaleway.iam
  application_id     = scaleway_iam_application.k8s_production.id
  description        = "API key used for the main infrastructure devops project to access production cluster (managed by terraform)"
  default_project_id = scaleway_account_project.k8s_prod.id
}
resource "scaleway_iam_api_key" "k8s_development" {
  provider           = scaleway.iam
  application_id     = scaleway_iam_application.k8s_development.id
  description        = "API key used for the main infrastructure devops project to access development cluster (managed by terraform)"
  default_project_id = scaleway_account_project.k8s_dev.id
}

# API key for default project
module "default_project_api_key" {
  source      = "./modules/scw_api_key"
  name        = "terraform infra default project"
  project_id  = scaleway_account_project.default.id
  description = "Used by the infra terraform project to manage resources on Scaleway's default project"
  permissions = ["AllProductsFullAccess"]
  providers = {
    scaleway.iam = scaleway.iam
  }
}

# API key for provider creating DNS zones across projects
resource "scaleway_iam_application" "allprojectsdns_key" {
  provider    = scaleway.iam
  name        = local.allprojectsdns_key.name
  description = local.allprojectsdns_key.description
}
resource "scaleway_iam_policy" "allprojectsdns_key" {
  provider       = scaleway.iam
  name           = local.allprojectsdns_key.name
  description    = local.allprojectsdns_key.description
  application_id = scaleway_iam_application.allprojectsdns_key.id
  rule {
    organization_id      = var.scaleway_organization_id
    permission_set_names = ["DomainsDNSFullAccess"]
  }
}
resource "scaleway_iam_api_key" "allprojectsdns_key" {
  provider       = scaleway.iam
  application_id = scaleway_iam_application.allprojectsdns_key.id
  description    = "${local.allprojectsdns_key.name} (${local.allprojectsdns_key.description})"
}
