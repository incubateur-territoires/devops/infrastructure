resource "scaleway_object_bucket" "decidim" {
  name   = "anct-${var.project_slug}"
  region = "fr-par"
}
resource "scaleway_object_bucket_acl" "decidim" {
  bucket = scaleway_object_bucket.decidim.name
  acl    = "public-read"
}

resource "scaleway_object_bucket" "decidim_backups" {
  name   = "anct-${var.project_slug}-database-backups"
  region = "fr-par"
}
