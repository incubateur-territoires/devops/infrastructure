variable "scaleway_access_key" {
  type = string
}
variable "scaleway_secret_key" {
  type      = string
  sensitive = true
}
variable "scaleway_project_id" {
  type = string
}

variable "decidim_host" {
  type = string
}

variable "decidim_smtp_user" {
  type = string
}
variable "decidim_smtp_password" {
  type      = string
  sensitive = true
}

variable "sendinblue_smtp_host" {
  type = string
}

variable "project_name" {
  type = string
}
variable "project_slug" {
  type = string
}
variable "backup_offset_min" {
  type = number
}
variable "extra_env" {
  type    = map(string)
  default = {}
}

variable "memcached_enabled" {
  type    = bool
  default = false
}

variable "image_repository" {
  type = string
}
variable "image_tag" {
  type = string
}
