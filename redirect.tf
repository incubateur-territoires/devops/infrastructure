locals {
  redirects = {
    test = {
      dns_zone  = "incubateur.anct.gouv.fr"
      name      = "test-redirect"
      keep_path = true
    }
    decidim_incub = {
      dns_zone = "incubateur.anct.gouv.fr"
      name     = "mon"
    }
    esd = {
      dns_zone = "espacesurdemande.fr"
      target   = "espacesurdemande.anct.gouv.fr"
    }
    esd_www = {
      dns_zone = "espacesurdemande.fr"
      name     = "www"
      target   = "espacesurdemande.anct.gouv.fr"
    }
    aei = {
      dns_zone = "agentsenintervention.fr"
      target   = "agentsenintervention.anct.gouv.fr"
    }
  }
}
resource "scaleway_container_namespace" "redirect" {
  provider = scaleway.default_project
  name     = "redirect"
}
resource "scaleway_container" "redirect" {
  provider       = scaleway.default_project
  namespace_id   = scaleway_container_namespace.redirect.id
  name           = "redirect"
  http_option    = "redirected" # force https
  registry_image = "registry.gitlab.com/incubateur-territoires/devops/redirect:main"
  port           = 80
  cpu_limit      = 250
  memory_limit   = 250
  deploy         = true
  min_scale      = 1
  max_scale      = 1

  environment_variables = {
    NGINX_INLINE_CONFIG = <<-EOT
    %{for r in local.redirects}
    server {
        listen       80;
        server_name  %{if lookup(r, "name", null) == null}${r.dns_zone}%{else}${r.name}.${r.dns_zone}%{endif};
        location / {
          rewrite (.*) https://${lookup(r, "target", "incubateur.anct.gouv.fr")}%{if lookup(r, "keep_path", false)}$1%{endif} permanent;
        }
    }
    %{endfor}
    EOT
  }
}
resource "scaleway_container_domain" "redirect" {
  provider     = scaleway.default_project
  for_each     = local.redirects
  container_id = scaleway_container.redirect.id
  hostname     = lookup(each.value, "name", null) == null ? each.value.dns_zone : "${each.value.name}.${each.value.dns_zone}"
}
resource "scaleway_domain_record" "redirect" {
  provider = scaleway.dns
  for_each = local.redirects
  dns_zone = each.value.dns_zone
  name     = lookup(each.value, "name", null)
  ttl      = 60
  type     = "ALIAS"
  data     = "${scaleway_container.redirect.domain_name}."
}
